//
//  UserAPI.swift
//  AQUA
//
//  Created by Krishna on 05/04/17.
//  Copyright © 2017 MindfulSas. All rights reserved.
//

import Foundation


/**
 UserSignIn API contains the endpoints to Create/Read/Update Logged in UserProfiles.
 */

class UserAPI{
    
    private let userRemoteReplicator: UserRemoteReplicator!
   
    
    //Utilize Singleton pattern by instanciating UserAPI only once.
    class var sharedInstance: UserAPI {
        struct Singleton {
            static let instance = UserAPI()
        }
        return Singleton.instance
    }
    
    init(){
        self.userRemoteReplicator = UserRemoteReplicator()
        
    }
    
    
    // MARK: SignUp
    func userSignUp(userDetials: Dictionary<String, AnyObject> , callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, AnyObject>? , _ error: String? ) -> Void)   {
        userRemoteReplicator.userSignUp(userDetials: userDetials){ (responseData, error) -> Void in
            if responseData != nil {
                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
                    let data = responseData![APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
                    Constants.kUserDefaults.set(data[appConstants.userId]! as! String, forKey: appConstants.userId)
                       print(Constants.kUserDefaults.string(forKey: appConstants.userId))
                   
                    
                    
                    callback(true,responseData,nil)
                }else{
                    callback(false,responseData,responseData!["error"] as? String)
                }
            }
            
        }
        
    }
    
  
    
    
    // MARK: Forgot Password
    func forgotPassword(userDetials: Dictionary<String, AnyObject> , callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, AnyObject>? , _ error: String? ) -> Void)   {
        userRemoteReplicator.forgotPassword(userDetials: userDetials  as! Dictionary<String, AnyObject>){ (responseData, error) -> Void in
            if responseData != nil {
                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
                    let data = responseData![APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
                    Constants.kUserDefaults.set(data[appConstants.id]! as! String, forKey: appConstants.userId)
                    print(Constants.kUserDefaults.string(forKey: appConstants.userId))
                    
                    
                    
                    callback(true,responseData,nil)
                }else{
                    callback(false,responseData,responseData!["error"] as? String)
                }
            }
            
        }
        
    }
    // Check Code manually
    func checkManually(userDetials: Dictionary<String, AnyObject> , callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, AnyObject>? , _ error: String? ) -> Void)   {
        userRemoteReplicator.checkManually(userDetials: userDetials){ (responseData, error) -> Void in
            if responseData != nil {
                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
                    let data = responseData![APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
                    print(data)
                    
                    
                    
                    callback(true,responseData,nil)
                }else{
                    callback(false,responseData,responseData!["error"] as? String)
                }
            }
            
        }
        
    }
    
    // Qr Code
    func scanQr(userDetials: Dictionary<String, AnyObject> , callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, AnyObject>? , _ error: String? ) -> Void)   {
        userRemoteReplicator.scanQrcode(userDetials: userDetials){ (responseData, error) -> Void in
            if responseData != nil {
                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
                    let data = responseData![APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
                    print(data)
                    
                    
                    
                    callback(true,responseData,nil)
                }else{
                    callback(false,responseData,responseData!["error"] as? String)
                }
            }
            
        }
        
    }
    // MARK:-- getAllUsers
    func getAllUsers1(type:String,callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
    {
        userRemoteReplicator.getAllUsers1(type: type, callback:  { (Data, error) in
            if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
                callback(Data! , nil)
            }
            else{
                print("Getting Error")
                
            }
            
        })
    }
  // MARK: SignIn
  func userSignIn(userDetials: Dictionary<String, AnyObject> , callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, AnyObject>? , _ error: String? ) -> Void)   {
    userRemoteReplicator.userSignIn(userDetials: userDetials){ (responseData, error) -> Void in
      if responseData != nil {
        if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
          let data = responseData![APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
            print(data)

            
            
          Constants.kUserDefaults.set(data[appConstants.token] , forKey: appConstants.token)
          let dataProfile = NSKeyedArchiver.archivedData(withRootObject: data)
          Constants.kUserDefaults.set(dataProfile, forKey: appConstants.profile)
          Constants.kUserDefaults.set(data[UserAttributes.status.rawValue], forKey: UserAttributes.status.rawValue)
          
         
          
          callback(true,responseData,nil)
        }else{
          callback(false,responseData,responseData!["error"] as? String)
        }
      }
      
    }
    
  }
  
    // MARK: Contact Us
    func ContactUs(userDetials: Dictionary<String, AnyObject> , callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, AnyObject>? , _ error: String? ) -> Void)   {
        userRemoteReplicator.UserContactUs(userDetials: userDetials){ (responseData, error) -> Void in
            if responseData != nil {
                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
                    let data = responseData![APIConstants.data.rawValue] as! Dictionary<String, AnyObject>

                    callback(true,responseData,nil)
                }else{
                    callback(false,responseData,responseData!["error"] as? String)
                }
            }
            
        }
        
    }
  
    // MARK: Update Profile
    func userUpdateProfile(userDetials: User , callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, AnyObject>? , _ error: String? ) -> Void)   {
        
        
        userRemoteReplicator.userUpdateProfile(userDetials: userDetials.dictionaryRepresentation() as! Dictionary<String, AnyObject>, userID : String(describing: userDetials.id!) ){ (responseData, error) -> Void in
            if responseData != nil {
                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
                    let data = responseData![APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
                    let dataProfile = NSKeyedArchiver.archivedData(withRootObject: data)
                    Constants.kUserDefaults.set(dataProfile, forKey: appConstants.profile)
                    
                    
                    
                    
                    callback(true,responseData,nil)
                }else{
                    callback(false,responseData,responseData!["error"] as? String)
                }
            }
            
        }
        
    }
    func barUpdate(userDetials: BarUpdate , callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, AnyObject>? , _ error: String? ) -> Void)   {
        
        
        userRemoteReplicator.barUpdateee(userDetials: userDetials as! Dictionary<String, AnyObject>, userID : String(describing: userDetials.id!) ){ (responseData, error) -> Void in
            if responseData != nil {
                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
                    let data = responseData![APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
                    let dataProfile = NSKeyedArchiver.archivedData(withRootObject: data)
                    Constants.kUserDefaults.set(dataProfile, forKey: appConstants.profile)
                    
                    
                    
                    
                    
                    callback(true,responseData,nil)
                }else{
                    callback(false,responseData,responseData!["error"] as? String)
                }
            }
            
        }
        
    }
    
    // MARK: Update Profile with user ID
    func userUpdateProfileWithUserID(userDetials: User , callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, AnyObject>? , _ error: String? ) -> Void)   {
        
        
        userRemoteReplicator.userUpdateProfileCurUserID(userDetials: userDetials.dictionaryRepresentation() as! Dictionary<String, AnyObject>, userID : String(describing: userDetials.id!) ){ (responseData, error) -> Void in
            if responseData != nil {
                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
                    let data = responseData![APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
                    let dataProfile = NSKeyedArchiver.archivedData(withRootObject: data)
                    Constants.kUserDefaults.set(dataProfile, forKey: appConstants.profile)
                    
                    
                    
                    
                    
                    callback(true,responseData,nil)
                }else{
                    callback(false,responseData,responseData!["error"] as? String)
                }
            }
            
        }
        
    }
    
    func paymentUpdate(userDetials: User , callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, AnyObject>? , _ error: String? ) -> Void)   {
        
        
        userRemoteReplicator.paymntUpdate(userDetials: userDetials.dictionaryRepresentation() as! Dictionary<String, AnyObject>, userID : String(describing: userDetials.id!) ){ (responseData, error) -> Void in
            if responseData != nil {
                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
                    let data = responseData![APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
                    let dataProfile = NSKeyedArchiver.archivedData(withRootObject: data)
                    Constants.kUserDefaults.set(dataProfile, forKey: appConstants.profile)
                    
                    
                    
                    
                    
                    callback(true,responseData,nil)
                }else{
                    callback(false,responseData,responseData!["error"] as? String)
                }
            }
            
        }
        
    }
    
    // Get Bar Id
    func getBarId(userDetials: User , callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, AnyObject>? , _ error: String? ) -> Void)   {
        
        print("Cover fee is:\(userDetials.coverFee)")
        userRemoteReplicator.getbaridd(userDetials: userDetials.dictionaryRepresentation() as! Dictionary<String, AnyObject>) { (responseData, error) -> Void in
            
            
            if responseData != nil {
                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
                    let data = responseData![APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
                    let dataProfile = NSKeyedArchiver.archivedData(withRootObject: data)
                    Constants.kUserDefaults.set(dataProfile, forKey: appConstants.profile)
                    
                    
                    
                    
                    
                    callback(true,responseData,nil)
                }else{
                    callback(false,responseData,responseData!["error"] as? String)
                }
            }
            
        }
        
    }
    

    
    // MARK: create new user account
    func createNewUserAccount(userDetials: User , callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, AnyObject>? , _ error: String? ) -> Void)   {
        
        
        userRemoteReplicator.createNewuserAccount(userDetials: userDetials.dictionaryRepresentation() as! Dictionary<String, AnyObject>, userID : String(describing: userDetials.id!) ){ (responseData, error) -> Void in
            if responseData != nil {
                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
                    let data = responseData![APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
                    let dataProfile = NSKeyedArchiver.archivedData(withRootObject: data)
                    Constants.kUserDefaults.set(dataProfile, forKey: appConstants.profile)
                    
                    
                    
                    
                    
                    callback(true,responseData,nil)
                }else{
                    callback(false,responseData,responseData!["error"] as? String)
                }
            }
            
        }
        
    }
    
    func userUpdateProfileWhenForgotPassword(userDetials: Dictionary<String, AnyObject>? , userId: String, callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, AnyObject>? , _ error: String? ) -> Void)   {
        
        
        userRemoteReplicator.userUpdateProfile(userDetials: userDetials  as! Dictionary<String, AnyObject>, userID: userId ){ (responseData, error) -> Void in
            if responseData != nil {
                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
                    let data = responseData![APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
                    let dataProfile = NSKeyedArchiver.archivedData(withRootObject: data)
                    Constants.kUserDefaults.set(dataProfile, forKey: appConstants.profile)
                    
                    
                    
                    
                    
                    callback(true,responseData,nil)
                }else{
                    callback(false,responseData,responseData!["error"] as? String)
                }
            }
            
        }
        
    }
    

   // MARK:-- GetUserDetals
      func getHomeData(userId:String,pageNo:Int, callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
      {
        userRemoteReplicator.getHomeDetails(query: userId,pageNo: pageNo) { (Data, error) in
          if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
            callback(Data! , nil)
          }
          else{
            print("Getting Error")

          }

        }
      }
    func getBaselineDataa(userId:String,pageNo:Int, callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
    {
        userRemoteReplicator.getbaselineDetails(query: userId,pageNo: pageNo) { (Data, error) in
            if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
                callback(Data! , nil)
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }
    //  getsignatureDrink
    func getSignatureDrnk(userId:String,pageNo:Int, callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
    {
        userRemoteReplicator.getsignatureDrink(query: userId,pageNo: pageNo) { (Data, error) in
            if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
                callback(Data! , nil)
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }
    // Get All User Data
    func getAllUserData(userId:String,pageNo:Int, callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
    {
        userRemoteReplicator.getAllUserAPi(query: userId,pageNo: pageNo) { (Data, error) in
            if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
                callback(Data! , nil)
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }
    // getbarAPi
    func getBarDataa(userId:String,pageNo:Int, callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
    {
        userRemoteReplicator.getbarAPi(query: userId,pageNo: pageNo) { (Data, error) in
            if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
                callback(Data! , nil)
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }
    func getcoverDataa(userId:String,pageNo:Int, callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
    {
        userRemoteReplicator.getcoverAPi(query: userId,pageNo: pageNo) { (Data, error) in
            if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
                callback(Data! , nil)
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }
    // Get Events
    func getEvents(userId:String,pageNo:Int, callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
    {
        userRemoteReplicator.getEvents(query: userId,pageNo: pageNo) { (Data, error) in
            if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
                callback(Data! , nil)
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }
    // Get Events Details by id
    func getEventsDetilsId(userId:String,pageNo:Int, callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
    {
        userRemoteReplicator.getEventsDetails(query: userId,pageNo: pageNo) { (Data, error) in
            if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
                callback(Data! , nil)
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }
    // Create New Event
    func newEvent(userDetials: User , callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, AnyObject>? , _ error: String? ) -> Void)   {
        
      
        userRemoteReplicator.createNewEvent(userDetials: userDetials.dictionaryRepresentation() as! Dictionary<String, AnyObject>, userID : String(describing: userDetials.id!) ){ (responseData, error) -> Void in
            if responseData != nil {
                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
                    let data = responseData![APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
                    let dataProfile = NSKeyedArchiver.archivedData(withRootObject: data)
                    Constants.kUserDefaults.set(dataProfile, forKey: appConstants.profile)
        
                    callback(true,responseData,nil)
                }else{
                    callback(false,responseData,responseData!["error"] as? String)
                }
            }
            
        }
        
    }
 
    // MARK: Update Event
    func UpdateEvent(userDetials: User , callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, AnyObject>? , _ error: String? ) -> Void)   {
        
        
        userRemoteReplicator.UpdateEvent(userDetials: userDetials.dictionaryRepresentation() as! Dictionary<String, AnyObject>, userID : String(describing: userDetials.id!) ){ (responseData, error) -> Void in
            if responseData != nil {
                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
                    let data = responseData![APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
                    let dataProfile = NSKeyedArchiver.archivedData(withRootObject: data)
                    Constants.kUserDefaults.set(dataProfile, forKey: appConstants.profile)
                    
                    callback(true,responseData,nil)
                }else{
                    callback(false,responseData,responseData!["error"] as? String)
                }
            }
            
        }
        
    }
    
    // Get User id
    func getuserID(userId:String,pageNo:Int, callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
    {
        userRemoteReplicator.fetchUserId(query: userId,pageNo: pageNo) { (Data, error) in
            if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
                callback(Data! , nil)
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }
    func getUserPaymentInfo(userId:String,pageNo:Int, callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
    {
        userRemoteReplicator.fetchPaymentInf(query: userId,pageNo: pageNo) { (Data, error) in
            if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
                callback(Data! , nil)
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }
    // Get All BArtender Doorperson
    func getBartenderDoorperson(userId:String,pageNo:Int, callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
    {
        userRemoteReplicator.getbartenderDoorperson(query: userId,pageNo: pageNo) { (Data, error) in
            if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
                callback(Data! , nil)
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }
    
    // MARK:-- AverageApi
    func AverageChartAPI(userId:String,pageNo:Int, callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
    {
        userRemoteReplicator.AverageStatApi(query: userId,pageNo: pageNo) { (Data, error) in
            if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
                callback(Data! , nil)
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }
  
    // MARK:-- FrequencyChart Api
    func ChartData(userId:String,pageNo:Int, callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
    {
        userRemoteReplicator.ChartApii(query: userId,pageNo: pageNo) { (Data, error) in
            if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
                callback(Data! , nil)
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }
    
  // MARK:-- getAllUsers
//  func getAllUsers(query:String,pageNo:Int, callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
//  {
//    userRemoteReplicator.getAllUsers(query: query,pageNo: pageNo) { (Data, error) in
//      if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
//        callback(Data! , nil)
//      }
//      else{
//        print("Getting Error")
//
//      }
//
//    }
//  }
    func getAllUsers(type:String,callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
    {
        userRemoteReplicator.getAllUsers(type: type, callback:  { (Data, error) in
            if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
                callback(Data! , nil)
            }
            else{
                print("Getting Error")
                
            }
            
        })
    }
    //MARK:-- Get States
    func getState(countryId:String,pageNo:Int, callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
    {
        userRemoteReplicator.getState(query: countryId,pageNo: pageNo) { (Data, error) in
            if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
                callback(Data! , nil)
            }
            else{
                print("Getting Error. Can't fetch states.")
                
            }
            
        }
    }
    
    
    
    //MARK:-- Get Towns
    func getTowns(countryId:String,pageNo:Int, callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
    {
        userRemoteReplicator.getTowns(query: countryId,pageNo: pageNo) { (Data, error) in
            if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
                callback(Data! , nil)
            }
            else{
                print("Getting Error. Can't fetch states.")
                
            }
            
        }
    }
    
    
    //MARK:-- Get LGA by State Id (When NIgeria)
    func getLGAByStateId(stateId:String,pageNo:Int, callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
    {
        userRemoteReplicator.getLGAByStateId(query: stateId,pageNo: pageNo) { (Data, error) in
            if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
                callback(Data! , nil)
            }
            else{
                print("Getting Error. Can't fetch states.")
                
            }
            
        }
    }

    
    
    //MARK:-- Get LGA by Town Id (When SA)
    func getLGAByTownId(zipCodeId:String,pageNo:Int, callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
    {
        userRemoteReplicator.getLGAByTownId(query: zipCodeId,pageNo: pageNo) { (Data, error) in
            if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
                callback(Data! , nil)
            }
            else{
                print("Getting Error. Can't fetch states.")
                
            }
            
        }
    }

    
    
    
    
    
    
    
    
    //    // MARK: Create Category
    //    func createCategory(categoryDetials: Budget , callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, AnyObject>? , _ error: String? ) -> Void)   {
    //
    //
    //        userRemoteReplicator.createCategory(categoryDetials: categoryDetials.dictionaryRepresentation() as! Dictionary<String, AnyObject> ) { (responseData, error) -> Void in
    //            if responseData != nil {
    //                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
    //                    let data = responseData![APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
    //                    let dataFolder = NSKeyedArchiver.archivedData(withRootObject: data)
    //                    Constants.kUserDefaults.set(dataFolder, forKey: appConstants.folder)
    //
    //
    //                    callback(true,responseData,nil)
    //                }else{
    //                    callback(false,responseData,responseData!["error"] as? String)
    //                }
    //            }
    //
    //        }
    //
    //    }

    
    
//    // MARK: Create Folder
//    func createFolder(folderDetials: Folder , callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, AnyObject>? , _ error: String? ) -> Void)   {
//
//
//        userRemoteReplicator.createFolder(folderDetials: folderDetials.dictionaryRepresentation() as! Dictionary<String, AnyObject> ) { (responseData, error) -> Void in
//            if responseData != nil {
//                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
//                    let data = responseData![APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
//                    let dataFolder = NSKeyedArchiver.archivedData(withRootObject: data)
//                    Constants.kUserDefaults.set(dataFolder, forKey: appConstants.folder)
//
//
//
//
//
//                    callback(true,responseData,nil)
//                }else{
//                    callback(false,responseData,responseData!["error"] as? String)
//                }
//            }
//
//        }
//
//    }
//
//
//
//  //MARK:-- Get Folder From Remote
//  func getAllFolder(name:String,pageNo:Int, callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
//  {
//    userRemoteReplicator.getAllFolder(query: name,pageNo: pageNo) { (Data, error) in
//      if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
//        callback(Data! , nil)
//      }
//      else{
//        print("Getting Error")
//
//      }
//
//    }
//  }
    
    
//
//    // MARK: Create Task
//    func createTask(taskDetials: CreateActivity , callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, AnyObject>? , _ error: String? ) -> Void)   {
//
//
//        userRemoteReplicator.createTask(taskDetials: taskDetials.dictionaryRepresentation() as! Dictionary<String, AnyObject> ) { (responseData, error) -> Void in
//            if responseData != nil {
//                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
//                    let data = responseData![APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
//                    let dataTask = NSKeyedArchiver.archivedData(withRootObject: data)
//                    Constants.kUserDefaults.set(dataTask, forKey: appConstants.task)
//
//
//
//                    callback(true,responseData,nil)
//                }else{
//                    callback(false,responseData,responseData!["error"] as? String)
//                }
//            }
//
//        }
//
//    }
//
//
//    // MARK: Update Task Status
//    func updateTaskStatus(taskId:String, routeId: String? , callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, AnyObject>? , _ error: String? ) -> Void)   {
//
//
//        userRemoteReplicator.updateTaskStatus(taskId: taskId, routeID: routeId ) { (responseData, error) -> Void in
//            if responseData != nil {
//                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
//                    let data = responseData![APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
//                    let dataTask = NSKeyedArchiver.archivedData(withRootObject: data)
//                    Constants.kUserDefaults.set(dataTask, forKey: appConstants.task)
//
//
//
//                    callback(true,responseData,nil)
//                }else{
//                    callback(false,responseData,responseData!["error"] as? String)
//                }
//            }
//
//        }
//
//    }
//
//
//
//
//    //MARK:-- Get Todays tasks
//    func getTodaysTask(name:String, callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
//    {
//        userRemoteReplicator.getTodaysTask(query: name) { (Data, error) in
//            if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
//                callback(Data! , nil)
//            }
//            else{
//                print("Getting Error")
//
//            }
//
//        }
//    }
//
//
//    //MARK:-- Get Tasks By Folder Id
//    func getTasksByFolderId(folderId:String, callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
//    {
//        userRemoteReplicator.getTasksByFolderId(query: folderId) { (Data, error) in
//            if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
//                callback(Data! , nil)
//            }
//            else{
//                print("Getting Error")
//
//            }
//
//        }
//    }
//
//
//
//
//    //MARK:-- Get All Tasks
//    func getAllTasks(name:String, callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
//    {
//        userRemoteReplicator.getAllTasks(query: name) { (Data, error) in
//            if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
//                callback(Data! , nil)
//            }
//            else{
//                print("Getting Error")
//
//            }
//
//        }
//    }
//
//
//
//
//    // MARK: Create Amount Spent
//    func createAmount(amountDetials: Amount , callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, AnyObject>? , _ error: String? ) -> Void)   {
//
//
//        userRemoteReplicator.createAmount(amountDetials: amountDetials.dictionaryRepresentation() as! Dictionary<String, AnyObject> ) { (responseData, error) -> Void in
//            if responseData != nil {
//                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
//                    let data = responseData![APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
//                    let dataTask = NSKeyedArchiver.archivedData(withRootObject: data)
//                    Constants.kUserDefaults.set(dataTask, forKey: appConstants.task)
//
//
//
//                    callback(true,responseData,nil)
//                }else{
//                    callback(false,responseData,responseData!["error"] as? String)
//                }
//            }
//
//        }
//
//    }
//
//
    
    
    
    
    

//    
//    // MARK: Create User
//    func userSignUp(userDetials: Dictionary<String, AnyObject> , callback:@escaping ( _ successResponse: Bool, _ error: String? ) -> Void)   {
//
//        userRemoteReplicator.userSignUp(userDetials: userDetials){ (responseData, error) -> Void in
//            if responseData != nil{
//                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
//
//                    if let data = responseData?[APIConstants.data.rawValue]! as? Dictionary<String, AnyObject>!{
//                        Constants.kUserDefaults.setValue(data["id"], forKey: appConstants.id)
//                    }
//                    callback(true,nil)
//                }else{
//                    print(responseData!["error"]!)
//                    callback(false,responseData!["error"] as? String)
//                }
//            }else{
//                callback(false,responseData!["error"] as? String)
//            }
//
//        }
//
//    }

    //MARK: ValidatePin
    func userValidatePin(userDetials: Dictionary<String, String> , callback:@escaping (_ successResponse: Bool,  _ user:User?, _ error: String? ) -> Void)   {
        
        userRemoteReplicator.userVarificationCode(userDetials:userDetials as Dictionary<String, AnyObject>){ (responseData, error) -> Void in
            
            if responseData != nil{
                if  responseData![APIConstants.isSuccess.rawValue] as! Bool == true{
                    let data = responseData![APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
                    Constants.kUserDefaults.set(data[appConstants.token], forKey: appConstants.token)
                    print(appConstants.token)
                    let dataProfile = NSKeyedArchiver.archivedData(withRootObject: data)
                    Constants.kUserDefaults.set(dataProfile, forKey: appConstants.profile)
                    
                    let user = User.init(dictionary: data as NSDictionary)
                    
                    callback(true,user,nil)
                }else{
                    callback(false,nil,responseData!["error"] as? String)
                }
            }else{
                callback(false,nil,"Something went wrong!")
            }
        }
        
    }//
//
//    //MARK:- Resend Code
//    func resendVarificationCode(userDetials: Dictionary<String, String>, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void) {
//        userRemoteReplicator.resendVarificationCode(userDetials: userDetials) { (responseData, error) in
//            if responseData != nil{
//                if responseData![APIConstants.isSuccess.rawValue] as! Bool == true {
//                    let data = responseData?[APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
//                    Constants.kUserDefaults.set(data["id"]!,forKey: appConstants.id)
//                    print(Constants.kUserDefaults.value(forKey: "id")!)
//                    callback(responseData , nil)
//                }
//                else{
//                    callback(responseData, responseData!["error"] as? String)
//                }
//            }
//        }
//    }
//
//
//
//    //MARK:- Update User Password
//
//    func updateUserPassword(employeeId:NSNumber,userDetials: Dictionary<String, AnyObject>, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
//        userRemoteReplicator.updateUserPassword(employeeId: employeeId, userDetials: userDetials) { (responseData, error) in
//            if responseData != nil{
//                if responseData![APIConstants.isSuccess.rawValue] as! Bool == true {
//                   // let data = responseData?[APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
//
//                    callback(responseData , nil)
//                }
//                else{
//                    callback(responseData, responseData!["error"] as? String)
//                }
//            }
//
//        }
//    }
//    //MARK:- Report a bug
//
//    func reportBug(detail:Dictionary<String,AnyObject>, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
//        userRemoteReplicator.reportBug(detail:detail) { (responseData, error) in
//            if responseData != nil{
//                if responseData![APIConstants.isSuccess.rawValue] as! Bool == true {
//                    // let data = responseData?[APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
//
//                    callback(responseData , nil)
//                }
//                else{
//                    callback(responseData, responseData!["error"] as? String)
//                }
//            }
//
//        }
//
//    }
//
//
//  //MARK:- UserManualCheckIn/CheckOut
//  func userManualAttendence(detail:Dictionary<String,Any>, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
//    userRemoteReplicator.manualCheckInCheckOut(details:detail) { (responseData, error) in
//      if responseData != nil{
//        if responseData![APIConstants.isSuccess.rawValue] as! Bool == true {
//          // let data = responseData?[APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
//          callback(responseData , nil)
//        }
//        else{
//          callback(responseData, responseData!["error"] as? String)
//        }
//      }
//
//    }
//
//  }
//
//
//
//    //MARK:- Getting External token for AMS
//
//    func getExternalToken(callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
//        userRemoteReplicator.getExternalToken { (responseData, error ) in
//            if responseData != nil{
//                if responseData![APIConstants.isSuccess.rawValue] as! Bool == true {
//                    let data = responseData?["data"]
//                    Constants.kUserDefaults.set(data?["token"]!, forKey: appConstants.token)
//                    Constants.kUserDefaults.set(data?["id"]!, forKey: appConstants.id)
//                    Constants.kUserDefaults.setValue(true, forKey: appConstants.alreadyLoggedIn)
//                    let empty = NSNull()
//                    if let name = data?["name"] as? String, !name.isEqual(empty), name != ""  {
//                        Constants.kUserDefaults.set(name , forKey: "name")
//
//                    }else{
//                        Constants.kUserDefaults.set("" , forKey: "name")
//
//                    }
//                    if let designation = data?["designation"] as? String, !designation.isEqual(empty) , designation != "" {
//                        Constants.kUserDefaults.set(designation , forKey: "designation")
//                    }else{
//                        Constants.kUserDefaults.set("" , forKey: "designation")
//
//                    }
//                    if let picUrls = data?["picUrls"] as? String, !picUrls.isEqual(empty), picUrls != ""  {
//                        Constants.kUserDefaults.set(picUrls as String, forKey: "picUrls")
//                    }
//                    if let email = data?["email"] as? String, !email.isEqual(empty) , email != "" {
//                        Constants.kUserDefaults.set(email as String, forKey: "email")
//                    }else{
//                        Constants.kUserDefaults.set("" , forKey: "email")
//
//                    }
//                    if let phone = data?["phone"] as? String, !phone.isEqual(empty) , phone != "" {
//                        Constants.kUserDefaults.set(phone as String, forKey: "phone")
//                    }else{
//                        Constants.kUserDefaults.set("" , forKey: "phone")
//
//                    }
//                    if let code = data?["code"] as? String, !code.isEqual(empty) , code != "" {
//                        Constants.kUserDefaults.set(code as String, forKey: "code")
//                    }else{
//                        Constants.kUserDefaults.set("" , forKey: "code")
//
//                    }
//
//
//                    callback(responseData , nil)
//                }
//                else{
//                    callback(responseData, responseData!["error"] as? String)
//                }
//            }
//
//        }
//
//    }
//
//    //MARK:- Sign in with tunnel
//    func signInWithTunnel(callback:@escaping(_ responsedata: Dictionary<String,AnyObject>?, _ error: String?) -> Void){
//        let tokenValue = Constants.kUserDefaults.value(forKey: appConstants.orgToken)
//        Constants.kUserDefaults.set(tokenValue, forKey: appConstants.loggedInExternalOrgToken)
//        Constants.kUserDefaults.set(tokenValue, forKey: appConstants.token)
//        userRemoteReplicator.signInWithTunnel { (responseData, error ) in
//            if responseData != nil{
//                if responseData![APIConstants.isSuccess.rawValue] as! Bool == true {
//                    let data = responseData?["data"]
//                    Constants.kUserDefaults.set(data?["token"]!, forKey: appConstants.token)
//                    Constants.kUserDefaults.set(data?["id"]!, forKey: appConstants.id)
//                    Constants.kUserDefaults.setValue(true, forKey: appConstants.alreadyLoggedIn)
//                    let empty = NSNull()
//                    if let name = data?["name"] as? String, !name.isEqual(empty), name != ""  {
//                        Constants.kUserDefaults.set(name , forKey: "name")
//
//                    }else{
//                        Constants.kUserDefaults.set("" , forKey: "name")
//
//                    }
//                    if let designation = data?["designation"] as? String, !designation.isEqual(empty) , designation != "" {
//                        Constants.kUserDefaults.set(designation , forKey: "designation")
//                    }else{
//                        Constants.kUserDefaults.set("" , forKey: "designation")
//
//                    }
//                    if let picUrls = data?["picUrls"] as? String, !picUrls.isEqual(empty), picUrls != ""  {
//                        Constants.kUserDefaults.set(picUrls as String, forKey: "picUrls")
//                    }
//                    if let email = data?["email"] as? String, !email.isEqual(empty) , email != "" {
//                        Constants.kUserDefaults.set(email as String, forKey: "email")
//                    }else{
//                        Constants.kUserDefaults.set("" , forKey: "email")
//
//                    }
//                    if let phone = data?["phone"] as? String, !phone.isEqual(empty) , phone != "" {
//                        Constants.kUserDefaults.set(phone as String, forKey: "phone")
//                    }else{
//                        Constants.kUserDefaults.set("" , forKey: "phone")
//
//                    }
//                    if let code = data?["code"] as? String, !code.isEqual(empty) , code != "" {
//                        Constants.kUserDefaults.set(code as String, forKey: "code")
//                    }else{
//                        Constants.kUserDefaults.set("" , forKey: "code")
//
//                    }
//
//
//                    callback(responseData , nil)
//                }
//                else{
//                    callback(responseData, responseData!["error"] as? String)
//                }
//            }
//
//        }
//
//    }

    
    
    // MARK: Create userProfile
    
    /**
     Create user profile, and persist it to Datastore via Worker(minion),
     that synchronizes with Main context.
     
     - Parameter userProfileDetails: <Dictionary<String, AnyObject> A single Profile to be persisted to the Datastore.
     - Returns: Void
     */
    
    
//    //MARK:-- Get My Profile From Remote
//    func getRemoteMyProfile(callback:(_ responseData:Dictionary<String,AnyObject>?,_ error:String?) -> Void )
//    {
//        profileRemoteAPI.getProfileByProfileId("my") { (Data, error) -> Void in
//            if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
//                let userProfile = Data![APIConstants.data.rawValue] as! Dictionary<String, AnyObject>
//                Constants.kUserDefaults.setObject(userProfile[UserAttributes.name.rawValue] as? String , forKey: UserAttributes.name.rawValue)
//                self.userLocalAPI.saveUserProfile(userProfile)
//                Constants.kUserDefaults.setBool(true, forKey: "isProfileCompleted")
//                callback(responseData:userProfile, error: nil)
//            }
//            else{
//                callback(responseData: Data!, error: "error")
//            }
//            
//        }
//    }
    
    

}
