//
//  ChatsAPI.swift
//  DrinkchopBartender
//
//  Created by Gurpreet Gulati on 15/05/19.
//  Copyright © 2019 Temp. All rights reserved.
//

import Foundation


class ChatsAPI{

    private let ChatsRemoteReplicatorr: ChatsRemoteReplicator!
//Utilize Singleton pattern by instanciating QuestionAPI only once.
class var sharedInstance: ChatsAPI {
    struct Singleton {
        static let instance = ChatsAPI()
    }
    return Singleton.instance
}

init(){
    self.ChatsRemoteReplicatorr = ChatsRemoteReplicator()
    }


//MARK:-- Get Chat list
func getchatlist(callback:@escaping (_ responseData:Dictionary<String,AnyObject>,_ error:String?) -> Void )
{
    ChatsRemoteReplicatorr.getchatlist() { (Data, error) in
        if Data![APIConstants.isSuccess.rawValue] as! Bool == true {
            callback(Data! , nil)
        }
        else{
            print("Getting Error")
            
        }
        
    }
}
    
    
    func createChatAndGetID(chatDetails: [String:Any] , callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, Any>? , _ error: String? ) -> Void)   {
        
        ChatsRemoteReplicatorr.createChatAndGetID(chatDetials: chatDetails ) { (responseData, error) -> Void in
            if responseData != nil {
                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
                    
                    callback(true,responseData,nil)
                }else{
                    callback(false,responseData,responseData!["error"] as? String)
                }
            }
            
        }
        
    }
    
    
    
    // MARK: Set count
    func setUnreadCountToZero(chatDetails: Chat, chatId : String, callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, AnyObject>? , _ error: String? ) -> Void)   {
        ChatsRemoteReplicatorr.setUnreadCountToZero(detailsOfChat: chatDetails.dictionaryRepresentation() as! Dictionary<String, AnyObject>, chatID : chatId ){ (responseData, error) -> Void in
            if responseData != nil {
                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
                    callback(true,responseData,nil)
                }else{
                    callback(false,responseData,responseData!["error"] as? String)
                }
            }
            
        }
        
    }
    
    
    // MARK: Set count
    func incrementUnreadCount(chatDetails: Chat, chatId : String, callback:@escaping (_ isSuccess:Bool , _ responseData:  Dictionary<String, AnyObject>? , _ error: String? ) -> Void)   {
        ChatsRemoteReplicatorr.incrementUnreadCount(detailsOfChat: chatDetails.dictionaryRepresentation() as! Dictionary<String, AnyObject>, chatID : chatId ){ (responseData, error) -> Void in
            if responseData != nil {
                if (responseData?[APIConstants.isSuccess.rawValue] as? Bool)! == true{
                    callback(true,responseData,nil)
                }else{
                    callback(false,responseData,responseData!["error"] as? String)
                }
            }
            
        }
        
    }
    
    
    
    
    
    
    
    
}
