//
//  MessagesVC.swift
//  DrinkchopBartender
//
//  Created by anmoldeep singh on 06/01/19.
//  Copyright © 2019 Temp. All rights reserved.
//

import UIKit
import SVProgressHUD
import Toast_Swift
import SDWebImage

class MessagesVC: UIViewController {
    
    @IBOutlet weak var mMessagestableview: UITableView!

    var chatModelArray : [Chat]!
    var chatsAPI : ChatsAPI!
    var refreshControl = UIRefreshControl()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
        self.chatsAPI = ChatsAPI.sharedInstance
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        refreshControl.attributedTitle = NSAttributedString(string: "PLEASE WAIT",attributes: attributes)
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        mMessagestableview.addSubview(refreshControl)
        print("my id is \(Constants.kUserDefaults.value(forKey: appConstants.id) ?? "NO ID FOUND")")
        self.getAllChatsList()
        
    }
 
    @objc func refresh(sender:AnyObject) {
        self.getAllChatsList()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.getAllChatsList()
    }
    // Hode Keyboard on touch
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    @IBAction func newGroupChatBtn(_ sender: Any) {
        let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "AddMembersVCID") as! AddMembersVC
        self.navigationController?.pushViewController(destinationvc, animated: true)
    }
    @IBAction func newChatBtn(_ sender: Any) {
        let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "SearchNewUserVC") as! SearchNewUserVC
        self.navigationController?.pushViewController(destinationvc, animated: true)
    }
    
    @IBAction func mBackbtnAct(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    //MARK: GET CHAT LIST
    func getAllChatsList () {
        SVProgressHUD.show()
        chatsAPI.getchatlist { (data, error) in
            if data[APIConstants.isSuccess.rawValue] as! Bool == true {
                if error == nil {
                    self.refreshControl.endRefreshing()
                    let chatList = data[APIConstants.items.rawValue] as! NSArray
                    self.chatModelArray = [Chat]()
                    self.chatModelArray = Chat.modelsFromDictionaryArray(array: chatList)
                    self.mMessagestableview.delegate = self
                    self.mMessagestableview.dataSource = self
                    self.mMessagestableview.reloadData()
                    SVProgressHUD.dismiss()
                }
                    
                else {
                    SVProgressHUD.dismiss()
                    self.view.makeToast("Something Went Wrong")
                }
            }
                
            else {
                SVProgressHUD.dismiss()
                self.view.makeToast("Something Went Wrong")
            }
        }
    }
    
    
    // Hide Keyboard on touch

}

extension MessagesVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if chatModelArray == nil
        {
            return 0
        }
        else
        {
            return chatModelArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = mMessagestableview.dequeueReusableCell(withIdentifier: "messageCell")as! messageCell
        let chattt = self.chatModelArray[indexPath.row]
        
        if chatModelArray[indexPath.row].chatType == "normal"{
            
            cell.detailLabel.text = chattt.lastMessage ?? "NA"
            let timeElapsedSince = chattt.updatedAt?.utcStringToNumberOfHoursTillNow ?? 000
            cell.dateLabel.text = chattt.updatedAt?.utcStringToJustTime
            if timeElapsedSince > 24  {
                cell.dateLabel.text = chattt.updatedAt?.utcStringToSmartDate
            }
            //            if timeElapsedSince < 24  {
            //                cell.mDateTimeLbl.text = String(describing: timeElapsedSince) + " hrs"
            //            }
            //            else {
            //                cell.mDateTimeLbl.text = chattt.updatedAt?.utcStringToSmartDate
            //            }
            
            
            
            
            if let singleWholeChatArry_UserData = chatModelArray[indexPath.row].participants{
                for i in singleWholeChatArry_UserData {
                    if let userrs = i.user {
                        for j in userrs {
                            if j.id != Constants.kUserDefaults.value(forKey: appConstants.id) as! String {
                                cell.NameLabel.text = j.firstName ?? "NA"
                                if j.imgUrl != nil { cell.userPic.sd_setImage(with: URL.init(string:((j.imgUrl!.httpsExtend))), placeholderImage: #imageLiteral(resourceName: "CircleFill"))}
                                
                            }
                            
                        }
                    }
                }
            }
            
        }
            
        else if chatModelArray[indexPath.row].chatType == "group" {
            cell.userPic.image = #imageLiteral(resourceName: "icon_group")
            cell.NameLabel.text = chattt.groupName ?? "NG"
            cell.detailLabel.text = chattt.lastMessage ?? "NM"
            let timeElapsedSince = chattt.updatedAt?.utcStringToNumberOfHoursTillNow ?? 000
            cell.dateLabel.text = chattt.updatedAt?.utcStringToJustTime
            if timeElapsedSince > 24  {
                cell.dateLabel.text = chattt.updatedAt?.utcStringToSmartDate
            }
            
            //            if timeElapsedSince < 24  {
            //                cell.mDateTimeLbl.text = String(describing: timeElapsedSince) + " hrs"
            //            }
            //
            //            else {
            //                cell.mDateTimeLbl.text = chattt.updatedAt?.utcStringToSmartDate
            //
            //            }
            
        }
        
        print("Existing Chats' Id's are : \(chattt.id)")
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "MessagesChatVCID") as! MessagesChatVC
//        self.navigationController?.pushViewController(destinationvc, animated: true)
        var otherUserId : String?
        var otherUserName : String?
        var userIdsInAGroup =  [String]()
        var otherUserImgUrl : String?
        var allUsersInChatDetailsMdlArray = [User]()
        
        let chatt = self.chatModelArray[indexPath.row]
        
        guard let singleChatArray_UsersData = self.chatModelArray[indexPath.row].participants
            else {
                self.view.makeToast("Something Went Wrong")
                return
        }
        
        for i in singleChatArray_UsersData {
            
            if let userss = i.user {
                
                
                for j in userss {
                    print("J.ID:\(j.id)")
                    allUsersInChatDetailsMdlArray.append(j) //FOR GROUP USE
                    
                    guard let chatType = chatt.chatType else { self.view.makeToast ("Chat type not found")
                        return }
                    
                    
                    
                    switch chatType{
                    case "normal":
                        if j.id != (Constants.kUserDefaults.value(forKey: appConstants.id) as! String) {
                            otherUserId = j.id
                            otherUserName = j.firstName
                            otherUserImgUrl = j.imgUrl
                            break
                        }
                    case "group":
                        userIdsInAGroup.append(j.id!)
                        if chatt.groupName != nil {otherUserName = chatt.groupName} else {otherUserName = j.groupName}
                        
                        if let outerModelUserId = chatt.user!.id {
                            if let index = userIdsInAGroup.index(of: outerModelUserId) {
                                userIdsInAGroup.remove(at: index)
                            }}
                        
                        break
                        
                    default:
                        break
                        
                    } //SWITCH CLOSED
                    
                }
                
            }
            
        }
        
        let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "MessagesChatVCID") as! MessagesChatVC
        
        if chatt.chatType == "normal" {
            if otherUserId != nil && otherUserName != nil {
                destinationvc.otherUserID = otherUserId
                destinationvc.otherUserName = otherUserName
                destinationvc.otherUserProfilePicUrl = otherUserImgUrl
                destinationvc.typeOfChat = "normal"
                if let chattIdd = chatt.id {
                    print("Id Of This Chat is: \(chattIdd)")
                    destinationvc.idOfTheChat = chattIdd
                    destinationvc.chatAlreadyExists = true
                }
                self.navigationController?.pushViewController(destinationvc, animated: true)
                return
                
            }}
            
        else if chatt.chatType == "group" {
            var ADMINId : String?
            
            if otherUserName != nil && userIdsInAGroup != nil{
                destinationvc.otherUserName = otherUserName
                destinationvc.idsOfUsersInTheGroup = userIdsInAGroup
                destinationvc.typeOfChat = "group"
                destinationvc.usersInGroupMdlArray = allUsersInChatDetailsMdlArray
                
                
                let chatMdl = chatt.participants
                for j in chatMdl! {
                    if j.isAdmin == true {
                        let adminId = j.user![0].id
                        ADMINId = adminId!
                        print("ADMINid: \(ADMINId)")
                    }
                    
                }
                
                
                if let chattIdd = chatt.id, let Id_Admin = ADMINId{
                    print("Id Of This Chat is: \(chattIdd)")
                    destinationvc.idOfTheChat = chattIdd
                    destinationvc.otherUserID = Id_Admin
                    destinationvc.groupAdminId = Id_Admin
                    destinationvc.chatAlreadyExists = true
                }
                
                
                
                
                
                self.navigationController?.pushViewController(destinationvc, animated: true)
                return
            }}
    }
}
class messageCell: UITableViewCell {
   
    @IBOutlet weak var userPic: UIImageView!
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

}
