//
//  EditEventVC.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 22/02/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit

class EditEventVC: UIViewController {
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var zipCodeTF: UITextField!
    @IBOutlet weak var stateTF: UITextField!
    @IBOutlet weak var addressTF: UITextField!
    @IBOutlet weak var twitterLinkTF: UITextField!
    @IBOutlet weak var facebookLinkTF: UITextField!
    @IBOutlet weak var eventDescriptionTextView: UITextView!
    @IBOutlet weak var eventPicture: UIButton!
    @IBOutlet weak var eventTimeToTF: UITextField!
    @IBOutlet weak var eventTimeFromTF: UITextField!
    @IBOutlet weak var eventNameTF: UITextField!
    @IBOutlet weak var eventDateTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func selectEventDateBtn(_ sender: Any) {
    }
    
    @IBAction func updateBtn(_ sender: Any) {
    }
    @IBAction func backBtn(_ sender: Any) {
    navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
