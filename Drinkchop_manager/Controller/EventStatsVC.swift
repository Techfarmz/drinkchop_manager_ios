//
//  EventStatsVC.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 22/02/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit
import Charts

class EventStatsVC: UIViewController {
    
    @IBOutlet weak var clickChart: PieChartView!
    @IBOutlet weak var frmaleColour: UIView!
    @IBOutlet weak var otherColour: UIView!
    @IBOutlet weak var userColour: UIView!
    @IBOutlet weak var timeColour: UIView!
    @IBOutlet weak var maleColour: UIView!
    @IBOutlet weak var goingChart: PieChartView!
    @IBOutlet weak var clickedGoingChart: PieChartView!
    @IBOutlet weak var clickFrequencyChart: LineChartView!
    @IBOutlet weak var goingFrequencyChart: LineChartView!
    @IBOutlet weak var cameFrequencyChart: LineChartView!
    @IBOutlet weak var viewCountLbl: UILabel!
    @IBOutlet weak var clickCountLbl: UILabel!
    @IBOutlet weak var goingCountLbl: UILabel!
    
    
    var goingPieChartValue = [Double]()
    var clicksPieChartValue = [Double]()
     var goingdataPoints = ["male", "Female", "Others"]
    var pieChartdataPoints = ["male", "Female", "Others"]
    var dataPoints = ["users", "Time"]
    var frequencyClickData = [Double]()
    var frequencyGoingData = [Double]()
    var frequencyCameData = [Double]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ChartDataApi()
        maleColour.layer.cornerRadius = maleColour.frame.size.width/2
        maleColour.clipsToBounds = true
        frmaleColour.layer.cornerRadius = frmaleColour.frame.size.width/2
        frmaleColour.clipsToBounds = true
        otherColour.layer.cornerRadius = otherColour.frame.size.width/2
        otherColour.clipsToBounds = true
        userColour.layer.cornerRadius = userColour.frame.size.width/2
        userColour.clipsToBounds = true
        timeColour.layer.cornerRadius = timeColour.frame.size.width/2
        timeColour.clipsToBounds = true
        // Do any additional setup after loading the view.
    }
    @IBAction func backBtn(_ sender: Any) {
    navigationController?.popViewController(animated: true)
    }
    func goingFrequencysetLineChart(dataPoints: [String], values: [Double])
    {
        var lineChartEntry = [ChartDataEntry]()
        for i in 0..<dataPoints.count
        {
            print(i)
            let dataEntry = ChartDataEntry(x:Double(i), y:values[i])
            print(i)
            lineChartEntry.append(dataEntry)
            
        }
          print(self.frequencyGoingData)
        let line1 = LineChartDataSet(values: lineChartEntry, label: " ")
        line1.colors = [NSUIColor.yellow]
        line1.circleColors = [NSUIColor.yellow]
        line1.circleHoleColor = NSUIColor.yellow
        line1.valueColors = [NSUIColor.white]
        self.goingFrequencyChart.backgroundColor = .clear
        self.goingFrequencyChart.gridBackgroundColor = .clear
        self.goingFrequencyChart.xAxis.drawGridLinesEnabled = false
        
        self.goingFrequencyChart.rightAxis.enabled = false
        self.goingFrequencyChart.leftAxis.labelTextColor = UIColor.init(displayP3Red: 255.0, green: 172.0, blue: 0.0, alpha: 1.0)
        self.goingFrequencyChart.xAxis.enabled = false
        //  self.mLineCHartView.rightAxis.drawLabelsEnabled = false
        self.goingFrequencyChart.legend.enabled = false
        let data = LineChartData()
        data.addDataSet(line1)
        goingFrequencyChart.data = data
        
        
    }
    
    func fromGoingsetPieChart(dataPoints: [String], values: [Double]) {

        //        self.mPieChartView.maxAngle = 180
        //        self.mPieChartView.rotationAngle = 180
        self.clickedGoingChart.legend.enabled = false
        self.clickedGoingChart.chartDescription?.enabled = false
        self.clickedGoingChart.drawHoleEnabled = false
        self.clickedGoingChart.rotationEnabled = false
        self.clickedGoingChart.rotationAngle = 0
        self.clickedGoingChart.isUserInteractionEnabled = false
        self.clickedGoingChart.setExtraOffsets(left: -200, top: 0, right: -200, bottom: 0)
        self.clickedGoingChart.setNeedsDisplay()
        self.clickedGoingChart.contentMode = .scaleAspectFill
        self.clickedGoingChart.notifyDataSetChanged()
        self.clickedGoingChart.usePercentValuesEnabled = true


        var dataEntries: [ChartDataEntry] = []

        for i in 0...dataPoints.count - 1  {
            let dataEntry = ChartDataEntry(x: Double(i), y: values[i], data: dataPoints[i] as AnyObject)
            dataEntries.append(dataEntry)
            print(dataEntries)
        }

        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "")
        pieChartDataSet.selectionShift = 0.0
        pieChartDataSet.drawValuesEnabled = true
        pieChartDataSet.xValuePosition = .insideSlice
        pieChartDataSet.yValuePosition = .insideSlice

        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 1
        formatter.multiplier = 1.0


        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        self.clickedGoingChart.data = pieChartData
        pieChartData.setValueFormatter(DefaultValueFormatter(formatter:formatter))

            let colors: [UIColor] = [UIColor.init(red:0.99, green:0.67, blue:0.19, alpha:1.0), UIColor.init(red:0.71, green:0.24, blue:0.15, alpha:1.0),UIColor.init(red:0, green:249, blue:0, alpha:0.8), ]
    pieChartDataSet.colors = colors
    }
    
    func fromClickssetPieChart(dataPoints: [String], values: [Double]) {
        
        //        self.mPieChartView.maxAngle = 180
        //        self.mPieChartView.rotationAngle = 180
        self.clickChart.legend.enabled = false
        // self.mpieChartView.animate(xAxisDuration: 1.0)
        self.clickChart.chartDescription?.enabled = false
        self.clickChart.drawHoleEnabled = false
        self.clickChart.rotationEnabled = false
        self.clickChart.rotationAngle = 0
        self.clickChart.isUserInteractionEnabled = false
        self.clickChart.setExtraOffsets(left: -200, top: 0, right: -200, bottom: 0)
        self.clickChart.setNeedsDisplay()
        self.clickChart.contentMode = .scaleAspectFill
        self.clickChart.notifyDataSetChanged()
        
        self.clickChart.usePercentValuesEnabled = true
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0...dataPoints.count - 1  {
            let dataEntry = ChartDataEntry(x: Double(i), y: values[i], data: dataPoints[i] as AnyObject)
            dataEntries.append(dataEntry)
            
        }
          print(dataEntries)
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "")
        pieChartDataSet.selectionShift = 0.0
        pieChartDataSet.drawValuesEnabled = true
        pieChartDataSet.xValuePosition = .insideSlice
        pieChartDataSet.yValuePosition = .insideSlice
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 1
        formatter.multiplier = 1.0
        
        
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        self.clickChart.data = pieChartData
        pieChartData.setValueFormatter(DefaultValueFormatter(formatter:formatter))
        print(pieChartData)
        
        let colors: [UIColor] = [UIColor.init(red:0.99, green:0.67, blue:0.19, alpha:1.0), UIColor.init(red:0.71, green:0.24, blue:0.15, alpha:1.0),UIColor.init(red:0, green:249, blue:0, alpha:0.8), ]
        pieChartDataSet.colors = colors
    }
    
    
    func clicksFrequencysetLineChart(dataPoints: [String], values: [Double])
    {
        print(dataPoints)
          print(self.frequencyClickData)
        var lineChartEntry = [ChartDataEntry]()
        for i in 0..<dataPoints.count
        {
            print(i)
            let dataEntry = ChartDataEntry(x:Double(i), y:values[i])
            print(i)
            lineChartEntry.append(dataEntry)
            
        }
      
        let line1 = LineChartDataSet(values: lineChartEntry, label: " ")
        line1.colors = [NSUIColor.red]
        line1.circleColors = [NSUIColor.red]
        line1.circleHoleColor = NSUIColor.red
        line1.valueColors = [NSUIColor.white]
        self.clickFrequencyChart.backgroundColor = .clear
        self.clickFrequencyChart.gridBackgroundColor = .clear
        self.clickFrequencyChart.xAxis.drawGridLinesEnabled = false
        
        self.clickFrequencyChart.rightAxis.enabled = false
        self.clickFrequencyChart.leftAxis.labelTextColor = UIColor.init(displayP3Red: 255.0, green: 172.0, blue: 0.0, alpha: 1.0)
        self.clickFrequencyChart.xAxis.enabled = false
        //  self.mLineCHartView.rightAxis.drawLabelsEnabled = false
        self.clickFrequencyChart.legend.enabled = false
        let data = LineChartData()
        data.addDataSet(line1)
        clickFrequencyChart.data = data
        
        
    }
    
    func ChartDataApi() {
        let userId = Constants.kUserDefaults.value(forKey: appConstants.userId) as? String
        UserAPI().ChartData(userId: userId!,pageNo: 0) { (data, error) in
            if data[APIConstants.isSuccess.rawValue] as! Bool == true {
               let jsonData = data["data"] 
                print(jsonData)
               
                let goingCount = jsonData?["goingCount"] as! Int
                let viewCount = jsonData?["viewCount"] as! Int
                let clickCount = jsonData?["clickCount"] as! Int
                let frequency = jsonData?["frequency"] as! NSArray
                let totalFemaleClick = jsonData?["totalFemaleClick"] as! Double
                 let totalFemaleGoing = jsonData?["totalFemaleGoing"] as! Double
                 let totalMaleClick = jsonData?["totalMaleClick"] as! Double
                 let totalMaleGoing = jsonData?["totalMaleGoing"] as! Double
                 let totalOtherClick = jsonData?["totalOtherClick"] as! Double
                 let totalOtherGoing = jsonData?["totalOtherGoing"] as! Double
                
                
                self.goingCountLbl.text = goingCount.description
                self.viewCountLbl.text = viewCount.description
                self.clickCountLbl.text = clickCount.description
                
                
                self.frequencyClickData.removeAll()
                for i in frequency
                {
                    print(i)
                    let fData = i as! NSDictionary
                    print(fData)
                    let time = fData["time"] as! String
                    print(time)
                    let eventTime = time.utcStringToSmartTime1
                    print(eventTime)
                    let clickCount = fData["clickCount"] as! Double
                    print(clickCount)
                    let goingCount = fData["goingCount"] as! Double
                    print(goingCount)
                    
                    
                    if eventTime == "8:00 AM"
                    {
                        self.frequencyClickData.append(clickCount)
                        self.frequencyGoingData.append(goingCount)
                    }
                    if eventTime == "12:00 PM"
                    {
                        self.frequencyClickData.append(clickCount)
                        self.frequencyGoingData.append(goingCount)
                    }
                    if eventTime == "4:00 PM"
                    {
                        self.frequencyClickData.append(clickCount)
                        self.frequencyGoingData.append(goingCount)                        
                    }
                }
               self.clicksPieChartValue = [totalMaleClick]
                self.clicksPieChartValue.append(totalFemaleClick)
                self.clicksPieChartValue.append(totalOtherClick)
                
                self.fromClickssetPieChart(dataPoints: self.pieChartdataPoints, values: self.clicksPieChartValue)
                self.goingPieChartValue = [totalMaleGoing]
                self.goingPieChartValue.append(totalFemaleGoing)
                self.goingPieChartValue.append(totalOtherGoing)

                
                
                    self.fromGoingsetPieChart(dataPoints: self.goingdataPoints, values: self.goingPieChartValue)
                
                
                print(self.frequencyClickData)
                self.clicksFrequencysetLineChart(dataPoints: self.dataPoints, values: self.frequencyClickData)
                self.goingFrequencysetLineChart(dataPoints: self.dataPoints, values: self.frequencyGoingData)
                
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }

}
