//
//  EventDetailsVC.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 21/02/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit
import SDWebImage

class EventDetailsVC: UIViewController {

    @IBOutlet weak var eventStatus: UILabel!
    @IBOutlet weak var addressTF: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var eventTime: UILabel!
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var eventImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        GeteventsDetailsApi()
        // Do any additional setup after loading the view.
    }
    @IBAction func backBtn(_ sender: Any) {
    navigationController?.popViewController(animated: true)
    }
    @IBAction func seeStatsBtn(_ sender: Any) {
        let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "EventStatsVC") as! EventStatsVC
        self.navigationController?.pushViewController(destinationvc, animated: true)
    }
    @IBAction func modifyBtn(_ sender: Any) {
        let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "CreateNewEventVC") as! CreateNewEventVC
        destinationvc.isEditEvent = true
        self.navigationController?.pushViewController(destinationvc, animated: true)
    }
    @IBAction func fbBtn(_ sender: Any) {
    }
    @IBAction func twitterBtn(_ sender: Any) {
    }
    
    // Get events Details Api()
    func GeteventsDetailsApi() {
        let userId = Constants.kUserDefaults.value(forKey: appConstants.userId) as? String
        UserAPI().getEventsDetilsId(userId: userId!,pageNo: 0) { (data, error) in
            if data[APIConstants.isSuccess.rawValue] as! Bool == true {
                if let items = data["data"] as? [String:AnyObject]
                {
                    print(items)
                    var name = items["name"] as? String
                    var status = items["status"] as? String
                   
                    self.eventTitle.text = name
                    self.eventStatus.text = status
                    let availbilityStart = items["availbilityStart"] as! String
                    
                    let dateFormatter = DateFormatter()
                    // Fetch Start Time          "2019-06-10 04:41:00"
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let updatedAt = dateFormatter.date(from: availbilityStart) // "Jun 5, 2016, 4:56 PM"
                    print(updatedAt)
                    dateFormatter.dateFormat = "h:mm a"
                  //  dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
                    let strtTime = dateFormatter.string(from: updatedAt!)
                    print(strtTime)

                    // Fetch end Time
                    let availbilityTill = items["availbilityTill"] as! String
                    
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let updatedEndTimeAt = dateFormatter.date(from: availbilityTill) // "Jun 5, 2016, 4:56 PM"
                    print(updatedEndTimeAt)
                    dateFormatter.dateFormat = "h:mm a"
               //     dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
                    let endTime = dateFormatter.string(from: updatedEndTimeAt!)
                    print(endTime)
                    
                    let combineTime = "Open:\(strtTime) to \(endTime)"
                    self.eventTime.text = combineTime
                    print(combineTime)
                    var description = items["description"] as? String
                    var eventadress = items["eventadress"] as? NSDictionary
                   
                    
                    var address = eventadress?["address"] as? String
                    var city = eventadress?["city"] as? String
                    var state = eventadress?["state"] as? String
                    var zipCode = eventadress?["zipCode"] as? Int
                    
                    if var imgUrl = items["imgUrl"] {
                    print(imgUrl)
                    
                        self.eventImage.sd_setImage(with: URL(string: imgUrl as! String), placeholderImage: UIImage(named: "eventDummy"))
                    }
                    if description == nil
                    {
                        description = ""
                    }
                    self.descriptionTextView.text = description
                    
                    var combineAddress = "\(address!),\(city!),\(state!),\(zipCode!)"
                    print(combineAddress)
                    self.addressTF.text = combineAddress
                }
                
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }
    
}
