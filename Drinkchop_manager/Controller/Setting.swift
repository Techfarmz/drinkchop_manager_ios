//
//  SettingNotificationVC.swift
//  drinkchop-ios
//
//  Created by Tech Farmerz on 04/12/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

import UIKit

class SettingNotificationVC: UIViewController {
    var sectionarr = ["","VIBRATE"]
    var textarr = [["Message"],["Message"]]
   // var textInfoarr = [["","","","Near expiration", "Near expiration"],["","","","Near expiration","Near expiration"]]
    @IBOutlet weak var mNotiftableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func mBackBtnAct(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }


}
extension SettingNotificationVC: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionarr.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRect(x: 0,y: 0 , width: self.mNotiftableView.frame.size.width ,height: 40))
        headerView.backgroundColor = #colorLiteral(red: 0.1725490196, green: 0.1019607843, blue: 0, alpha: 1)
        
        let titlelbl = UILabel(frame: CGRect(x: 10,y: headerView.frame.size.height/2 - 15 , width: 110 ,height: 30))
        titlelbl.textColor = .white
        titlelbl.font = UIFont(name: "HelveticaNeue-Bold", size: 18.0)!
        titlelbl.text = sectionarr[section]
        
        headerView.addSubview(titlelbl)
        return headerView
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return textarr[section].count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
            let cell = mNotiftableView.dequeueReusableCell(withIdentifier: "SettingCell")as! SettingCell
            cell.objectName.text = textarr[indexPath.section][indexPath.row]
       //     cell.info.text = textInfoarr[indexPath.section][indexPath.row]
            return cell
     
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }
        return 40
    }
}
class SettingCell: UITableViewCell {
    @IBOutlet weak var objectName: UILabel!
    @IBOutlet weak var info: UILabel!
    @IBOutlet weak var stateSwitch: UISwitch!
    
}
