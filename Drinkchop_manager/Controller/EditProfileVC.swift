//
//  EditProfileVC.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 19/02/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit
import SwiftyPickerPopover
import SVProgressHUD
import NotificationBannerSwift
import CropViewController
import SDWebImage


class EditProfileVC: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate {

    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var profilePicView: UIView!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var genderTF: UITextField!
    @IBOutlet weak var phoneNumberTF: UITextField!
    @IBOutlet weak var emailUserNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var userSinceLbl: UILabel!

    private var fileUploadAPI:FileUpload!
    var pickerView = UIPickerView()
     let Genders = ["male","female"]
    private var userApi : UserAPI!
    var user: User?
   var validator = Validators()
    
    var picUrl:String?
    var NameVar : String?
    var GenderVar : String?
    var passwordVAr : String?
    var emailVAr : String?
    var phoneVAr : String?
    var confirmPasswordVar : String?
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getUserDetails()


        self.fileUploadAPI = FileUpload.sharedInstance
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        genderTF.delegate = self
        genderTF.inputView = pickerView
        self.userApi = UserAPI.sharedInstance
        
        profilePic.layer.cornerRadius = profilePic.frame.size.width/2
        profilePic.clipsToBounds = true
        
        profilePicView.layer.cornerRadius = profilePicView.frame.size.width/2
        profilePicView.clipsToBounds = true
        // Do any additional setup after loading the view.
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    @IBAction func backBtn(_ sender: Any) {
   navigationController?.popViewController(animated: true)
    }
    @IBAction func profilePictureBtn(_ sender: Any) {
        addImage()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    @IBAction func privacyBtn(_ sender: Any) {
        let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyVC") as! PrivacyVC
        self.navigationController?.pushViewController(destinationvc, animated: true)
    }
    @IBAction func termsOfServiceBtn(_ sender: Any) {
        let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "TermsVC") as! TermsVC
        self.navigationController?.pushViewController(destinationvc, animated: true)
    }
    
    @IBAction func updateBtn(_ sender: Any) {
      
  
        guard validator.validators(TF1: self.firstNameTF,fieldName: "First Name") == false ||
            validator.validators(TF1: self.lastNameTF,fieldName: "Last Name") == false ||
          validator.validators(TF1: self.emailUserNameTF,fieldName: "Email") == false || validator.validators(TF1: self.genderTF,fieldName: "Select Gender") == false || validator.validators(TF1: self.phoneNumberTF,fieldName: "Phone Number") == false
        
            
            else
            
        {
            if (phoneNumberTF.text?.count)! > 9
            {
                if passwordTF.text == confirmPasswordTF.text
                {
                    // Hit APi Here
                    self.user = User.init(dictionary: NSDictionary())
                    print(Constants.kUserDefaults.string(forKey: appConstants.userId))
                    self.user?.id  = Constants.kUserDefaults.string(forKey: appConstants.userId)
                    self.user?.firstName = firstNameTF.text
                    self.user?.lastName = lastNameTF.text
                    self.user?.email = emailUserNameTF.text
                    self.user?.password = passwordTF.text
                    self.user?.gender = genderTF.text
                    self.user?.phone = phoneNumberTF.text
                    
                    self.userUpdateProfile(userDict: user!)
                }
                    
                else
                {
                    let banner = NotificationBanner(title: "ERROR", subtitle: "Password Do Not Match", style: .danger)
                    banner.duration = 1
                    banner.show()
                }
                print("valid ph no")
            }
            else
            {
                let banner = NotificationBanner(title: "ERROR", subtitle: "Please Enter Valid Phone Number", style: .danger)
                banner.duration = 1
                banner.show()
            }
            return
        }
    
    }
   
    // Get user Details
    func getUserDetails() {
        let userId = Constants.kUserDefaults.value(forKey: appConstants.userId) as? String
        UserAPI().getAllUserData(userId: userId!,pageNo: 0) { (data, error) in
            if data[APIConstants.isSuccess.rawValue] as! Bool == true {
                let userDict = data["data"]
                let dataProfile = NSKeyedArchiver.archivedData(withRootObject: userDict)
                //    Constants.kUserDefaults.set(dataProfile, forKey: appConstants.profile)
                
                let firstName = userDict!["firstName"] as! String
                let lastName = userDict!["lastName"] as! String
                let email = userDict?["email"] as! String
                let phone = userDict?["phone"] as! String
                let gender = userDict?["gender"] as! String
                 let imgUrl = userDict?["imgUrl"] as? String
                
                if firstName != nil {
                    self.firstNameTF.text = firstName
                    self.lastNameTF.text = lastName
                    self.emailUserNameTF.text = email
                    self.phoneNumberTF.text = phone
                    self.genderTF.text = gender
                    if let myImgUrl = Constants.kUserDefaults.value(forKey: "imgUrl") as? String {
                        self.profilePic.sd_setImage(with: URL.init(string:((myImgUrl.httpsExtend))), placeholderImage: #imageLiteral(resourceName: "dummy"))
                    }
                }

            }
            else{
                print("Getting Error")
                
            }
            
        }
    }
    // MARK: User Update Function
    func userUpdateProfile(userDict:User){
        SVProgressHUD.show(withStatus: "Please Wait")
        userApi.userUpdateProfile(userDetials: userDict){ (isSuccess,response, error) -> Void in
            SVProgressHUD.dismiss()
            
            if (isSuccess){
                SVProgressHUD.dismiss()
                
                Constants.kUserDefaults.set(true, forKey:"isLogin")
                self.navigationController?.popViewController(animated: true)
            }else{
                SVProgressHUD.dismiss()
                if error != nil{
                    kAppDelegate.showNotification(text: "Something went wrong!")
                    //kAppDelegate.showNotification(text: error!)
                }else{
                    kAppDelegate.showNotification(text: "Something went wrong!")
                }
            }
            
        }
        
        
    }
// Edit Image
    func addImage() {
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        let takePic = UIAlertAction(title: "Take Photo", style: .default,handler: {
            (alert: UIAlertAction!) -> Void in
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = UIImagePickerController.SourceType.camera
            self.present(myPickerController, animated: true, completion: nil)
        
        })
        
        let choseAction = UIAlertAction(title: "Choose from Library",style: .default,handler: {
            (alert: UIAlertAction!) -> Void in
            
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(myPickerController, animated: true, completion: nil)
            
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        optionMenu.addAction(takePic)
        optionMenu.addAction(choseAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print("abc")
        guard let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            else
        {
            return
        }
        
        self.dismiss(animated: false, completion: { [weak self] in
            self?.profilePic.image = originalImage
            self?.moveToImageCropper(image: originalImage)
        })
    }

}
    // Crop Profile Image
extension EditProfileVC : CropViewControllerDelegate {
    
    private func moveToImageCropper(image: UIImage) {
        let cropController = CropViewController(croppingStyle: CropViewCroppingStyle.default, image: image)
        cropController.delegate = self
        cropController.aspectRatioPickerButtonHidden = true
        cropController.aspectRatioLockEnabled = true
        cropController.aspectRatioPreset = .presetSquare
        self.present(cropController, animated: true, completion: nil)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        self.profilePic.contentMode = .scaleAspectFill
        self.profilePic.image = image
        
        // let image = UIImage()
        let compressData = image.jpegData(compressionQuality: 0.75)
        let compressedImage = UIImage(data: compressData!)
        self.fileUploadAPI.uploadImageaaaRemote(image: compressedImage!){ dataImage, error -> Void in
            if error == nil{
                var picUrl:String?
                picUrl = dataImage
                //  self.PictureURLVar = self.picUrl
                Constants.kUserDefaults.set(dataImage, forKey: "picUrls")
            }
        }
        cropViewController.dismiss(animated: true, completion: nil)
    }
    
    
}
extension EditProfileVC : UIPickerViewDelegate, UIPickerViewDataSource {
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
            return Genders.count
        }
        
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return Genders[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
            self.genderTF.text = Genders[row]
            self.genderTF.resignFirstResponder()
        
        }
}

