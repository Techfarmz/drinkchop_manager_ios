//
//  EventsVC.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 19/02/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit

class EventsVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var eventsTableview: UITableView!
    
    var nameAry = [String]()
    var eventsStatus = [String]()
    var availbilityStartAry = [String]()
    var eventName: String!
    var getEventName: String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eventsTableview.delegate = self
        eventsTableview.dataSource = self
        self.eventsTableview.separatorStyle = .none
      self.eventsApi()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        eventsTableview.reloadData()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    @IBAction func plusBtn(_ sender: Any) {
        let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "CreateNewEventVC") as! CreateNewEventVC
        self.navigationController?.pushViewController(destinationvc, animated: true)
    }
    @IBAction func backBtn(_ sender: Any) {
    navigationController?.popViewController(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameAry.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(nameAry)
        print(availbilityStartAry)
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventsCell")as! EventsCell
        cell.eventLbl.text = nameAry[indexPath.row]
        cell.status.text = eventsStatus[indexPath.row]
        cell.dateLbl.text = availbilityStartAry[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

            let cell = tableView.cellForRow(at: indexPath) as! EventsCell
            getEventName = cell.eventLbl.text
        print(getEventName!)
            getUEventId()
        
    }
    // Get Events
    func eventsApi() {
        let userId = Constants.kUserDefaults.value(forKey: appConstants.userId) as? String
        UserAPI().getEvents(userId: userId!,pageNo: 0) { (data, error) in
            if data[APIConstants.isSuccess.rawValue] as! Bool == true {
                if let items = data["items"] as? [[String:AnyObject]]
                {
                    print(items)
                    for i in items
                    {
                        print(i)
                        let name = i["name"] as! String
                        print(name)
                        self.nameAry.append(name)
                        let eventsStatus = i["eventsStatus"] as! String
                        print(eventsStatus)
                        self.eventsStatus.append(eventsStatus)
                        let availbilityStart = i["availbilityStart"] as! String
                        
                        let dateFormatter = DateFormatter()
                                               
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        
                        let updatedAtStr = "2016-06-05T16:56:57.019+01:00"
                        let updatedAt = dateFormatter.date(from: availbilityStart) // "Jun 5, 2016, 4:56 PM"
                        print(updatedAt)
                        dateFormatter.dateFormat = "d/MM/yyyy"
                        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
                        let dateStr = dateFormatter.string(from: updatedAt!)
                        print(dateStr)
                        self.availbilityStartAry.append(dateStr)
                    }
                    self.eventsTableview.reloadData()
                }
                
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }
    
    // Get Emploee Account ID
    func getUEventId() {
        let userId = Constants.kUserDefaults.value(forKey: appConstants.userId) as? String
        UserAPI().getEvents(userId: userId!,pageNo: 0) { (data, error) in
            if data[APIConstants.isSuccess.rawValue] as! Bool == true {
                if let userDict = data["items"] as? [[String:AnyObject]]
                {
                    for i in userDict
                    {
                        self.eventName = i["name"] as! String
                        print(self.eventName)
                     print(self.eventName)
                       print(self.getEventName)
                        if self.eventName == self.getEventName
                        {
                            let id = i["id"] as! String
                            print(id)
                            Constants.kUserDefaults.set(id, forKey: "currentEventId")
                         
                            let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "EventDetailsVC") as! EventDetailsVC
                            self.navigationController?.pushViewController(destinationvc, animated: true)

                            
                        }


                        
                        
                    }
                }
                
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }
    
}
class EventsCell: UITableViewCell {
    
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var eventLbl: UILabel!
    @IBOutlet weak var status: UITextField!
    
}
