//
//  NewEmployeeAccountVC.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 21/02/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit
import SVProgressHUD
import NotificationBannerSwift
import Toast_Swift
import iOSDropDown

class NewEmployeeAccountVC: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var roleDropdown: DropDown!
    @IBOutlet weak var selectRoleTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var phoneNumberTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var firstNameTF: UITextField!
    
    private var userApi : UserAPI!
    var user: User?
    var validator = Validators()
    var dropdownValue = ["Bartender", "DoorPerson"]
    override func viewDidLoad() {
        super.viewDidLoad()
        selectRoleTF.delegate = self
        self.userApi = UserAPI.sharedInstance
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        roleDropdown.optionArray = dropdownValue
        roleDropdown.didSelect{(selectedText , index ,id) in
            self.selectRoleTF.text = selectedText
            
        }
    }

    @IBAction func dropDownBtn(_ sender: Any) {

    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder()
        return false;
    }
    @IBAction func saveBtn(_ sender: UIButton) {
        guard validator.validators(TF1: self.firstNameTF,fieldName: "First Name") == false ||
            validator.validators(TF1: self.lastNameTF,fieldName: "Last Name") == false ||
            validator.validators(TF1: self.passwordTF,fieldName: "Password") == false || validator.validators(TF1: self.selectRoleTF,fieldName: "Confirm Password") == false || validator.validators(TF1: self.emailTF,fieldName: "Email") == false || validator.validators(TF1: self.phoneNumberTF,fieldName: "Select Gender") == false
            
            
            else
            
        {
            if (phoneNumberTF.text?.count)! > 9
            {
                    // Hit APi Here
                    self.user = User.init(dictionary: NSDictionary())
                    print(Constants.kUserDefaults.string(forKey: appConstants.userId))
                
                
                self.user?.id  = Constants.kUserDefaults.string(forKey: appConstants.userId)
                    self.user?.firstName = firstNameTF.text
                    self.user?.lastName = lastNameTF.text
                    self.user?.email = emailTF.text
                    self.user?.password = passwordTF.text
                    self.user?.phone = phoneNumberTF.text
                    self.user?.deviceType = "ios"
                    self.user?.deviceId = "000"

            var roleName = String()
                if selectRoleTF.text == "Bartender"
                {
                    roleName = "bartender"
                }
                else if selectRoleTF.text == "DoorPerson"
                {
                    roleName = "doorPerson"
                }
                  self.user?.role = roleName
                    self.createNewUserApi(userDict: user!)
                
            }

            else
            {
                let banner = NotificationBanner(title: "ERROR", subtitle: "Please Enter Valid Phone Number", style: .danger)
                banner.duration = 1
                banner.show()
            }
            return
        }
        
    }
    @IBAction func backBtn(_ sender: Any) {
    navigationController?.popViewController(animated: true)
    }
    @IBAction func selectRoleDropdown(_ sender: UITextField) {
    }
    // MARK: Create New User Api
    func createNewUserApi(userDict:User){
        SVProgressHUD.show(withStatus: "Please Wait")
        userApi.createNewUserAccount(userDetials: userDict){ (isSuccess,response, error) -> Void in
            SVProgressHUD.dismiss()
            
            if (isSuccess){
                SVProgressHUD.dismiss()
                kAppDelegate.showNotification(text: "Profile Updated Successfully")
                let response = response
             
                let data = response!["data"] as! NSDictionary
                   print(data)
                let newEventId = data["id"] as! String

                Constants.kUserDefaults.set(newEventId, forKey: "newEventId")
                self.navigationController?.popViewController(animated: true)
                
            }else{
                SVProgressHUD.dismiss()
             

                    let response = response
              
                let message = response?["message"] as? String
                    print(message)
                    self.view.makeToast(message)
              
            }
            
        }
        
        
    }


}
