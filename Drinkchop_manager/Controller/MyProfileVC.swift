//
//  MyProfileVC.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 19/02/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit

class MyProfileVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var profilePicView: UIView!
    @IBOutlet weak var lastName: UILabel!
    @IBOutlet weak var firstName: UILabel!

    @IBOutlet weak var profileTableView: UITableView!
    
    var titleAry = ["Employee", "Drink", "Bar Night/Club"]
    var subTitleAry = ["Accounts", "Selection", "Selection"]
    override func viewWillAppear(_ animated: Bool) {
    //    self.getUserDetails()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let firstname = Constants.kUserDefaults.value(forKey: "firstName") as! String
        let lastNamee = Constants.kUserDefaults.value(forKey: "lastName") as! String
        
        firstName.text = firstname
        lastName.text = lastNamee
        if let myImgUrl = Constants.kUserDefaults.value(forKey: "imgUrl") as? String {
            profilePic.sd_setImage(with: URL.init(string:((myImgUrl.httpsExtend))), placeholderImage: #imageLiteral(resourceName: "dummy"))
        }
        
        
        profileTableView.delegate = self
        profileTableView.dataSource = self
        self.profileTableView.separatorStyle = .none
        profilePic.layer.cornerRadius = profilePic.frame.size.width/2
        profilePic.clipsToBounds = true
        
        profilePicView.layer.cornerRadius = profilePicView.frame.size.width/2
        profilePicView.clipsToBounds = true
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtn(_ sender: Any) {
    navigationController?.popViewController(animated: true)
    }
    
    @IBAction func profilePicBtn(_ sender: Any) {
    }
    @IBAction func editProfileBtn(_ sender: Any) {
        let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        self.navigationController?.pushViewController(destinationvc, animated: true)
    }
    @IBAction func SetCoverFeeBtn(_ sender: Any) {
        let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "CoverFees_OptionsVC") as! CoverFees_OptionsVC
        self.navigationController?.pushViewController(destinationvc, animated: true)
    }
    @objc func btnClicked(sender:UIButton!)
    {
let buttonTag = sender.tag
        if buttonTag == 0
        {
            let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "EmployeeAccountDetailVC") as! EmployeeAccountDetailVC
            self.navigationController?.pushViewController(destinationvc, animated: true)
        }
        if buttonTag == 1
        {
            let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "DrinkSelectionVCID") as! DrinkSelectionVC
            self.navigationController?.pushViewController(destinationvc, animated: true)
        }
        if buttonTag == 2
        {
            let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "BarSelectionVCID") as! BarSelectionVC
            self.navigationController?.pushViewController(destinationvc, animated: true)
        }
    }
    
    
    @IBAction func paymentMethodBtn(_ sender: Any) {
        let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentInformationVC") as! PaymentInformationVC
        self.navigationController?.pushViewController(destinationvc, animated: true)
    }
    @IBAction func retrievePaymentBtn(_ sender: Any) {
    // PaymentTransferVCID
        let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentTransferVCID") as! PaymentTransferVC
        self.navigationController?.pushViewController(destinationvc, animated: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 93
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleAry.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell")as! ProfileCell
        cell.titleLbl.text = titleAry[indexPath.row]
        cell.subtitleLbl.text = subTitleAry[indexPath.row]
        // Mark Clear user Selection
        cell.selectionStyle = .none
        cell.setBtn.tag = indexPath.row
        cell.setBtn.addTarget(self, action: #selector(MyProfileVC.btnClicked(sender:)), for: .touchUpInside)
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    // Get user Details
//    func getUserDetails() {
//        let userId = Constants.kUserDefaults.value(forKey: appConstants.userId) as? String
//        UserAPI().getAllUserData(userId: userId!,pageNo: 0) { (data, error) in
//            if data[APIConstants.isSuccess.rawValue] as! Bool == true {
//                let userDict = data["data"]
//                let dataProfile = NSKeyedArchiver.archivedData(withRootObject: userDict)
//                //    Constants.kUserDefaults.set(dataProfile, forKey: appConstants.profile)
//
//                let firstName = userDict!["firstName"] as! String
//                let lastName = userDict!["lastName"] as! String
//                let email = userDict?["email"] as! String
//                let phone = userDict?["phone"] as! String
//                let gender = userDict?["gender"] as! String
//                //  let email = userDict?["entrance"] as! String
//
//
//
//                // print(usersGivenName, username, role, gender, phone)
//                if firstName != nil {
//                    //   Constants.kUserDefaults.set(townvar, forKey: appConstants.town)
//                    //  var phoneString = String(phone)
//
//
//                    self.firstName.text = firstName
//                    self.lastName.text = lastName
//
//                }
//                let lgaVar = userDict?["lga"] as? String
//                if lgaVar != nil {
//                    Constants.kUserDefaults.set(lgaVar, forKey: appConstants.lga)
//                }
//
//                // self.NameVar = usersGivenName as? String
//                //   self.imgUrlVar = imgUrl as? String
//
//                let myUser = User.init(dictionary: userDict as! NSDictionary)
//                //  self.updateInputParams(user: myUser!)
//
//
//
//
//            }
//            else{
//                print("Getting Error")
//
//            }
//
//        }
//    }


}
class ProfileCell: UITableViewCell {
    
    @IBOutlet weak var subtitleLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var setBtn: UIButton!
    
}
