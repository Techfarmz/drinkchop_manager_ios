//
//  DrinkSelectionVC.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 13/06/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire

class DrinkSelectionVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var selectionOutlet: UIButton!
    @IBOutlet weak var baselineOutlet: UIButton!
    @IBOutlet weak var selectionTableView: UITableView!
    
    var selectedCat = String()
    var importBtnClicked = false
    var drinkAry = [String]()
    var catClicked = false
    var baselineDrinkSelectedAry = [String]()
    var drinkName = String()
    var baselineDrinksAry = NSArray()
    var baselineCatNameAry = [String]()
    var selectedDrinksDict = [String:String]()
    var sectionsWithStatesDicr = [String:String]()
      var selectedCategoryDict = [String:String]()
     var drinkCatModel : [Category]!
    var DrinktitleAry: [String] = []
    var currentsecTitle = String()
    
    var thereIsCellTapped = false
    var indexPathFromCellForRowAt = IndexPath()
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.getBaselineData()
        // allow multiselection
        selectionTableView.allowsMultipleSelection = true
        selectionTableView.separatorStyle = .none
          baselineOutlet.backgroundColor = UIColor.brown

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
     
        print(self.sectionsWithStatesDicr["SECTION1"])
        selectionTableView.delegate = self
        selectionTableView.dataSource = self
        
    }
    
    
    func checkSectionAndReturn (value: Int, noOfSections:Int) -> CGFloat {
        
        for i in 0...noOfSections {
            if value == i {
                if self.sectionsWithStatesDicr[self.currentsecTitle] == "true" {return 50} else {return 0}
            }
                
            else if value == i {
                if self.sectionsWithStatesDicr[self.currentsecTitle] == "true" {return 50} else {return 0}
            }
           
            // selectedCategoryDict
//            if value == i {
//                if self.selectedCategoryDict[self.currentsecTitle] == "true" {return 50} else {return 0}
//            }
//
//            else if value == i {
//                if self.selectedCategoryDict[self.currentsecTitle] == "true" {return 50} else {return 0}
//            }

            
        }
   return 0
    }

//    func checkSelectedCategory (value: Int, noOfSections:Int) -> CGFloat {
//
//        for i in 0...noOfSections {
//            if value == i {
//                if self.selectedCategoryDict[self.currentsecTitle] == "true" {return 50} else {return 0}
//            }
//
//            else if value == i {
//                if self.selectedCategoryDict[self.currentsecTitle] == "true" {return 50} else {return 0}
//            }
//
//            // selectedCategoryDict
//            if value == i {
//                if self.selectedCategoryDict[self.currentsecTitle] == "true" {return 50} else {return 0}
//            }
//
//            else if value == i {
//                if self.selectedCategoryDict[self.currentsecTitle] == "true" {return 50} else {return 0}
//            }
//
//
//        }
//        return 0
//    }
    
    @IBAction func backBtn(_ sender: Any) {
    navigationController?.popViewController(animated: true)
    }
    @IBAction func baselineBtn(_ sender: Any) {
    }
    @IBAction func selectionBtn(_ sender: Any) {
        let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "DrinkSelection2VCID") as! DrinkSelection2VC
        self.navigationController?.pushViewController(destinationvc, animated: false)
    }
    @IBAction func saveBtn(_ sender: Any) {
    self.importBtnClicked = true
//        DispatchQueue.main.async(execute: {
//            self.selectionTableView.reloadData()
//
//        })
self.selectionTableView.reloadData()
        // Delay Function
        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0, execute: {
            self.importSelectionApi()
            self.importBtnClicked = false

        })
      
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let currentsecTitleForRow = self.baselineCatNameAry[indexPath.section]
        print(self.DrinktitleAry)
        print(currentsecTitleForRow)
        self.currentsecTitle = currentsecTitleForRow
        print(baselineCatNameAry)
        
        return self.checkSectionAndReturn(value: indexPath.section, noOfSections: self.baselineCatNameAry.count - 1)
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "DrinkSelectionCell") as! DrinkSelectionCell
      //let headerCell = tableView.cellForRow(at: self.indexPathFromCellForRowAt) as! DrinkSelectionCell
        
        headerCell.name.text = self.baselineCatNameAry[section]
        headerCell.selectionImg.tag = section
        headerCell.dropDown.tag = section
        headerCell.dropDown.isHidden = false
        headerCell.selectionLeadingConst.constant = 21.0
        
      //  headerCell.selectionImg.currentBackgroundImage = UIImage.init(cgImage: "CircleFill")
        
        headerCell.dropDown.addTarget(self, action: #selector(DrinkSelectionVC.btnSectionClick(sender:)), for: .touchUpInside)
        
        headerCell.selectionImg.addTarget(self, action: #selector(DrinkSelectionVC.btnSelectionDrink(sender:)), for: .touchUpInside)
       
        self.currentsecTitle = self.baselineCatNameAry[section]
        print(self.currentsecTitle)
        let myCurrentSection = self.currentsecTitle
        if self.catClicked == true
        {
         if self.selectedCategoryDict[myCurrentSection]  == "false" {
        
            headerCell.selectionImg.setBackgroundImage(#imageLiteral(resourceName: "CircleBlank"), for: .normal)
        }
         else if  self.selectedCategoryDict[myCurrentSection] == "true"{
         
           headerCell.selectionImg.setBackgroundImage(#imageLiteral(resourceName: "CircleFill"), for: .normal)
            
            
        }
        }
        
//        if self.selectedCategoryDict[myCurrentSection]  == "false" {
//            headerCell.selectionImg.setImage(UIImage(named: "CircleFill.png"), for: .normal)
//
//        }
//        if self.selectedCategoryDict[myCurrentSection]  == "true" {
//            headerCell.selectionImg.setImage(UIImage(named: "CircleBlank.png"), for: .normal)
//        }
        
        
        return headerCell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.baselineCatNameAry.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        return (self.drinkCatModel[section].drinks?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = selectionTableView.dequeueReusableCell(withIdentifier: "DrinkSelectionCell")as! DrinkSelectionCell
        self.indexPathFromCellForRowAt = indexPath
         cell.dropDown.isHidden = true
         cell.name.textAlignment = .right
        
         cell.selectionLeadingConst.constant = 1.0
         view.layoutIfNeeded()
        cell.selectionStyle = .none
        
        cell.selectionImg.tag = (indexPath.section * 1000) + indexPath.row
        
        cell.selectionImg.addTarget(self, action: #selector(DrinkSelectionVC.selectDrinkClick(sender:)), for: .touchUpInside)
        print("Section is \(indexPath.section) Row is: \(indexPath.row)")
        
        
        //self.currentsecTitle = self.baselineCatNameAry[indexPath.section]
        let myCurrentSection = self.currentsecTitle
        if self.catClicked == true
        {
            print(drinkCatModel[indexPath.section].name)
        //    print(self.selectedCat)
            let drnkkMdl = drinkCatModel[indexPath.section].name
            if drnkkMdl == self.selectedCat
            {
               print(self.selectedCat)
                if self.selectedCategoryDict[self.selectedCat]  == "false"
            {

                    cell.selectionImg.setBackgroundImage(#imageLiteral(resourceName: "CircleBlank"), for: .normal)
            }
                else if  self.selectedCategoryDict[self.selectedCat] == "true"
            {
                    cell.selectionImg.setBackgroundImage(#imageLiteral(resourceName: "CircleFill"), for: .normal)
            }
            }
           }
        
      cell.name.text = self.drinkCatModel[indexPath.section].drinks![indexPath.row].name

        if self.importBtnClicked == true
        {
            let btnImage = #imageLiteral(resourceName: "CircleBlank")

            if cell.selectionImg.currentBackgroundImage == btnImage
            {

            }
            else
            {
                let alldrinksid = self.drinkCatModel[indexPath.section].drinks?[indexPath.row].id
                print(alldrinksid)
                //  let drinkId = alldrinks!["id"] as! String
               
                
                if let index = drinkAry.index(where: { $0.hasPrefix(alldrinksid!) }) {
                    print("The first name starting with \(alldrinksid) was found at \(index)")
                   
                }
                else {
                     self.drinkAry.append(alldrinksid!)
                }
                
                print("Values of drinkarray is \(drinkAry)")
            }
        }
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
//            if indexPath == lastVisibleIndexPath {
//                // do here...
//            }
//        }
//         let cell = selectionTableView.cellForRow(at: indexPath) as? DrinkSelectionCell
//        if self.importBtnClicked == true
//        {
//            let btnImage = #imageLiteral(resourceName: "CircleBlank")
//
//            if cell?.selectionImg.currentBackgroundImage == btnImage
//            {
//
//            }
//            else
//            {
//                let alldrinksid = self.drinkCatModel[indexPath.section].drinks?[indexPath.row].id
//                print(alldrinksid)
//
//              //  self.drinkAry.append(alldrinksid!)
//              //  if let i = drinkAry.index(where: { $0.name == alldrinksid }) {
//              //      return drinkAry[i]
//              //  }
//                if let index = drinkAry.index(where: { $0.hasPrefix(alldrinksid!) }) {
//                    print("The first name starting with \(alldrinksid) was found at \(index)")
//                }
//                else {
//
//                }
//                print("Values of drinkarray is \(drinkAry)")
//            }
//        }
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
          print(indexPath.section)
    }
    
    
    
    @objc func selectDrinkClick(sender:UIButton!)
    {
        self.catClicked = false

        print("selected index",sender.tag)
       let row = sender.tag % 1000
       let section = sender.tag / 1000
        
        let indexPath = IndexPath(row: row, section:section )
        let cell = selectionTableView.cellForRow(at: indexPath) as? DrinkSelectionCell
        
       
        
        print(indexPath)
        
        let btnImage = #imageLiteral(resourceName: "CircleBlank")
        print(btnImage)
        if cell?.selectionImg.currentBackgroundImage == btnImage
        {
         cell?.selectionImg.setBackgroundImage(#imageLiteral(resourceName: "CircleFill"), for: .normal)
        }
        else
        {
            cell?.selectionImg.setBackgroundImage(#imageLiteral(resourceName: "CircleBlank"), for: .normal)

        }
        
        
//        if self.sectionsWithStatesDicr[myCurrentSection]  == "false" {
//            self.sectionsWithStatesDicr[myCurrentSection] = "true"
//        }
//
//        else if  self.sectionsWithStatesDicr[myCurrentSection] == "true"{
//            self.sectionsWithStatesDicr[myCurrentSection]  = "false"
//
////        }
//        self.selectionTableView.reloadData()
        
    }
    @objc func btnSectionClick(sender:UIButton!)
    {

        print("selected index",sender.tag)
        self.currentsecTitle = self.baselineCatNameAry[sender.tag]
        print(self.currentsecTitle)
        let myCurrentSection = self.currentsecTitle

        if self.sectionsWithStatesDicr[myCurrentSection]  == "false" {
            self.sectionsWithStatesDicr[myCurrentSection] = "true"
        }
        
        else if  self.sectionsWithStatesDicr[myCurrentSection] == "true"{
            self.sectionsWithStatesDicr[myCurrentSection]  = "false"
          
        }
        self.selectionTableView.reloadData()
        
    }
    
    @objc func btnSelectionDrink(sender:UIButton!)
    {
        let row = sender.tag % 1000
        let section = sender.tag / 1000
        
        let indexPath = IndexPath(row: row, section:section )
        let cell = selectionTableView.cellForRow(at: indexPath) as? DrinkSelectionCell
        
        self.catClicked = true
        self.currentsecTitle = self.baselineCatNameAry[sender.tag]
        print(self.currentsecTitle)
        let myCurrentSection = self.currentsecTitle
   self.selectedCat = self.currentsecTitle
        print("selected index",sender.tag)
        if self.selectedCategoryDict[myCurrentSection]  == "false" {
            self.selectedCategoryDict[myCurrentSection] = "true"
            
        }
            
        else if  self.selectedCategoryDict[myCurrentSection] == "true"{
            self.selectedCategoryDict[myCurrentSection]  = "false"
            
        }
       
        self.selectionTableView.reloadData()
        
    }
    
    func importSelectionApi()
    {
        SVProgressHUD.show(withStatus: "Loading...")
        SVProgressHUD.setDefaultMaskType(.clear)
        let token = Constants.kUserDefaults.string(forKey: appConstants.token)
        let urlString = remoteConfig.mBaseUrl + "selectionCategories"
        print(urlString)
        let _headers : HTTPHeaders = ["x-access-token": token!]
        let params : Parameters = ["drinks": self.drinkAry]
    print(params)
        print(self.drinkAry)
        request(urlString, method: .post, parameters: params, encoding: URLEncoding.httpBody , headers: _headers).responseJSON(completionHandler: {
            response in response
            
            print(response.result)
       if response.result.isSuccess == true
            {
                SVProgressHUD.dismiss()
                self.drinkAry.removeAll()
                let jsonResponse = response.result.value as! NSDictionary
                print(jsonResponse)
                self.view.makeToast("Drink imported successfully")
            }
        })
        SVProgressHUD.dismiss()
    }
    
    // Fetch Baseline Data
    func getBaselineData() {
        SVProgressHUD.show(withStatus: "Loading...")
        SVProgressHUD.setDefaultMaskType(.clear)
        let userId = Constants.kUserDefaults.value(forKey: appConstants.userId) as? String
        UserAPI().getBaselineDataa(userId: userId!,pageNo: 0) { (data, error) in
            if data[APIConstants.isSuccess.rawValue] as! Bool == true {
                SVProgressHUD.dismiss()
                let itemsAry = data["items"] as! NSArray
                print(itemsAry)
                for itemData in itemsAry
                {
                    print(itemData)
                    let itemDta = itemData as! NSDictionary
                    let catName = itemDta["name"] as! String
                    self.baselineCatNameAry.append(catName)
                    let id = itemDta["id"] as! String
                    self.baselineDrinksAry = itemDta["drinks"] as! NSArray
                }
                
                
                for i in self.baselineCatNameAry {
                    self.sectionsWithStatesDicr[i] = "false"
                    
                      self.selectedCategoryDict[i] = "false"
                }
        //        print(self.sectionsWithStatesDicr)
                 print(self.selectedCategoryDict)
                 self.drinkCatModel = Category.modelsFromDictionaryArray(array: itemsAry as NSArray)
                print(self.drinkCatModel)

                print("BaslineDrinksAry: \(self.baselineDrinksAry)")
                
//                var drinksss = self.drinkCatModel[indexPath.row].drinks
//                print(drinksss)
//
//                var drinkName = String()
//                for i in drinksss!
//                {
//                    print("Index path is: \(indexPath.row) Name of drink: \(i.name)")
//                    //            cell.name.text = i.name
//                    drinkName = i.name!
//                     self.selectionTableView.reloadData()
//                }
                
                
             self.selectionTableView.reloadData()
                
                
            }
            else{
                print("Getting Error")
                SVProgressHUD.dismiss()
            }
            
        }
    }
    
}
class DrinkSelectionCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var dropDown: UIButton!
    @IBOutlet weak var selectionImg: UIButton!
    @IBOutlet weak var selectionLeadingConst: NSLayoutConstraint!
    
}
