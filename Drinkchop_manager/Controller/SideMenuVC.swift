//
//  SideMenuVC.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 18/02/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit
import SideMenu
import SDWebImage
import CropViewController

class SideMenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var profilePicView: UIView!
    @IBOutlet weak var lastName: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var firstName: UILabel!

    @IBOutlet weak var menuTableView: UITableView!
    private var fileUploadAPI:FileUpload!
    var Sidearr = ["Home","Profile","Messages","Events","Stats","Contact Us","Settings","Logout"]
    var SideImgarr = [#imageLiteral(resourceName: "home"),#imageLiteral(resourceName: "profile"),#imageLiteral(resourceName: "message"),#imageLiteral(resourceName: "events"),#imageLiteral(resourceName: "ststs"),#imageLiteral(resourceName: "contactus"),#imageLiteral(resourceName: "settings"),#imageLiteral(resourceName: "logout")]
    
    override func viewWillAppear(_ animated: Bool) {
        let firstname = Constants.kUserDefaults.value(forKey: "firstName") as! String
        let lastNamee = Constants.kUserDefaults.value(forKey: "lastName") as! String
        let emaill = Constants.kUserDefaults.value(forKey: "email") as! String
        let imgUrl = Constants.kUserDefaults.value(forKey: "imgUrl") as? String

        firstName.text = firstname
        lastName.text = lastNamee
        email.text = emaill
        if let myImgUrl = Constants.kUserDefaults.value(forKey: "imgUrl") as? String {
            profilePic.sd_setImage(with: URL.init(string:((myImgUrl.httpsExtend))), placeholderImage: #imageLiteral(resourceName: "dummy"))
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        SideMenuManager.default.menuWidth = view.frame.size.width - view.frame.size.width/4
        SideMenuManager.default.menuAnimationBackgroundColor = .clear
        menuTableView.delegate = self
        menuTableView.dataSource = self
        self.fileUploadAPI = FileUpload.sharedInstance
        self.menuTableView.separatorStyle = .none
        self.navigationController?.navigationBar.isHidden = true
        
        profilePic.layer.cornerRadius = profilePic.frame.size.width/2
        profilePic.clipsToBounds = true
        
        profilePicView.layer.cornerRadius = profilePicView.frame.size.width/2
        profilePicView.clipsToBounds = true
        
        // Do any additional setup after loading the view.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func profilePicBtn(_ sender: Any) {
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 59
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return Sidearr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell")as! MenuCell
        cell.menuName.text = Sidearr[indexPath.row]
        cell.menuImg.image = SideImgarr[indexPath.row]
        // Mark Clear user Selection
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
            if indexPath.row == 0{
                let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self.navigationController?.pushViewController(destinationvc, animated: true)
            }else if indexPath.row == 1{
                let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
                self.navigationController?.pushViewController(destinationvc, animated: true)
            }else if indexPath.row == 2{
                let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "MessagesVC") as! MessagesVC
                self.navigationController?.pushViewController(destinationvc, animated: true)
            }else if indexPath.row == 3{
                let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "EventsVC") as! EventsVC
                self.navigationController?.pushViewController(destinationvc, animated: true)
            }else if indexPath.row == 4{
                let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "StatsVC") as! StatsVC
                self.navigationController?.pushViewController(destinationvc, animated: true)
            }else if indexPath.row == 5{
                let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsNewID") as! ContactUsNew
                self.navigationController?.pushViewController(destinationvc, animated: true)
            }else if indexPath.row == 6{
                let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "SettingNotificationVC") as! SettingNotificationVC
                self.navigationController?.pushViewController(destinationvc, animated: true)
            }else if indexPath.row == 7{
                // Create the alert controller
                let alertController = UIAlertController(title: "Logout", message: "Are you sure you want to Logout", preferredStyle: .alert)
                
                // Create the actions
                let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) {
                    UIAlertAction in
                    NSLog("OK Pressed")
                Constants.kUserDefaults.set(false, forKey: "isLogin")
                    
                    let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                    
                    Constants.kUserDefaults.set(false, forKey:"isLogin")
                    Constants.kUserDefaults.set(nil, forKey: appConstants.token)
                    self.navigationController?.pushViewController(destinationvc, animated: true)
                }
                let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) {
                    UIAlertAction in
                    NSLog("Cancel Pressed")
                    return
                }
                
                // Add the actions
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
                
                // Present the controller
            }else {
                
            }
    }

    

    
}


class MenuCell: UITableViewCell {
    @IBOutlet weak var menuImg: UIImageView!
    @IBOutlet weak var menuName: UILabel!
    
}
