//
//  CoverFees&OptionsVC.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 20/02/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit
import  SVProgressHUD
import Alamofire
class CoverFees_OptionsVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var daysCollectionView: UICollectionView!
    @IBOutlet weak var coverfeeTF: UITextField!
    var dayName = String()
     var daysModel = [Days]()
    private var userApi : UserAPI!
    var user: User?
    var validator = Validators()
    var weakDaysAry = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
     var daysAry = NSArray()
     var barUpdateMdl : BarUpdate!
    var btnSelected = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        coverfeeTF.delegate = self
        self.barUpdateMdl = BarUpdate.init(dictionary: NSDictionary())
        self.getCoverfeeDetails()
        daysCollectionView.delegate = self
        daysCollectionView.dataSource = self
        self.userApi = UserAPI.sharedInstance
        // Do any additional setup after loading the view.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
 
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == coverfeeTF
        {
            var dayFetch = String()
            print(daysAry)
            for (index, dy) in (barUpdateMdl.days?.enumerated())!
            {
                dayFetch = dy.day ?? ""
                print(dayName)
                if dayName == dayFetch
                {
                    let coverFeee = coverfeeTF.text
                    self.daysModel[index].newcoverfee = coverFeee
                    dy.newcoverfee = coverFeee
                    print(self.daysModel[index].newcoverfee)
                }
            }
        }

    }
    @IBAction func saveBtn(_ sender: Any) {
        coverfeeTF.resignFirstResponder()
         guard validator.validators(TF1: self.coverfeeTF,fieldName: "Cover Fee") == false
        else
         {
            // Hit APi Here
       //     self.user = User.init(dictionary: NSDictionary())

      //      self.user?.coverFee = coverfeeTF.text!

     //       print(user?.coverFee!)
          //  self.getBarId(userDict: user!)
          self.updateCoverfeeApi()
            return
        }
    }
     // MARK: User Cover Fee Function
    func coverFeeApi()
    {
        let token = Constants.kUserDefaults.string(forKey: appConstants.token)
        let barid = Constants.kUserDefaults.string(forKey: appConstants.barId)
        print(barid)
        var url = "http://159.89.226.199:8080/api/bars/"
        let urlString = "\(url)" + barid!
        print(urlString)
        let _headers : HTTPHeaders = ["x-access-token": token!]
        let params : Parameters = ["coverFee": coverfeeTF.text!]
        
        let myurl =  NSURL(string:"url" as String)
        
        request(urlString, method: .put, parameters: params, encoding: JSONEncoding.default , headers: _headers).responseJSON(completionHandler: {
            response in response
            
            let jsonResponse = response.result.value as! NSDictionary
            print(jsonResponse)
            
        })
    }

    @IBAction func backBtn(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    // Get Cover fee details
    func getCoverfeeDetails() {
        let barId = Constants.kUserDefaults.string(forKey: "barId")!
        UserAPI().getcoverDataa(userId: barId,pageNo: 0) { (data, error) in
            if data[APIConstants.isSuccess.rawValue] as! Bool == true {
                
                let userDict = data["data"] as! [String: AnyObject]
                print(userDict)
                
                let dataProfile = NSKeyedArchiver.archivedData(withRootObject: userDict)
                
                let name = userDict["name"] as! String
                let imgUrl = userDict["imgUrl"] as? String
                let tax = userDict["tax"] as? Int
                let destcription = userDict["destcription"] as? String
                let twitterLink = userDict["twitterLink"] as? String
                let fbLink = userDict["fbLink"] as? String
                let instagramLink = userDict["instagramLink"] as? String
                let country = userDict["country"] as? String
                let state = userDict["state"] as? String
                let city = userDict["city"] as? String
                let zipCode = userDict["zipCode"] as? Int
                
                let radius = userDict["radius"] as? Int
                
                let location = userDict["location"] as? [String: AnyObject]
                let address = location?["address"] as! String
                let coordinates = location?["coordinates"] as? NSArray
                print(coordinates)
                let lat = coordinates?[0] as! Double
                let long = coordinates?[1] as! Double
                self.daysAry = userDict["days"] as! NSArray
                print(self.daysAry)
                //                for dy in days
                //                {
                //                    let daysData = dy as! [String: AnyObject]
                //
                //                }
                self.daysModel = Days.modelsFromDictionaryArray(array: self.daysAry as NSArray)

           
                print(self.daysAry)
                self.daysModel = Days.modelsFromDictionaryArray(array: self.daysAry as NSArray)
                self.barUpdateMdl = BarUpdate.init(dictionary: userDict as NSDictionary)
                print(self.barUpdateMdl.dictionaryRepresentation())
  //              if name != nil {
                    
                  
                    
               //     self.coverfeeTF.text = address
   //             }
                //                let myUser = User.init(dictionary: userDict as! NSDictionary)
                
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }
// Update cover fee api
    func updateCoverfeeApi()
    {
     
        let token = Constants.kUserDefaults.string(forKey: appConstants.token)
        print(token)
        let barId = Constants.kUserDefaults.string(forKey: "barId")!
        let urlString = remoteConfig.mBaseUrl + "bars/" + barId
        print(urlString)
        print(self.daysModel)
        let _headers : HTTPHeaders = ["x-access-token": token!]
        
        var allDaysData = [NSDictionary]()
        var daysMdlArray = [Days]()
        allDaysData.removeAll()
        for mdlData in (self.barUpdateMdl?.days)!
        {
            var dayyyy = Days(dictionary: NSDictionary())
            dayyyy?.barstatus = mdlData.barstatus
            dayyyy?.day = mdlData.day
            dayyyy?.starttime = mdlData.starttime
            dayyyy?.endtime = mdlData.endtime
            dayyyy?.newcoverfee = mdlData.newcoverfee
            
//            let barststus = mdlData.barstatus
//            let day = mdlData.day
//            let starttime = mdlData.starttime
//            let endtime = mdlData.endtime
//            let coverfee = mdlData.newcoverfee
            
//            let mdlDic = ["barstatus":barststus,"day": day, "starttime": starttime, "endtime": endtime, "newcoverfee": coverfee] as [String : Any]
//            allDaysData.append(mdlDic as NSDictionary)
//            print(mdlDic)
            daysMdlArray.append(dayyyy!)
        }
        print(allDaysData)
        // barUpdateMdl?.days = allDaysData a
        let params: Parameters = self.barUpdateMdl!.dictionaryRepresentation() as! Dictionary<String,AnyObject>
        print(params)
        request(urlString, method: .put, parameters: params, encoding: JSONEncoding.default , headers: _headers).responseJSON(completionHandler: {
            response in response
            
            print(response.result.value)
            if response.result.isSuccess == true
            {
                self.view.makeToast("Save successfully")
                
                let jsonResponse = response.result.value as! NSDictionary
                print(jsonResponse)
                let jsonData = jsonResponse["data"] as! [String: AnyObject]
                print(jsonData)
                
                let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self.navigationController?.pushViewController(destinationvc, animated: true)
                
            }
            
            
        })
    }
    
//    func getBarId(userDict:User){
//        SVProgressHUD.show(withStatus: "Please Wait")
//        userApi.getBarId(userDetials: userDict){ (isSuccess,response, error) -> Void in
//            SVProgressHUD.dismiss()
//
//            if (isSuccess){
//                SVProgressHUD.dismiss()
//                kAppDelegate.showNotification(text: "Profile Updated Successfully")
//                self.navigationController?.popViewController(animated: true)
//            }else{
//                SVProgressHUD.dismiss()
//                if error != nil{
//                    kAppDelegate.showNotification(text: "Something went wrong!")
//                    //kAppDelegate.showNotification(text: error!)
//                }else{
//                    kAppDelegate.showNotification(text: "Something went wrong!")
//                }
//            }
//
//        }
//
//
//    }
    
    @objc func daysBtnClick(sender:UIButton!)
    {
        let CircleBlank = UIImage(named: "boxBlank") as UIImage?
        let CircleFill = UIImage(named: "boxSelected") as UIImage?
        var indxPath = IndexPath(item: sender.tag, section: 0)
        let cell = daysCollectionView.cellForItem(at: indxPath) as! daysCoverCell
        print("TAG VALUE: \(sender.tag)")
    //    cell.daySelectionOutlet.setBackgroundImage(#imageLiteral(resourceName: "CircleFill"), for: .normal)
        //let button = sender as? UIButton
       // let point: CGPoint = button!.convert(.zero, to: daysCollectionView)
     //   let indexPath = daysCollectionView!.indexPathForItem(at: point)
        
     //   let cell = daysCollectionView!.cellForItem(at: indexPath as! IndexPath) as! daysCoverCell

        
     //   print("Indexpath is : \(indexPath)")
        
        
//        if sender.tag == 0
//        {
//        //    print(barUpdateMdl.days?.count)
//            dayName = "Monday"
//            var barstatus = Bool()
//            for dysData in (barUpdateMdl?.days)!
//            {
//            self.btnSelected = "0"
//                let daydata = dysData
//                let day = daydata.day as! String
//                if day == "Monday"
//                {
//                    let newcoverfee = daydata.newcoverfee as! String
//
//                    barstatus = daydata.barstatus! as NSNumber as! Bool
//                        coverfeeTF.text = newcoverfee
//                }
//            }
//        }
//
//        else if sender.tag == 1
//        {
//            self.btnSelected = "1"
//
//            dayName = "Tuesday"
//            for dysData in (barUpdateMdl?.days)!
//            {
//                let daydata = dysData
//                var day = daydata.day as! String
//                if day == "Tuesday"
//                {
//                     let newcoverfee = daydata.newcoverfee as! String
//                       var barstatus = daydata.barstatus! as NSNumber as! Bool
//                    if barstatus == true
//                    {
//
//                  coverfeeTF.text = newcoverfee
//                    }
//                    else
//                    {
//                 coverfeeTF.text = "0"
//                    }
//
//                }
//            }
//        }
//
//        else if sender.tag == 2
//        {
//            self.btnSelected = "2"
//
//            dayName = "Wednesday"
//            for dysData in (barUpdateMdl?.days)!
//            {
//                let daydata = dysData
//                var day = daydata.day
//                if day == "Wednesday"
//                {
//                  let newcoverfee = daydata.newcoverfee as! String
//                    var barstatus = daydata.barstatus! as NSNumber as! Bool
//                    if barstatus == true
//                    {
//
//              coverfeeTF.text = newcoverfee
//                    }
//                    else
//                    {
//              coverfeeTF.text = "0"
//                    }
//
//                }
//            }
//        }
//
//        else if sender.tag == 3
//        {
//            self.btnSelected = "3"
//
//            dayName = "Thursday"
//
//            for dysData in (barUpdateMdl?.days)!
//            {
//              let daydata = dysData
//                var day = daydata.day
//                if day == "Thursday"
//                {
//                      let newcoverfee = daydata.newcoverfee as! String
//                    var barstatus = daydata.barstatus! as NSNumber as! Bool
//                    if barstatus == true
//                    {
//
//             coverfeeTF.text = newcoverfee
//                    }
//                    else
//                    {
//          coverfeeTF.text = "0"
//                    }
//
//                }
//            }
//        }
//
//        else if sender.tag == 4
//        {
//            self.btnSelected = "4"
//
//            dayName = "Friday"
//
//            for dysData in (barUpdateMdl?.days)!
//            {
//              let daydata = dysData
//                var day = daydata.day
//                if day == "Friday"
//                {
//                 let newcoverfee = daydata.newcoverfee as! String
//                    var barstatus = daydata.barstatus! as NSNumber as! Bool
//                    if barstatus == true
//                    {
//
//          coverfeeTF.text = newcoverfee
//                    }
//                    else
//                    {
//           coverfeeTF.text = "0"
//                    }
//
//
//                }
//            }
//        }
//
//        else if sender.tag == 5
//        {
//            self.btnSelected = "5"
//
//            dayName = "Saturday"
//
//            for dysData in (barUpdateMdl?.days)!
//            {
//                let daydata = dysData
//                var day = daydata.day
//                if day == "Saturday"
//                {
//              let newcoverfee = daydata.newcoverfee as! String
//                    var barstatus = daydata.barstatus! as NSNumber as! Bool
//                    if barstatus == true
//                    {
//
//       coverfeeTF.text = newcoverfee
//                    }
//                    else
//                    {
//        coverfeeTF.text = "0"
//                    }
//
//                }
//            }
//        }
//
//        else if sender.tag == 6
//        {
//            self.btnSelected = "6"
//
//            dayName = "Sunday"
//            for dysData in (barUpdateMdl?.days)!
//            {
//                 let daydata = dysData
//                var day = daydata.day
//                if day == "Sunday"
//                {
//            let newcoverfee = daydata.newcoverfee as! String
//                    var barstatus = daydata.barstatus! as NSNumber as! Bool
//                    if barstatus == true
//                    {
//
//         coverfeeTF.text = newcoverfee
//                    }
//                    else
//                    {
//     coverfeeTF.text = "0"
//                    }
//                }
//            }
//        }
        self.daysCollectionView.reloadData()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return weakDaysAry.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "daysCoverCell", for: indexPath as IndexPath) as! daysCoverCell
        
        cell.daysLbl.text = weakDaysAry[indexPath.row]
        cell.daySelectionOutlet.tag =  indexPath.item  //(indexPath.section * 100) + indexPath.item
        print(daysAry)
       // cell.daySelectionOutlet.addTarget(self, action: #selector(daysBtnClick(sender:)), for: .touchUpInside)

//
//            if self.btnSelected == "0"
//            {
//                if cell.daySelectionOutlet.currentBackgroundImage == #imageLiteral(resourceName: "CircleFill")
//                {
//                      cell.daySelectionOutlet.setBackgroundImage(#imageLiteral(resourceName: "CircleBlank"), for: .normal)
//                }
//                else
//                {
//                    cell.daySelectionOutlet.setBackgroundImage(#imageLiteral(resourceName: "CircleFill"), for: .normal)
//                }
//
//                self.btnSelected = "66"
//            }
//           else if self.btnSelected == "1"
//            {
//                if cell.daySelectionOutlet.currentBackgroundImage == #imageLiteral(resourceName: "CircleFill")
//                {
//                    cell.daySelectionOutlet.setBackgroundImage(#imageLiteral(resourceName: "CircleBlank"), for: .normal)
//                }
//                else
//                {
//                    cell.daySelectionOutlet.setBackgroundImage(#imageLiteral(resourceName: "CircleFill"), for: .normal)
//                }
//                 self.btnSelected = "66"
//            }
//            else if self.btnSelected == "2"
//            {
//                if cell.daySelectionOutlet.currentBackgroundImage == #imageLiteral(resourceName: "CircleFill")
//                {
//                    cell.daySelectionOutlet.setBackgroundImage(#imageLiteral(resourceName: "CircleBlank"), for: .normal)
//                }
//                else
//                {
//                    cell.daySelectionOutlet.setBackgroundImage(#imageLiteral(resourceName: "CircleFill"), for: .normal)
//                }
//             self.btnSelected = "66"
//            }
//            else if self.btnSelected == "3"
//            {
//                if cell.daySelectionOutlet.currentBackgroundImage == #imageLiteral(resourceName: "CircleFill")
//                {
//                    cell.daySelectionOutlet.setBackgroundImage(#imageLiteral(resourceName: "CircleBlank"), for: .normal)
//                }
//                else
//                {
//                    cell.daySelectionOutlet.setBackgroundImage(#imageLiteral(resourceName: "CircleFill"), for: .normal)
//                }
//             self.btnSelected = "66"
//            }
//            else if self.btnSelected == "4"
//            {
//                if cell.daySelectionOutlet.currentBackgroundImage == #imageLiteral(resourceName: "CircleFill")
//                {
//                    cell.daySelectionOutlet.setBackgroundImage(#imageLiteral(resourceName: "CircleBlank"), for: .normal)
//                }
//                else
//                {
//                    cell.daySelectionOutlet.setBackgroundImage(#imageLiteral(resourceName: "CircleFill"), for: .normal)
//                }
//             self.btnSelected = "66"
//            }
//            else if self.btnSelected == "5"
//            {
//                if cell.daySelectionOutlet.currentBackgroundImage == #imageLiteral(resourceName: "CircleFill")
//                {
//                    cell.daySelectionOutlet.setBackgroundImage(#imageLiteral(resourceName: "CircleBlank"), for: .normal)
//                }
//                else
//                {
//                    cell.daySelectionOutlet.setBackgroundImage(#imageLiteral(resourceName: "CircleFill"), for: .normal)
//                }
//             self.btnSelected = "66"
//            }
//            else if self.btnSelected == "6"
//            {
//                if cell.daySelectionOutlet.currentBackgroundImage == #imageLiteral(resourceName: "CircleFill")
//                {
//                    cell.daySelectionOutlet.setBackgroundImage(#imageLiteral(resourceName: "CircleBlank"), for: .normal)
//                }
//                else
//                {
//                    cell.daySelectionOutlet.setBackgroundImage(#imageLiteral(resourceName: "CircleFill"), for: .normal)
//                }
//         self.btnSelected = "66"
//        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
          let cell = collectionView.cellForItem(at: indexPath) as? daysCoverCell
        cell?.daySelectionOutlet.setBackgroundImage(#imageLiteral(resourceName: "CircleBlank"), for: .normal)
        coverfeeTF.resignFirstResponder()

    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? daysCoverCell
        cell?.daySelectionOutlet.setBackgroundImage(#imageLiteral(resourceName: "CircleFill"), for: .normal)
        
                if indexPath.row == 0
                {
                //    print(barUpdateMdl.days?.count)
                    dayName = "Monday"
                    for dysData in ((barUpdateMdl?.days)!)
                    {
                    self.btnSelected = "0"
                        let daydata = dysData
                        let day = daydata.day as! String
                        if day == "Monday"
                        {
                            let newcoverfee = daydata.newcoverfee as! String
        
                                coverfeeTF.text = newcoverfee
                        }
                    }
                }
        
                else if indexPath.row == 1
                {
                    self.btnSelected = "1"
        
                    dayName = "Tuesday"
                    for dysData in (barUpdateMdl?.days)!
                    {
                        let daydata = dysData
                        var day = daydata.day as! String
                        if day == "Tuesday"
                        {
                             let newcoverfee = daydata.newcoverfee as! String
                          coverfeeTF.text = newcoverfee

        
                        }
                    }
                }
        
                else if indexPath.row == 2
                {
                    self.btnSelected = "2"
        
                    dayName = "Wednesday"
                    for dysData in (barUpdateMdl?.days)!
                    {
                        let daydata = dysData
                        var day = daydata.day
                        if day == "Wednesday"
                        {
                          let newcoverfee = daydata.newcoverfee as! String
                      coverfeeTF.text = newcoverfee
                        }
                    }
                }
        
                else if indexPath.row == 3
                {
                    self.btnSelected = "3"
        
                    dayName = "Thursday"
        
                    for dysData in (barUpdateMdl?.days)!
                    {
                      let daydata = dysData
                        var day = daydata.day
                        if day == "Thursday"
                        {
                              let newcoverfee = daydata.newcoverfee as! String
                     coverfeeTF.text = newcoverfee

                        }
                    }
                }
        
                else if indexPath.row == 4
                {
                    self.btnSelected = "4"
        
                    dayName = "Friday"
        
                    for dysData in (barUpdateMdl?.days)!
                    {
                      let daydata = dysData
                        var day = daydata.day
                        if day == "Friday"
                        {
                         let newcoverfee = daydata.newcoverfee as! String
                  coverfeeTF.text = newcoverfee

                        }
                    }
                }
        
                else if indexPath.row == 5
                {
                    self.btnSelected = "5"
        
                    dayName = "Saturday"
        
                    for dysData in (barUpdateMdl?.days)!
                    {
                        let daydata = dysData
                        var day = daydata.day
                        if day == "Saturday"
                        {
                      let newcoverfee = daydata.newcoverfee as! String
               coverfeeTF.text = newcoverfee

                        }
                    }
                }
        
                else if indexPath.row == 6
                {
                    self.btnSelected = "6"
        
                    dayName = "Sunday"
                    for dysData in (barUpdateMdl?.days)!
                    {
                         let daydata = dysData
                        var day = daydata.day
                        if day == "Sunday"
                        {
                    let newcoverfee = daydata.newcoverfee as! String
                      
                 coverfeeTF.text = newcoverfee
                            
                       
                        }
                    }
                }

        
    }

}
class daysCoverCell: UICollectionViewCell {
    @IBOutlet weak var daysLbl: UILabel!
    @IBOutlet weak var daySelectionOutlet: UIButton!
    
}
