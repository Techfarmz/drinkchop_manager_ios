//
//  ContactUsNew.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 12/06/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit

class ContactUsNew: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtn(_ sender: Any) {
   navigationController?.popViewController(animated: true)
    }
    @IBAction func sendEmailBtn(_ sender: Any) {
        //  support@drinkchop.com
        let url = NSURL(string: "mailto:support@drinkchop.com")
        UIApplication.shared.openURL(url as! URL)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
