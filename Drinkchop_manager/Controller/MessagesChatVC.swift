//
//  MessagesChatVC.swift
//  DrinkchopBartender
//
//  Created by anmoldeep singh on 07/01/19.
//  Copyright © 2019 Temp. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import SVProgressHUD
import Toast_Swift
import SDWebImage

class MessagesChatVC: UIViewController {

    @IBOutlet weak var mMessagestableview: UITableView!
    @IBOutlet weak var typeTextTF: UITextField!
    
    
    var chatsAPI : ChatsAPI!
   //   var partiDict = [[String: AnyObject]]()
    var otherUserID : String?
    var otherUserName : String?
    var otherUserProfilePicUrl : String?
    var idsOfUsersInTheGroup : [String]?
    var typeOfChat : String?
    var idOfTheChat = String()
    var firebaseDBRef : DatabaseReference!
    var chatDictionary : Chat?
    var arrayForSnapshotOfChatValues  = NSMutableArray()
    var chatModelArray : [Chat]!
    var mlabelHeight = Int()
    var myId : String?
    var isComingFromSearchUserVC : Bool = false
    var groupName : String?
    var chatAlreadyExists : Bool = false
    var usersInGroupMdlArray = [User]()
    var groupAdminId : String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
 self.hideKeyboardWhenTappedAround() 
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = false
        self.mMessagestableview.delegate = self
        self.mMessagestableview.dataSource = self
        self.mMessagestableview.backgroundView = UIImageView(image: #imageLiteral(resourceName: "OTHER-SCREEN-BACKROUND"))
        self.mMessagestableview.backgroundView?.alpha = 0.2
        self.typeTextTF.delegate = self
        self.chatsAPI = ChatsAPI.sharedInstance
        self.chatModelArray = [Chat]()
        
        self.setupNavBarTitle()
        self.typeTextTF.attributedPlaceholder = NSAttributedString(string: "Type message..", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
        self.myId = (Constants.kUserDefaults.value(forKey: appConstants.id) as! String)
        
        print("Name of the other User: \(String(describing: otherUserName))")
        print("Id of the other User: \(String(describing: otherUserID))")
        print("IDS of users in the group \(String(describing: idsOfUsersInTheGroup))")
        print("Other User Id \(String(describing: self.otherUserID))")
        print("Id of the Admin \(String(describing: self.groupAdminId))")
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if self.chatAlreadyExists == false{
            self.createDataForChat()
        }
        else if self.chatAlreadyExists == true {
            var chatDict = Chat.init(dictionary: NSDictionary())
            
            self.setZeroUnreadCount(chatDetails: chatDict!, iddOfChatt: self.idOfTheChat)
            self.getChatFromFireBase()
        }
    }
    func popViewControllerss(popViews: Int, animated: Bool = true) {
        if self.navigationController!.viewControllers.count > popViews
        {
            let vc = self.navigationController!.viewControllers[self.navigationController!.viewControllers.count - popViews - 1]
            self.navigationController?.popToViewController(vc, animated: animated)
        }
    }
    func setupNavBarTitle () {
        self.navigationItem.title = otherUserName ??  ""
    }
    // Hode Keyboard on touch
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func msgSendBtn(_ sender: Any) {
        if self.typeTextTF.text !=  nil && self.typeTextTF.text?.count != 0 {
            
            if self.typeOfChat == "group"{
                
                if self.groupAdminId != nil {
                    if self.myId != self.groupAdminId {
                        self.otherUserID = self.groupAdminId
                    }
                        
                    else if self.myId == self.groupAdminId {
                        self.otherUserID = ""
                    }
                }}
            
            
            self.createChatOnFireBase()
            self.mMessagestableview.reloadData()
        }
        
        self.view.endEditing(true)
    }
    
    @IBAction func mBackbtnAct(_ sender: Any) {
         if self.isComingFromSearchUserVC == false {
        navigationController?.popViewController(animated: true)
        }
        else
        
         {
            self.popViewControllerss(popViews: 2)
        }
    }
    func createDataForChat () {
        self.chatDictionary = Chat.init(dictionary: NSDictionary())
        // var participantsArr = [Participantss]()
        var participantsDictionary = [[String:AnyObject]]()
        
        
        print(self.typeOfChat)
        if self.typeOfChat == "normal"{
            //            let participantDict = Participantss.init(dictionary: NSDictionary())
            //            participantDict?.userId = self.otherUserID!
            //            participantDict?.unreadCount = 0
            //            participantDict?.timeStamp = "0"
            //            participantsArr.append(participantDict!)
            // participantsDictionary = ["userId":self.otherUserID! as Any,"unreadCount":0,"timeStamp":"0"]
            participantsDictionary.append(["userId":[self.otherUserID!] as AnyObject])
            
            
            
        }
            
        else if self.typeOfChat == "group"{
            for i in self.idsOfUsersInTheGroup! {
                var arrayOfIds = [AnyObject]()
                arrayOfIds.append(i as AnyObject)
                participantsDictionary.append(["userId":arrayOfIds as AnyObject])
                
            }
            
            chatDictionary?.chatType = self.typeOfChat
            chatDictionary?.lastMessage = "last msg"
            //chatDictionary?.participants = participantsArr
            
        }
        var dict = ["chatType":self.typeOfChat as AnyObject,"lastMessage":"last msg" as AnyObject as AnyObject] as [String : AnyObject]
        dict["participants"] = participantsDictionary as AnyObject
        
        if self.typeOfChat == "group"{
            dict["groupName"] = self.groupName as AnyObject
        }
        print("dict: \(dict)")
        self.createChatAndGetId(detailsOfChat: dict)
        
    }
    
    

    func createChatAndGetId (detailsOfChat: [String:Any]) {
        chatsAPI.createChatAndGetID(chatDetails: detailsOfChat) { (isSuccess, response, error) in
            if isSuccess == true {
                let responseReceived_ChatData = response![APIConstants.data.rawValue] as! NSDictionary
                self.chatDictionary = Chat.init(dictionary: responseReceived_ChatData)
                guard let identityOfTheChat = self.chatDictionary?.id else { self.view.makeToast("Chat Id Not Found")
                    return }
                
                self.idOfTheChat = identityOfTheChat
                
                for i in (self.chatDictionary?.participants)! {
                    if i.isAdmin == true {
                        self.groupAdminId = i.user![0].id
                    }
                    
                }
                
                
                if self.typeOfChat == "group"{
                    
                    if self.groupAdminId != nil {
                        if self.myId != self.groupAdminId {
                            self.otherUserID = self.groupAdminId
                        }
                            
                        else if self.myId == self.groupAdminId {
                            self.otherUserID = ""
                        }
                    }
                    
                    //                    if self.myId != self.chatDictionary?.user!.id! {
                    //                        self.otherUserID = self.chatDictionary?.user!.id!
                    //                    }
                    //                    else {
                    //                        self.otherUserID = ""
                    //                        }
                    
                    //                    if let otherrrUserrrIddd = self.chatDictionary?.user!.id! {
                    //                        self.otherUserID = otherrrUserrrIddd
                    //                    }
                }
                
                
                self.setZeroUnreadCount(chatDetails: self.chatDictionary!, iddOfChatt: self.idOfTheChat)
                self.getChatFromFireBase()
                
            }
            else {
                self.view.makeToast("An Error Occurred.Try Again")
            }
        }
        
    }
    //createChatAndGetId func CLOSED
    
    func createChatOnFireBase () {
        print("Chat id is: \(self.idOfTheChat) (inside createchatOnFireBase Func)")
        let myUserId = Constants.kUserDefaults.value(forKey: appConstants.id) as! String
        self.firebaseDBRef = Database.database().reference()
        let automaticallyGeneratedID  = firebaseDBRef.childByAutoId()
        print("Auto Id Generated By Firebase is: \(automaticallyGeneratedID.key!)")
        
        
        let detailsDict:Dictionary<String, Any> = ["chatId": self.idOfTheChat, "message":self.typeTextTF.text!, "msgId":automaticallyGeneratedID.key!, "myId":myUserId, "opponentId":self.otherUserID!, "timeStamp": ServerValue.timestamp()]
        
        let messagesNodeRef = self.firebaseDBRef.child("messages").child(self.idOfTheChat)
        let chatIdNodeRef = messagesNodeRef.child(automaticallyGeneratedID.key!)
        chatIdNodeRef.setValue(detailsDict)
        
        self.chatDictionary = Chat.init(dictionary: NSDictionary())!
        self.chatDictionary!.lastMessage = self.typeTextTF.text! //DOUBTFUL
        
        self.incrementUnreadCount(chatDetails : chatDictionary!, iddOfChatt: self.idOfTheChat) //DOUBTFUL
        self.typeTextTF.text = ""
        self.getChatFromFireBase()
        //
    }
    
    func getChatFromFireBase () {
        SVProgressHUD.show(withStatus: "Loading Chat")
        print("Chat id is: \(self.idOfTheChat) (inside getchatFromFireBase Func)")
        self.firebaseDBRef = Database.database().reference()
        print(self.idOfTheChat)
        self.firebaseDBRef.child("messages").child(self.idOfTheChat).queryOrdered(byChild: "timeStamp").observe(.value,with: { (data) in
            print("observed data: \(data)")
            SVProgressHUD.dismiss()
            if data.value! is NSNull {
                print("data is null")
            }
                
            else {
                let receivedValues = data.value as? NSDictionary
                print("received Values: \(receivedValues)")
                self.arrayForSnapshotOfChatValues.removeAllObjects()
                for i in data.children {
                    let snapp = i as! DataSnapshot
                    print("snapshot: \(snapp)")
                    self.arrayForSnapshotOfChatValues.add(snapp.value!)}
                self.chatModelArray.removeAll()
                self.chatModelArray = Chat.modelsFromDictionaryArray(array: self.arrayForSnapshotOfChatValues)
                self.mMessagestableview.reloadData()
                //self.mChatTblView.scrollto
            }
        })
            
        { (error) in
            print(error.localizedDescription)
            self.view.makeToast(error.localizedDescription)
        }
        
    }
    
    
    func setZeroUnreadCount(chatDetails : Chat,iddOfChatt : String){
        chatsAPI.setUnreadCountToZero(chatDetails: chatDetails, chatId: iddOfChatt) { (isSuccess, response, error) in
            if isSuccess == true { print("Unread Count API Hit Successfully")
                return }
            else { print("Something Went Wrong")
                return
            }
            
        }
    } //setZeroUnreadCount func CLOSED
    
    func incrementUnreadCount(chatDetails : Chat,iddOfChatt : String){
        chatsAPI.incrementUnreadCount(chatDetails: chatDetails, chatId: iddOfChatt) { (isSuccess, response, error) in
            if isSuccess == true { print("Increment Unread Count API Hit Successfully")
                return }
            else { print("Something Went Wrong")
                return
            }
            
        }
    }
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.chatModelArray.count-1, section: 0)
            self.mMessagestableview.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
    
    
    func timestampToDate(Timestamp :Double,completion: (String) -> ()){
        let x = Timestamp / 1000
        let date = NSDate(timeIntervalSince1970: x)
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .medium
        let localDate = formatter.string(from: date as Date)
        completion(localDate)
    }
    
    // Move the text field in a pretty animation!
    func moveTextField(_ textField: UITextField, moveDistance: Int, up: Bool) {
        let moveDuration = 0.3
        let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
        
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(moveDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
}

extension MessagesChatVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return self.chatModelArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
            let cell = mMessagestableview.dequeueReusableCell(withIdentifier: "MessagesChatTableViewCellID")as! MessagesChatTableViewCell
            
        let chatt = chatModelArray[indexPath.row]
        
        self.mlabelHeight = cell.mLabel.calculateMaxLines()
        
        if let timmestamp = chatt.timeStamp {
            self.timestampToDate(Timestamp:timmestamp) { (coverttime) in
                cell.mLabel1.text! = coverttime
                
            }
        }
            
        switch self.typeOfChat {
        case "normal":
            if self.myId != chatt.opponentId {
                cell.mSenderImg.isHidden = false
                cell.muserImage.isHidden = true
                if chatt.message != nil {
                    cell.mLabel.text = chatt.message!
                }
                
                if let myImgUrl = Constants.kUserDefaults.value(forKey: appConstants.imgUrl) as? String {
                    cell.mSenderImg.sd_setImage(with: URL.init(string:((myImgUrl.httpsExtend))), placeholderImage: #imageLiteral(resourceName: "usrDef"))
                }
                
                cell.mLabel.textAlignment = .right
                cell.mImageView.image = #imageLiteral(resourceName: "image-27")
            }
                
            else if self.myId == chatt.opponentId {
                cell.mSenderImg.isHidden = true
                cell.muserImage.isHidden = false
                cell.mLabel.text = chatt.message!
                cell.mImageView.image = #imageLiteral(resourceName: "receiverimg")
                cell.mLabel.textAlignment = .left
                if self.otherUserProfilePicUrl != nil {
                cell.muserImage.sd_setImage(with: URL.init(string:((otherUserProfilePicUrl!.httpsExtend))), placeholderImage: #imageLiteral(resourceName: "dummy"))
                }
            }
            
            
            break;
        case "group":
            
            if self.myId != chatt.opponentId && self.myId == chatt.myId {
                cell.mSenderImg.isHidden = false
                cell.muserImage.isHidden = true
                if chatt.message != nil {
                    cell.mLabel.text = chatt.message!
                }
                if let myImgUrl = Constants.kUserDefaults.value(forKey: appConstants.imgUrl) as? String {
                   cell.mSenderImg.sd_setImage(with: URL.init(string:((myImgUrl.httpsExtend))), placeholderImage: #imageLiteral(resourceName: "dummy"))
                }
                cell.mLabel.textAlignment = .right
               cell.mImageView.image = #imageLiteral(resourceName: "image-27")
                
            }
                
                
                
            else if self.myId == chatt.opponentId && self.myId != chatt.myId {
                
                cell.mSenderImg.isHidden = true
                cell.muserImage.isHidden = false
                cell.mLabel.text = chatt.message!
             //   cell.mImageView.image = #imageLiteral(resourceName: "receiverimg")
                cell.mLabel.textAlignment = .left
                for i in self.usersInGroupMdlArray {
                    if i.imgUrl != nil {
                        if chatt.myId == i.id {cell.muserImage.sd_setImage(with: URL.init(string:((i.imgUrl!.httpsExtend))), placeholderImage: #imageLiteral(resourceName: "dummy"))}
                    }
                    
                }
                
            }
                
                
            else if self.myId != chatt.opponentId && self.myId != chatt.opponentId {
                
                cell.mSenderImg.isHidden = true
                cell.muserImage.isHidden = false
                cell.mLabel.text = chatt.message!
             //   cell.mImageView.image = #imageLiteral(resourceName: "receiverimg")
                cell.mLabel.textAlignment = .left
                for i in self.usersInGroupMdlArray {
                    if i.imgUrl != nil {
                        if chatt.myId == i.id {cell.muserImage.sd_setImage(with: URL.init(string:((i.imgUrl!.httpsExtend))), placeholderImage: #imageLiteral(resourceName: "bv"))}
                    }
                    
                }
                
            }
            
            
            
            
            
            
            
            
            
            
            
            
            break;
            
        default:
            break;
            
            
        }
        return cell
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        print(mlabelHeight)
        if self.mlabelHeight == 1{
            return 96
        }else if self.mlabelHeight == 3{
            return 133
        }else{
            return UITableView.automaticDimension
        }
    }
}

extension MessagesChatVC : UITextFieldDelegate {
    // Start Editing The Text Field
    func textFieldDidBeginEditing(_ textField: UITextField) {
        moveTextField(textField, moveDistance: -250, up: true)
    }
    
    // Finish Editing The Text Field
    func textFieldDidEndEditing(_ textField: UITextField) {
        moveTextField(textField, moveDistance: -250, up: false)
    }
    
    // Hide the keyboard when the return key pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }}

class MessagesChatTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mImageView: UIImageView!
    @IBOutlet weak var mSenderImg: UIImageView!
    @IBOutlet weak var muserImage: UIImageView!
    @IBOutlet weak var mLabel: UILabel!
    @IBOutlet weak var mLabel1: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

