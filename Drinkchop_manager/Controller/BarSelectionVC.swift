//
//  BarSelectionVC.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 12/06/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit
import NotificationBannerSwift
import Alamofire
import Toast_Swift
import CoreLocation
import SVProgressHUD
import iOSDropDown

class BarSelectionVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, CLLocationManagerDelegate, UITextFieldDelegate{

    @IBOutlet weak var stateDropDown: DropDown!
    
    @IBOutlet weak var countryDropDown: DropDown!
    @IBOutlet weak var radiusTF: UITextField!
    @IBOutlet weak var longitudeTF: UITextField!
    @IBOutlet weak var latitudeTF: UITextField!
    @IBOutlet weak var zipcodeTF: UITextField!
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var stateTF: UITextField!
    @IBOutlet weak var countryTF: UITextField!
    @IBOutlet weak var addressTF: UITextField!
    @IBOutlet weak var instalinkTF: UITextField!
    @IBOutlet weak var twitterLinkTF: UITextField!
    @IBOutlet weak var fbLinkTF: UITextField!
    @IBOutlet weak var descriptionTV: UITextView!
    @IBOutlet weak var barImage: UIImageView!
    @IBOutlet weak var timeToTF: UITextField!
    
    @IBOutlet weak var taxTF: UITextField!
    @IBOutlet weak var barOpenCloseOutlet: UIButton!
    @IBOutlet weak var timeFromTF: UITextField!
    @IBOutlet weak var barNameTF: UITextField!
    @IBOutlet weak var weakCOllectionView: UICollectionView!
    
    
    let timePicker = UIDatePicker()
    var daysModel = [Days]()
    var cordinatesAry = [String]()
    var isBarOpen = false
    var dayName = String()
    var daysAry = NSArray()
    var stateNameAry = [String]()
    var selectedCountry : String?
     var isCountry = 1
    var weakDaysAry = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
       var validator = Validators()
        var countrynameAry = [String]()
    var idOfSelctedCountry : String?
  var countryModel = [countries]()
    var barUpdateMdl : BarUpdate!
    
    override func viewDidLoad() {
       
        super.viewDidLoad()
        
    //  Hide keyboard on TextField
         self.timeToTF.inputView = UIView()
        self.timeFromTF.inputView = UIView()
        
        self.getBarDetails()
        getCountryistAPI()
        countryDropDown.delegate = self
        stateDropDown.delegate = self
weakCOllectionView.delegate = self
        weakCOllectionView.dataSource = self
        timeFromTF.delegate = self
        timeToTF.delegate = self
        weakCOllectionView.allowsSelection = true
        self.barUpdateMdl = BarUpdate.init(dictionary: NSDictionary())
        // Do any additional setup after loading the view.
    }
    func openTimePicker()  {
        timePicker.datePickerMode = UIDatePicker.Mode.time
        timePicker.frame = CGRect(x: 0.0, y: (self.view.frame.height/2 + 60), width: self.view.frame.width, height: 150.0)
        timePicker.backgroundColor = UIColor.white
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.dateFormat = "HH:mm"
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        // add toolbar to textField
        timeFromTF.inputAccessoryView = toolbar
        
        self.view.addSubview(timePicker)
      
    }
    @objc func donedatePicker(sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.dateFormat = "HH:mm"
        timeFromTF.text = formatter.string(from: timePicker.date)
        self.view.endEditing(true)
        timePicker.removeFromSuperview() // if you want to remove time picker
    }
    @objc func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }
    @IBAction func backBtn(_ sender: Any) {
    navigationController?.popViewController(animated: true)
    }
    @IBAction func barOpenCloseBtn(_ sender: Any) {
        if barOpenCloseOutlet.backgroundImage(for: .normal) == #imageLiteral(resourceName: "boxSelected")
        {
            barOpenCloseOutlet.setBackgroundImage(#imageLiteral(resourceName: "boxBlank"), for: .normal)
        }
        else if barOpenCloseOutlet.backgroundImage(for: .normal) == #imageLiteral(resourceName: "boxBlank")
        {
             barOpenCloseOutlet.setBackgroundImage(#imageLiteral(resourceName: "boxSelected"), for: .normal)
        }
        //  bar update model
            var dayFetch = String()
            print(daysAry)
        let daysArrayFromModel = self.barUpdateMdl.days
        
        // Commented barstatus problem occurs
        
//        for (index, dy) in (daysArrayFromModel?.enumerated())!
//            {
//                dayFetch = dy.day ?? ""
//                print(dayName)
//                if dayName == dayFetch
//                {
//                    let status = daysArrayFromModel![index].barstatus
//                    if barOpenCloseOutlet.backgroundImage(for: .normal) == #imageLiteral(resourceName: "boxSelected")
//                    {
//                        dy.barstatus = true
//                        self.daysModel[index].barstatus = true
//                        print(dy.barstatus)
//
//                    }
//                    else
//                    {
//                        dy.barstatus = false
//                        self.daysModel[index].barstatus = false
//                        print(dy.barstatus)
//                    }
//                }
//            }
    
    
    }
    @IBAction func fetchLocationBtn(_ sender: Any) {
        
        var locManager = CLLocationManager()
        locManager.requestWhenInUseAuthorization()
        if( CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() ==  .authorizedAlways){
            
           var currentLocation = locManager.location
            self.latitudeTF.text = "\(currentLocation!.coordinate.latitude)"
            print(currentLocation!.coordinate.latitude)
            self.longitudeTF.text = "\(currentLocation!.coordinate.longitude)"

        }
        
    }
    @IBAction func saveBtn(_ sender: Any) {
        guard validator.validators(TF1: self.timeFromTF,fieldName: "Time From") == false ||
            validator.validators(TF1: self.timeToTF,fieldName: "Time To") == false ||
            validator.validators(TF1: self.taxTF,fieldName: "tax") == false
            || validator.validators(TF1: self.addressTF,fieldName: "Address") == false
            || validator.validators(TF1: self.countryTF,fieldName: "Country") == false
            || validator.validators(TF1: self.stateTF,fieldName: "State") == false
            || validator.validators(TF1: self.cityTF,fieldName: "City") == false
            || validator.validators(TF1: self.zipcodeTF,fieldName: "Zip Code") == false
            || validator.validators(TF1: self.latitudeTF,fieldName: "LAtitude Longitude") == false
            || validator.validators(TF1: self.radiusTF,fieldName: "Radius") == false
            else
        {
            if descriptionTV.text != nil
            {
                let boxSelected = #imageLiteral(resourceName: "boxSelected")
           if barOpenCloseOutlet.currentBackgroundImage == boxSelected
           {
            isBarOpen = true
                }
               // barUpdateProfile(userDict:barUpdateMdl)
                 updateBarDetailsAPI()
            }
            else
            {
                let banner = NotificationBanner(title: "ERROR", subtitle: "Description", style: .danger)
                banner.duration = 1
                banner.show()
            }
        return
        }
    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return weakDaysAry.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "daysCell", for: indexPath as IndexPath) as! DaysCollectionViewCell
       // cell.weakSelectionOutlet.tag = indexPath.row
        cell.mDayBtnOutl.tag = (indexPath.section * 100) + indexPath.item
        print(daysAry)
        cell.mDayNameLbl.text = weakDaysAry[indexPath.row] as! String
    cell.mDayBtnOutl.addTarget(self, action: #selector(daysBtnClick(sender:)), for: .touchUpInside)
    
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "daysCell", for: indexPath as IndexPath) as! DaysCollectionViewCell
          cell.mDayNameLbl.text = "Testing"
//        let CircleBlank = UIImage(named: "CircleBlank") as UIImage?
//        let CircleFill = UIImage(named: "CircleFill") as UIImage?
        if cell.mDayBtnOutl.backgroundImage(for: .normal) == #imageLiteral(resourceName: "CircleBlank")
        {
            cell.mDayBtnOutl.setBackgroundImage(#imageLiteral(resourceName: "CircleFill"), for: .normal)

        }
        else if cell.mDayBtnOutl.backgroundImage(for: .normal) == #imageLiteral(resourceName: "CircleFill")
        {
            cell.mDayBtnOutl.setBackgroundImage(#imageLiteral(resourceName: "CircleBlank"), for: .normal)

        }
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
//        let CircleBlank = UIImage(named: "CircleBlank") as UIImage?
//        let CircleFill = UIImage(named: "CircleFill") as UIImage?
//           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "weakCell", for: indexPath as IndexPath) as! weakCell
//        cell.weakSelectionOutlet.setBackgroundImage(CircleBlank, for: .normal)

    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        self.openTimePicker()
       
        return true
    }
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField == countryDropDown
//        {
//            return false
//
//        }
//        if textField == stateDropDown
//        {
//            return false
//
//        }
//
//        if textField == timeFromTF
//        {
//
//            return false
//
//        }
//        if textField == timeToTF
//        {
//            return false
//
//        }
//
//        return true
//    }
    func textFieldDidEndEditing(_ textField: UITextField) {
       
        if textField == timeFromTF
        {
            var dayFetch = String()
            print(daysAry)
            for (index, dy) in (barUpdateMdl.days?.enumerated())!
            {
               dayFetch = dy.day ?? ""
                print(dayName)
                if dayName == dayFetch
                {
                    let strtTme = timeFromTF.text
                    self.daysModel[index].starttime = strtTme
                    dy.starttime = strtTme
                    print(self.daysModel[index].starttime)
                }
            }
        
        }
        
        if textField == timeToTF
        {
            var dayFetch = String()
            print(daysAry)
            for (index, dy) in (barUpdateMdl.days?.enumerated())!
            {
                dayFetch = dy.day ?? ""
                print(dayName)
                if dayName == dayFetch
                {
                    let endTme = timeToTF.text
                    self.daysModel[index].endtime = endTme
                    dy.endtime = endTme

                    print(self.daysModel[index].endtime)
                }
            }
            
            
        }
        
    
        
        
        
        
    }
    @objc func daysBtnClick(sender:UIButton!)
    {
        let CircleBlank = UIImage(named: "boxBlank") as UIImage?
         let CircleFill = UIImage(named: "boxSelected") as UIImage?
       
        let button = sender as? UIButton
        let point: CGPoint = button!.convert(.zero, to: weakCOllectionView)
            let indexPath = weakCOllectionView!.indexPathForItem(at: point)
            let cell = weakCOllectionView!.cellForItem(at: indexPath!) as! DaysCollectionViewCell
  
        print("Indexpath is : \(indexPath)")
        
       
        if sender.tag == 0
        {
            print(barUpdateMdl.days?.count)
            dayName = "Monday"
            var barstatus = Bool()
            for dysData in (barUpdateMdl?.days)!
            {
               
                let daydata = dysData
                let day = daydata.day as! String
                if day == "Monday"
                {
                    
                    let starttime = daydata.starttime as! String
                    self.timeFromTF.text = starttime
                    let endtime = daydata.endtime as! String
                    self.timeToTF.text = endtime
                 // Commented barstatus problem occurs
                    
                    //     barstatus = daydata.barstatus! as NSNumber as! Bool
                    if barstatus == true
                    {
                        
                        barOpenCloseOutlet.setBackgroundImage(CircleFill, for: .normal)
                    }
                    else
                    {
                        barOpenCloseOutlet.setBackgroundImage(CircleBlank, for: .normal)
                    }
                }
            }
            
            
        }
        
       else if sender.tag == 1
        {
             dayName = "Tuesday"
            for dysData in daysAry
            {
                var daydata = dysData as! [String: AnyObject]
                var day = daydata["day"] as! String
                if day == "Tuesday"
                {
                    var starttime = daydata["starttime"] as! String
                    self.timeFromTF.text = starttime
                    var endtime = daydata["endtime"] as! String
                    self.timeToTF.text = endtime
                    var barstatus = daydata["barstatus"] as! Bool
                    if barstatus == true
                    {

                       barOpenCloseOutlet.setBackgroundImage(CircleFill, for: .normal)
                    }
                    else
                    {
                        barOpenCloseOutlet.setBackgroundImage(CircleBlank, for: .normal)
                    }

                }
            }
        }

       else if sender.tag == 2
        {
              dayName = "Wednesday"
            for dysData in daysAry
            {
                var daydata = dysData as! [String: AnyObject]
                var day = daydata["day"] as! String
                if day == "Wednesday"
                {
                    var starttime = daydata["starttime"] as! String
                    self.timeFromTF.text = starttime
                    var endtime = daydata["endtime"] as! String
                    self.timeToTF.text = endtime
                    var barstatus = daydata["barstatus"] as! Bool
                    if barstatus == true
                    {

                        barOpenCloseOutlet.setBackgroundImage(CircleFill, for: .normal)
                    }
                    else
                    {
                        barOpenCloseOutlet.setBackgroundImage(CircleBlank, for: .normal)
                    }

                }
            }
        }

       else if sender.tag == 3
        {
            dayName = "Thursday"

            for dysData in daysAry
            {
                var daydata = dysData as! [String: AnyObject]
                var day = daydata["day"] as! String
                if day == "Thursday"
                {
                    var starttime = daydata["starttime"] as! String
                    self.timeFromTF.text = starttime
                    var endtime = daydata["endtime"] as! String
                    self.timeToTF.text = endtime
                    var barstatus = daydata["barstatus"] as! Bool
                    if barstatus == true
                    {

                    barOpenCloseOutlet.setBackgroundImage(CircleFill, for: .normal)
                    }
                    else
                    {
                        barOpenCloseOutlet.setBackgroundImage(CircleBlank, for: .normal)
                    }

                }
            }
        }

       else if sender.tag == 4
        {
            dayName = "Friday"

            for dysData in daysAry
            {
                var daydata = dysData as! [String: AnyObject]
                var day = daydata["day"] as! String
                if day == "Friday"
                {
                    var starttime = daydata["starttime"] as! String
                    self.timeFromTF.text = starttime
                    var endtime = daydata["endtime"] as! String
                    self.timeToTF.text = endtime
                    var barstatus = daydata["barstatus"] as! Bool
                    if barstatus == true
                    {

                        barOpenCloseOutlet.setBackgroundImage(CircleFill, for: .normal)
                    }
                    else
                    {
                         barOpenCloseOutlet.setBackgroundImage(CircleBlank, for: .normal)
                    }
                    

                }
            }
        }

       else if sender.tag == 5
        {
            dayName = "Saturday"

            for dysData in daysAry
            {
                var daydata = dysData as! [String: AnyObject]
                var day = daydata["day"] as! String
                if day == "Saturday"
                {
                    var starttime = daydata["starttime"] as! String
                    self.timeFromTF.text = starttime
                    var endtime = daydata["endtime"] as! String
                    self.timeToTF.text = endtime
                    var barstatus = daydata["barstatus"] as! Bool
                    if barstatus == true
                    {

                        barOpenCloseOutlet.setBackgroundImage(CircleFill, for: .normal)
                    }
                    else
                    {
                        barOpenCloseOutlet.setBackgroundImage(CircleBlank, for: .normal)
                    }

                }
            }
        }

       else if sender.tag == 6
        {
            dayName = "Sunday"
            for dysData in daysAry
            {
                var daydata = dysData as! [String: AnyObject]
                var day = daydata["day"] as! String
                if day == "Sunday"
                {
                    var starttime = daydata["starttime"] as! String
                    self.timeFromTF.text = starttime
                    var endtime = daydata["endtime"] as! String
                    self.timeToTF.text = endtime
                    var barstatus = daydata["barstatus"] as! Bool
                    if barstatus == true
                    {

                        barOpenCloseOutlet.setBackgroundImage(CircleFill, for: .normal)
                    }
                    else
                    {
                        barOpenCloseOutlet.setBackgroundImage(CircleBlank, for: .normal)
                    }
                }
            }
        }
        
    
        
        self.weakCOllectionView.reloadData()
        
        
    }
    
//    // MARK: User Update Function
//    func barUpdateProfile(userDict:BarUpdate){
//        self.cordinatesAry.removeAll()
//        self.cordinatesAry.append(latitudeTF.text!)
//        self.cordinatesAry.append(longitudeTF.text!)
//        SVProgressHUD.show(withStatus: "Please Wait")
//        UserAPI().barUpdate(userDetials: userDict){ (isSuccess,response, error) -> Void in
//            SVProgressHUD.dismiss()
//
//            if (isSuccess){
//                SVProgressHUD.dismiss()
//
//                Constants.kUserDefaults.set(true, forKey:"isLogin")
//                self.navigationController?.popViewController(animated: true)
//            }else{
//                SVProgressHUD.dismiss()
//                if error != nil{
//                    kAppDelegate.showNotification(text: "Something went wrong!")
//                    //kAppDelegate.showNotification(text: error!)
//                }else{
//                    kAppDelegate.showNotification(text: "Something went wrong!")
//                }
//            }
//
//        }
//
//
//    }
    
    // update Bar Details API
    func updateBarDetailsAPI()
    {
        self.cordinatesAry.removeAll()
        self.cordinatesAry.append(latitudeTF.text!)
        self.cordinatesAry.append(longitudeTF.text!)
        let token = Constants.kUserDefaults.string(forKey: appConstants.token)
        print(token)
          let barId = Constants.kUserDefaults.string(forKey: "barId")!
        let urlString = remoteConfig.mBaseUrl + "bars/" + barId
        print(urlString)
        print(self.daysModel)
        let _headers : HTTPHeaders = ["x-access-token": token!]
        self.barUpdateMdl.radius = radiusTF.text
        
        var allDaysData = [NSDictionary]()
        allDaysData.removeAll()
        for mdlData in (self.barUpdateMdl?.days)!
        {
              // Commented barstatus problem occurs
            
       //    let barststus = mdlData.barstatus
            let day = mdlData.day
            let starttime = mdlData.starttime
            let endtime = mdlData.endtime
            let coverfee = mdlData.newcoverfee
            
               // Commented barstatus problem occurs
            //  "barstatus": Bool(barststus as! NSNumber),

            let mdlDic = ["day": day, "starttime": starttime, "endtime": endtime, "newcoverfee": coverfee] as [String : Any]
            allDaysData.append(mdlDic as NSDictionary)
            print(mdlDic)
        }
        print(allDaysData)

        let params: Parameters = self.barUpdateMdl!.dictionaryRepresentation() as! Dictionary<String,AnyObject>
        print(params)
        request(urlString, method: .put, parameters: params, encoding: URLEncoding.httpBody , headers: _headers).responseJSON(completionHandler: {
            response in response
            
            print(response.result.value)
            if response.result.isSuccess == true
            {
                self.view.makeToast("Save successfully")
                
                let jsonResponse = response.result.value as! NSDictionary
                print(jsonResponse)
                let jsonData = jsonResponse["data"] as! [String: AnyObject]
                print(jsonData)
                
                let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self.navigationController?.pushViewController(destinationvc, animated: true)
               
            }
            
            
        })
    }
    
    // Get user Details
    func getBarDetails() {
        let userId = Constants.kUserDefaults.string(forKey: "barId")!
        UserAPI().getBarDataa(userId: userId,pageNo: 0) { (data, error) in
            if data[APIConstants.isSuccess.rawValue] as! Bool == true {
                
                let userDict = data["data"] as! [String: AnyObject]
                print(userDict)
        
                let dataProfile = NSKeyedArchiver.archivedData(withRootObject: userDict)
              
                let name = userDict["name"] as! String
                let imgUrl = userDict["imgUrl"] as? String
                let tax = userDict["tax"] as? Int
                let destcription = userDict["destcription"] as? String
                let twitterLink = userDict["twitterLink"] as? String
                let fbLink = userDict["fbLink"] as? String
                let instagramLink = userDict["instagramLink"] as? String
                let country = userDict["country"] as? String
                let state = userDict["state"] as? String
                let city = userDict["city"] as? String
                let zipCode = userDict["zipCode"] as? Int

                let radius = userDict["radius"] as? Int
               
                let location = userDict["location"] as? [String: AnyObject]
                let address = location?["address"] as! String
                let coordinates = location?["coordinates"] as? NSArray
                print(coordinates)
                let lat = coordinates?[0] as! Double
                let long = coordinates?[1] as! Double
                self.daysAry = userDict["days"] as! NSArray
                print(self.daysAry)
//                for dy in days
//                {
//                    let daysData = dy as! [String: AnyObject]
//
//                }
               self.daysModel = Days.modelsFromDictionaryArray(array: self.daysAry as NSArray)
                self.barUpdateMdl = BarUpdate.init(dictionary: userDict as NSDictionary)
                print(self.barUpdateMdl.dictionaryRepresentation())
                if name != nil {
                    
                self.barNameTF.text = name
                    self.taxTF.text = tax?.description
                    self.descriptionTV.text = destcription
                    self.twitterLinkTF.text = twitterLink
                    self.fbLinkTF.text = fbLink
                    self.instalinkTF.text = instagramLink
                    self.countryTF.text = country
                    self.stateTF.text = state
                    self.cityTF.text = city
                    self.zipcodeTF.text = zipCode?.description
                    self.radiusTF.text = radius?.description
                    self.latitudeTF.text = lat.description
                    self.longitudeTF.text = long.description
                    self.addressTF.text = address
                }
//                let myUser = User.init(dictionary: userDict as! NSDictionary)
                
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }
    
    func getCountryistAPI() {
        //SVProgressHUD.show(withStatus: "Loading...")
        let tokenString = Constants.kUserDefaults.value(forKey: "token")
        
        let headers: HTTPHeaders = [
            "x-access-token": tokenString as! String,
            ]
        print(headers)
        let urlString = "http://159.89.226.199:8010/v1/admin/state?type=" + isCountry.description
        print(urlString)
        Alamofire.request(urlString, method: .get, headers: headers).responseJSON {
            response in
            switch response.result{
            case .success(_):
                let jsonData = response.result.value
                print(jsonData!)
                //                var allcountries = jsonData?["data"] as? [[String: AnyObject]]
                //                print(allcountries)
                if let JSON = response.result.value as? NSDictionary
                {
                    let status:Int = (JSON["success"] as? Int)!
                    print(JSON)
                    if status == 1{
                        SVProgressHUD.dismiss()
                        let jsonArray:NSArray = (JSON["data"] as? NSArray)!
                        print(jsonArray)
                        countries.modelsFromDictionaryArray(array: jsonArray)
                        self.countrynameAry.removeAll()
                        
                        for countryData in jsonArray
                        {
                            let conData = countryData as! [String: AnyObject]
                            let name = conData["name"] as? String
                            print(name)
                            self.countrynameAry.append(name!)
                        }
                        print(self.countrynameAry)
                        self.countryModel = countries.modelsFromDictionaryArray(array: jsonArray)
                        print(self.countryModel)
                        self.countryDropDown.optionArray = self.countrynameAry
                        self.countryDropDown.didSelect{(selectedText , index ,id) in
                            self.countryDropDown.text = selectedText
                            self.selectedCountry = selectedText
                            self.countryDropDown.arrowColor = .orange
                            
                            for data1 in self.countryModel
                            {
                                print(data1.name)
                                print(data1.id)
                                print(self.selectedCountry)
                                if data1.name == self.selectedCountry! {
                                    self.idOfSelctedCountry = data1.id
                                }
                                print(self.idOfSelctedCountry)
                            }
                            if self.idOfSelctedCountry != nil
                            {
                                self.getStateListAPI()
                            }
                            
                        }
                        
                        
                    }else{
                        SVProgressHUD.dismiss()
                        print("Fave api not call succes")
                        
                    }
                }
            case .failure(_):
                print("Faield Trinash")
                
            }}
    }
    
    func getStateListAPI() {
        //SVProgressHUD.show(withStatus: "Loading...")
        let tokenString = Constants.kUserDefaults.value(forKey: "token")
        
        let headers: HTTPHeaders = [
            "x-access-token": tokenString as! String,
            ]
        print(headers)
        let urlString = "http://159.89.226.199:8010/v1/admin/state?type=" + 2.description + "&countryId=\(self.idOfSelctedCountry!)"
        print(urlString)
        Alamofire.request(urlString, method: .get, headers: headers).responseJSON {
            response in
            switch response.result{
            case .success(_):
                let jsonData = response.result.value
                print(jsonData!)
                //                var allcountries = jsonData?["data"] as? [[String: AnyObject]]
                //                print(allcountries)
                if let JSON = response.result.value as? NSDictionary
                {
                    let status:Int = (JSON["success"] as? Int)!
                    print(JSON)
                    if status == 1{
                        SVProgressHUD.dismiss()
                        let jsonArray:NSArray = (JSON["data"] as? NSArray)!
                        print(jsonArray)
                        countries.modelsFromDictionaryArray(array: jsonArray)
                        self.stateNameAry.removeAll()
                        for stateData in jsonArray
                        {
                            let conData = stateData as! [String: AnyObject]
                            let name = conData["name"] as? String
                            print(name)
                            self.stateNameAry.append(name!)
                        }
                        self.stateTF.text = self.stateNameAry[0]
                        print(self.stateNameAry)
                        self.stateDropDown.optionArray = self.stateNameAry
                        self.stateDropDown.didSelect{(selectedText , index ,id) in
                            self.stateDropDown.text = selectedText
                            
                            
                            
                        }
                        
                        
                    }else{
                        SVProgressHUD.dismiss()
                        print("Fave api not call succes")
                        
                    }
                }
            case .failure(_):
                print("Faield Trinash")
                
            }}
    }
    
}

class weakCell: UICollectionViewCell {
    
    @IBOutlet weak var weakSelectionOutlet: UIButton!
    @IBOutlet weak var weakLbl: UILabel!
}
