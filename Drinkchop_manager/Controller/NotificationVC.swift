//
//  NotificationVC.swift
//  drinkchop-ios
//
//  Created by Tech Farmerz on 03/12/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire

class NotificationVC: UIViewController {

    @IBOutlet weak var mnotifiactiontableview: UITableView!
    
      var rowArr = Array<Any>()
    var discription = String()
    var convertedDate = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getNotificationApi()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func mBackbtnAct(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func getNotificationApi() {
        SVProgressHUD.setStatus("Loading.......")
        SVProgressHUD.setDefaultMaskType(.clear)
        let tokenString = Constants.kUserDefaults.value(forKey: "token")
        
        let headers: HTTPHeaders = [
            "x-access-token": tokenString as! String,
            ]
        let urlString = remoteConfig.mBaseUrl + "bartableUsers"
        print(urlString)
        //        let urlString:String = String(format: "http://18.222.249.193:3006/api/bars/%@/%@",favName, BarDictionaryInfo.id_Bar!)
        Alamofire.request(urlString, method: .get, parameters: nil, headers: headers).responseJSON {
            response in
            switch response.result{
            case .success(_):
                if let JSON = response.result.value as? NSDictionary{
                    print("json get by id-- \(JSON)")

                  
                    
                    let status:Int = (JSON["isSuccess"] as? Int)!
                    //  BarDictionaryInfo = JSON["data"]
              
                    if status == 1{
                        //    print(items)
                          let items = JSON["items"] as! [[String: AnyObject]]
                        for data in items
                        {
                            print(data)
                            self.discription = data["description"] as! String
                            
                            let dateFromAPI = data["updatedat"] as! String
                            print("Date from API is \(dateFromAPI)")
                            
                            let ConvertToString = String(dateFromAPI)
                            print ("my date substring is \(dateFromAPI)")
                            self.convertedDate = ConvertToString.utcStringToSmartDate1
                            print("converted date is \(self.convertedDate)")
                            
                            self.rowArr.append(data)
                            print(self.rowArr)
                        }
                        self.mnotifiactiontableview.reloadData()
                        SVProgressHUD.dismiss()
                    }else{
                        SVProgressHUD.dismiss()
                    }
                }
            case .failure(_):
                print("Faield Trinash")
                let alert = UIAlertController(title: "Oops!", message: "Server Error....", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }}
        
    }

}
extension NotificationVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.rowArr.count == 0 || self.rowArr.isEmpty == true || self.rowArr == nil
        {
            self.mnotifiactiontableview.setEmptyMessage("You do not have any notifications", tablename: self.mnotifiactiontableview)
        }
        else
        {
            self.mnotifiactiontableview.restore()
        }
        return rowArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = mnotifiactiontableview.dequeueReusableCell(withIdentifier: "NotificationCell")as! NotificationCell
        cell.selectionStyle = .none
        mnotifiactiontableview.tableFooterView = UIView()
        
        cell.notificationDate.text = convertedDate
        cell.notificationTitle.text = discription
        cell.notificationImg.cornerRadius = cell.notificationImg.frame.size.width / 2
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //    let vc = storyboard?.instantiateViewController(withIdentifier: "ProductDetailVCID")as! ProductDetailVC
        //       navigationController?.pushViewController(vc, animated: true)
        
    }
}
class NotificationCell: UITableViewCell {
    @IBOutlet weak var notificationImg: UIImageView!
    @IBOutlet weak var notificationTitle: UILabel!
    
    @IBOutlet weak var notificationDate: UILabel!
    
    
    
}
