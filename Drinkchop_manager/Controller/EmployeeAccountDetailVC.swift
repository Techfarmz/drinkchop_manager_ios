//
//  EmployeeAccountDetailVC.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 20/02/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit

class EmployeeAccountDetailVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var bartenderTableview: UITableView!
    var doorpersonAry = [String]()
    var bartenderAry =  [String]()
    var EmployeeName: String!
    var userCombineName: String!
    
    let dataArr = ["prabhjot","shubham"]
    var persons = ["bartender", "doorperson"]
    var role: String?
    override func viewDidLoad() {
        super.viewDidLoad()

 self.bartenderTableview.separatorStyle = .none
        bartenderTableview.delegate = self
        bartenderTableview.dataSource = self
        
        bartenderTableview.estimatedRowHeight = 70.0
        bartenderTableview.rowHeight = UITableView.automaticDimension
        
        // Do any additional setup after loading the view.
        
//       var yPos = 90
//        for i in 0..<dataArr.count {
//            let element = dataArr[i]
//            let labelNum = UILabel()
//            let split = element.components(separatedBy: ",")
//
//            let num1 = split[0]
//            let num2 = split[1]
//
//            let num1Nnum2 = "number 1 :" + num1 + " number 2:" + num2
//            labelNum.text = num1Nnum2
//            labelNum.textAlignment = .center
//            labelNum.frame = CGRect( x:10, y:yPos, width:250, height: 80)
//            yPos += 80
//            self.view.addSubview(labelNum)
//
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getUEmployeeAccounts()
        self.doorpersonAry.removeAll()
        self.bartenderAry.removeAll()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    @IBAction func backBtn(_ sender: Any) {
   navigationController?.popViewController(animated: true)
    }
    @IBAction func addnewAccount(_ sender: Any) {
        let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "NewEmployeeAccountVC") as! NewEmployeeAccountVC
        self.navigationController?.pushViewController(destinationvc, animated: true)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 31
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0
        {
            return bartenderAry.count

        }
        
        else
        {
            return doorpersonAry.count

        }
        
//        if bartenderAry != nil
//        {
//
//          return bartenderAry.count
//        }
//        if doorpersonAry != nil
//        {
//            return doorpersonAry.count
//        }
    }
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let view = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:18))
//        let label = UILabel(frame: CGRect(x:10, y:5, width:tableView.frame.size.width, height:18))
//        label.font = UIFont.systemFont(ofSize: 20)
//        label.textColor = UIColor.white
//        label.text = "This is a test";
//        view.addSubview(label);
//        view.backgroundColor = UIColor.clear;
//
//        let labelLine = UILabel(frame: CGRect(x:1, y:1, width:tableView.frame.size.width, height:1))
//        labelLine.backgroundColor = UIColor.lightGray
//        view.addSubview(labelLine);
//
//        return view
//
//
//    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
       (view as! UITableViewHeaderFooterView).backgroundView?.backgroundColor = UIColor.clear
       (view as! UITableViewHeaderFooterView).textLabel?.textColor = UIColor.white
        let headerView = view as! UITableViewHeaderFooterView
    let labelLine = UILabel(frame: CGRect(x:1, y:1, width:view.frame.size.width, height:1))
        labelLine.backgroundColor = UIColor.lightGray
        view.addSubview(labelLine);
        
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        let sectionName: String
        switch section {
        case 0:
            sectionName = NSLocalizedString("Bartender(s)", comment: "Bartender(s)")
        case 1:
            sectionName = NSLocalizedString("Doorperson(s)", comment: "Doorperson(s)")
        // ...
        default:
            sectionName = ""
        }
        return sectionName
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmployeeBartenderCell")as! EmployeeBartenderCell

        // Mark Clear user Selection
        cell.selectionStyle = .none
     //   cell.employeeType.text = persons[indexPath.section]
        cell.employeeName?.numberOfLines = 0
       if doorpersonAry != nil
       {
        if indexPath.section == 0
        {
           
            cell.employeeName.text = bartenderAry[indexPath.row]
        }
        if indexPath.section == 1
        {
             cell.employeeName.text = doorpersonAry[indexPath.row]
        }
       // let personNameString = doorpersonAry[indexPath.row]
       // print(personNameString)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentCell = tableView.cellForRow(at: indexPath) as! EmployeeBartenderCell
        EmployeeName = currentCell.employeeName.text
        print(EmployeeName!)
        getUEmployeeId()
    }
    // Get Emploee Account Details
    func getUEmployeeAccounts() {
        let userId = Constants.kUserDefaults.value(forKey: appConstants.userId) as? String
        UserAPI().getBartenderDoorperson(userId: userId!,pageNo: 0) { (data, error) in
            if data[APIConstants.isSuccess.rawValue] as! Bool == true {
                if let userDict = data["items"] as? [[String:AnyObject]]
               {
                for i in userDict
                {
                    
                    let role = i["role"] as! String
                   // print("role of the user is\(role)")
                    if role == "doorPerson"
                    {
                        let name = i["firstName"] as! String
                        print(name)
                        let lastName = i["lastName"] as! String
                        print(lastName)
                        let combineName = "\(name) \(lastName)"
                        self.doorpersonAry.append(combineName)
                        print(self.doorpersonAry)
                       
           //For Fetch Current user id
                        
                       
                    }
                    if role == "bartender"
                    {
                        let name = i["firstName"] as! String
                        print(name)
                        let lastName = i["lastName"] as! String
                        print(lastName)
                        let combineName = "\(name) \(lastName)"
                        self.bartenderAry.append(combineName)
                        print(self.bartenderAry)
                    }
                }
                self.bartenderTableview.reloadData()
                }
//                let dataProfile = NSKeyedArchiver.archivedData(withRootObject: userDict)
//                print(userDict)
//
//
//                let bar = userDict?["bar"]
//                print(bar)
//                //    Constants.kUserDefaults.set(dataProfile, forKey: appConstants.profile)
//
//                let firstName = userDict!["firstName"] as! String
             
  //              if firstName != nil {
//                    self.firstNameTF.text = firstName
//                    self.lastNameTF.text = lastName
//                    self.emailUserNameTF.text = email
//                    self.phoneNumberTF.text = phone
//                    self.genderTF.text = gender
   //             }
     //           let lgaVar = userDict?["lga"] as? String
     //           let myUser = User.init(dictionary: userDict as! NSDictionary)
                
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }
    // Get Emploee Account ID
    func getUEmployeeId() {
        let userId = Constants.kUserDefaults.value(forKey: appConstants.userId) as? String
        UserAPI().getBartenderDoorperson(userId: userId!,pageNo: 0) { (data, error) in
            if data[APIConstants.isSuccess.rawValue] as! Bool == true {
                if let userDict = data["items"] as? [[String:AnyObject]]
                {
                    for i in userDict
                    {
                        let name = i["firstName"] as! String
                        print(name)
                        let lastName = i["lastName"] as! String
                        print(lastName)
                        self.userCombineName = "\(name) \(lastName)"
                        let role = i["role"] as! String
                        print(self.userCombineName)
                        if self.userCombineName == self.EmployeeName
                        {
                            let id = i["id"] as! String
                            print(id)
                            Constants.kUserDefaults.set(id, forKey: "currentUserId")
                            let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "EditEmployeeAccountVC") as! EditEmployeeAccountVC
                            self.navigationController?.pushViewController(destinationvc, animated: true)

                        }
                    }
                    self.bartenderTableview.reloadData()
                }
                
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }

}
class EmployeeBartenderCell: UITableViewCell {
    @IBOutlet weak var employeeName: UILabel!
    }
    

