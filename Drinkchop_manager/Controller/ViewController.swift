//
//  ViewController.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 18/02/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit
import SVProgressHUD
import NotificationBannerSwift

class ViewController: UIViewController {

    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var email: UITextField!
    
    var validator = Validators()
     var role = "manager"
     private var userApi : UserAPI!
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view, typically from a nib.
          self.userApi = UserAPI.sharedInstance
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    @IBAction func signInBtn(_ sender: Any) {
        guard validator.validators(TF1: self.email,fieldName: "Valid Email") == false || validator.validators(TF1: self.password,fieldName: "Password") == false
            else
        {
            print("validated")
            let userDict = ["username":self.email.text as Any,"password":self.password.text,"role":role as Any]
            self.userSignIn(userDict: userDict as Dictionary<String, AnyObject>)
            
            return
        }
    }
    func saveForUserDefaut(value:Any,keyName:String){
        UserDefaults.standard.set(value, forKey: keyName)
        UserDefaults.standard.synchronize()
    }
    // Sign In
    func userSignIn(userDict:Dictionary<String, AnyObject>)
        
    {
        SVProgressHUD.show(withStatus: "Please Wait")
        Constants.kUserDefaults.set(nil, forKey: appConstants.token)
        userApi.userSignIn(userDetials: userDict as Dictionary<String, AnyObject> ){ (isSuccess,response, error) -> Void in
            SVProgressHUD.dismiss()
            
            if (isSuccess){
                SVProgressHUD.dismiss()
                Constants.kUserDefaults.set(true, forKey: "isLogin")
                Constants.kUserDefaults.set(true, forKey: appConstants.isLoggedIn)
                Constants.kUserDefaults.set("active", forKey: UserAttributes.status.rawValue)
                Constants.kUserDefaults.set(self.password.text! as String, forKey: appConstants.password)
                let tokenStr = response!["data"]!["token"]! as! String
                Constants.kUserDefaults.set(tokenStr, forKey: appConstants.token)
            //    Constants.kUserDefaults.set(response!["data"], forKey: appConstants.userInfo)
            //    self.saveForUserDefaut(value: response!["data"] ?? AnyObject.self, keyName: appConstants.userInfo)
                let userInfo:NSDictionary = response!["data"] as! NSDictionary
                print(userInfo)
                let Year = userInfo["year"] as! String
                Constants.kUserDefaults.set(Year, forKey: "year")
                
                Constants.kUserDefaults.set(userInfo["referalCode"] as! String, forKey: appConstants.referalCode)
                Constants.kUserDefaults.set(userInfo["username"], forKey: appConstants.username)
                Constants.kUserDefaults.set(userInfo["email"], forKey: appConstants.email)
              Constants.kUserDefaults.set(userInfo["id"], forKey: appConstants.userId)
           
                var bar = userInfo["bar"] as? [String: AnyObject]
                var barid = bar!["id"] as! String
                print(barid)
                Constants.kUserDefaults.set(barid, forKey: "barId")
                var imageUrl = ""
                if (userInfo.object(forKey: "imgUrl") != nil){
                    imageUrl = String(format: "%@", userInfo.object(forKey: "imgUrl") as! CVarArg)
                }
                print(imageUrl)
                UserDefaults.standard.set(imageUrl, forKey: "profileimage")
             
                
                let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self.navigationController?.pushViewController(destinationvc, animated: true)
            }
            else{
                SVProgressHUD.dismiss()
                if error != nil{
                    kAppDelegate.showNotification(text: error!)
                    let banner = NotificationBanner(title: "ERROR", subtitle: "Invalid Username or Password", style: .danger)
                    banner.duration = 1
                    banner.show()
                }else{
                    kAppDelegate.showNotification(text: "Something went wrong!")
                    
                }
            }
            
        }
    }

}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
}
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
}
