//
//  PaymentInformationVC.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 20/02/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit
import iOSDropDown
import SVProgressHUD
import Stripe

class PaymentInformationVC: UIViewController , UITextFieldDelegate{

    @IBOutlet weak var countryDropDown: DropDown!
    @IBOutlet weak var accountNumberTF: UITextField!
    @IBOutlet weak var routingNumberTF: UITextField!
    @IBOutlet weak var accountNameTF: UITextField!
    @IBOutlet weak var bankNameTF: UITextField!
    @IBOutlet weak var countryTF: UITextField!

      var user: User?
    private var userApi : UserAPI!
    var validator = Validators()
    var stripeCountries = [
    "Australia",
    "Austria",
    "Belgium",
    "Brazil",
    "Canada",
    "Denmark",
    "Finland",
    "France",
    "Germany",
    "Hong Kong",
    "Ireland",
    "Japan",
    "Luxembourg",
    "Mexico",
    "Netherlands",
    "New Zealand",
    "Norway",
    "Singapore",
    "Spain",
    "Sweden",
    "Switzerland",
    "United Kingdom",
    "United States",
    "Italy",
    "Portugal"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getPaymentDetails()
      //  self.getPaymentDetails()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        countryTF.delegate = self
        countryDropDown.optionArray = stripeCountries
        countryDropDown.didSelect{(selectedText , index ,id) in
            self.countryTF.text = selectedText
            
        }
    }
    
    @IBAction func backBtn(_ sender: Any) {
    navigationController?.popViewController(animated: true)
    }
    @IBAction func saveBtn(_ sender: Any) {
        guard validator.validators(TF1: self.bankNameTF,fieldName: "Bank Name") == false ||
            validator.validators(TF1: self.accountNameTF,fieldName: "Account Holder Name") == false ||
            validator.validators(TF1: self.routingNumberTF,fieldName: "Routing Number") == false || validator.validators(TF1: self.accountNumberTF,fieldName: "Account Number") == false || validator.validators(TF1: self.countryTF,fieldName: "Country") == false
    else
        {
            self.user = User.init(dictionary: NSDictionary())
           
            
          
            self.user?.StripeAccountHolderName = accountNameTF.text
            self.user?.StripeAccountLast4Digit = accountNumberTF.text
            self.user?.routingNumber = routingNumberTF.text
            self.user?.bankName = bankNameTF.text
            self.user?.bankToken = ""
            self.user?.country = countryTF.text
            self.user?.StripeCurrency = ""
            self.user?.email = ""
            
            //  https://stackoverflow.com/questions/48189363/create-external-account-for-stripe-connect-payouts-with-stripe-js-ios
            
           // var bankAccountToken = req.body.stripeToken;
            
           return
        }
    }
    

        

        
        
    // Get Payment Details
    func getPaymentDetails() {
        let userId = Constants.kUserDefaults.value(forKey: appConstants.userId) as? String
        UserAPI().getUserPaymentInfo(userId: userId!,pageNo: 0) { (data, error) in
            if data[APIConstants.isSuccess.rawValue] as! Bool == true {
                let userDict = data["items"]
                let dataProfile = NSKeyedArchiver.archivedData(withRootObject: userDict)
                //    Constants.kUserDefaults.set(dataProfile, forKey: appConstants.profile)
                
                let bankName = userDict!["bankName"] as! String
                let accountHolderName = userDict!["StripeAccountHolderName"] as! String
                let accountNumber = userDict?["StripeAccountLast4Digit"] as? String
                let routingNumber = userDict?["routingNumber"] as? String
               
                
                if bankName != nil {
                    self.bankNameTF.text = bankName
                    self.accountNameTF.text = accountHolderName
                    self.routingNumberTF.text = routingNumber
                    self.accountNumberTF.text = "********" +  accountNumber!
               
                }
                let myUser = User.init(dictionary: userDict as! NSDictionary)
                
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }
    // MARK: Update Payment Details
    func updatePymentMethod(userDict:User){
        SVProgressHUD.show(withStatus: "Please Wait")
        userApi.paymentUpdate(userDetials: userDict){ (isSuccess,response, error) -> Void in
            SVProgressHUD.dismiss()
            
            if (isSuccess){
                SVProgressHUD.dismiss()
             
                let response = response
                print(response)
                let data = response!["data"] as! NSDictionary
//                let newEventId = data["id"] as! String
//                print(newEventId)
//                Constants.kUserDefaults.set(newEventId, forKey: "newEventId")
                
                self.navigationController?.popViewController(animated: true)
            }else{
                SVProgressHUD.dismiss()
                if error != nil{
                    //kAppDelegate.showNotification(text: error!)
                }else{
                    let response = response
                    
                    let message = response?["message"] as! String
                    print(message)
                    self.view.makeToast(message)
                }
            }
            
        }
        
        
    }
    
    
    }
    



