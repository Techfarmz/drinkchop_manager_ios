//
//  AddMembersVC.swift
//  DrinkchopBartender
//
//  Created by Gurpreet Gulati on 21/05/19.
//  Copyright © 2019 Temp. All rights reserved.
//

import UIKit
import Toast_Swift

class AddMembersVC: UIViewController {
    
    @IBOutlet weak var mMembersTblView: UITableView!
    
    var usersModelArray : [User]!
    var userAPI : UserAPI!
    var noOfPeopleSelected : Int = 0
    var idsOfSelectedUsers = [String]()
    var GroupName : String?
   

    override func viewDidLoad() {
        super.viewDidLoad()
        self.userAPI = UserAPI.sharedInstance
        self.usersModelArray = [User]()
        self.getAllUsers()
        self.mMembersTblView.tableFooterView = UIView()
    
    }
    
    //MARK: Get All Users
    func getAllUsers(name:String = "") {
        self.userAPI.getAllUsers(type: name) { (data, error) in
            if data[APIConstants.isSuccess.rawValue] as! Bool == true {
                if error == nil{
                    let userList = data[APIConstants.items.rawValue] as! NSArray
                    self.usersModelArray = User.modelsFromDictionaryArray(array: userList)
                    print("printing user model array : \(self.usersModelArray)")
                    self.mMembersTblView.delegate = self
                    self.mMembersTblView.dataSource = self
                    self.mMembersTblView.reloadData()
                    
                }}
            else{
                print("Getting Error")
                
            }
            
        }
    }
    
    

    @IBAction func mBackBtnTapped(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func mCreateGroupTapped(_ sender: UIButton) {
        if self.noOfPeopleSelected == 0 || self.idsOfSelectedUsers.count == 0 {
            self.view.makeToast("Select Members To Create A Group")
            return
        }
        
       else if self.noOfPeopleSelected == 1 || self.idsOfSelectedUsers.count == 1 {
            self.view.makeToast("Select Atleast 2 Members To Create A Group")
            return
        }
            
        else {
            let resultController = self.storyboard?.instantiateViewController(withIdentifier: "GroupNamePopUpVCID") as? GroupNamePopUpVC
            self.navigationController?.definesPresentationContext = true
            resultController?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            resultController?.modalTransitionStyle = .crossDissolve
            resultController?.delegate = self
            resultController!.idsOfSelectedUsers = self.idsOfSelectedUsers
            self.present(resultController!, animated: true, completion: nil)
        }
    }
    
    

}

extension AddMembersVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.usersModelArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.mMembersTblView.dequeueReusableCell(withIdentifier: "AddGroupMembersTblCellID")as! AddGroupMembersTblCell
        let userr = self.usersModelArray[indexPath.row]
        cell.mUserNameLbl.text = userr.firstName ?? "NA"
        if let role = userr.role {
            cell.mRoleLbl.text = "(\(role))"
        }
        
        if userr.imgUrl != nil {
       //     cell.mUserImgView.sd_setImage(with: URL.init(string:((userr.imgUrl!.httpsExtend))), placeholderImage: #imageLiteral(resourceName: "default_user_square"))
        }
        
        cell.mTickBtnOutl.tag = indexPath.row
        cell.mTickBtnOutl.addTarget(self, action: #selector(tickBtnAction), for: .touchUpInside)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    
    
    
    
    @objc func tickBtnAction(sender:UIButton) {
        let tag = sender.tag
        let user = self.usersModelArray[tag]
         let i = IndexPath(row: tag, section: 0)
        let cell = mMembersTblView.cellForRow(at: i)  as! AddGroupMembersTblCell
        if cell.mTickBtnOutl.backgroundImage(for: .normal) == #imageLiteral(resourceName: "uncheckedBox") {
            cell.mTickBtnOutl.setBackgroundImage(#imageLiteral(resourceName: "checkedBox"), for: .normal)
            self.noOfPeopleSelected += 1
            if let idOfSelectedCellUser = user.id {
                self.idsOfSelectedUsers.append(idOfSelectedCellUser)
                
            }
            
        }
        
        else if cell.mTickBtnOutl.backgroundImage(for: .normal) == #imageLiteral(resourceName: "checkedBox") {
            cell.mTickBtnOutl.setBackgroundImage(#imageLiteral(resourceName: "uncheckedBox"), for: .normal)
            if self.noOfPeopleSelected > 0 {
                self.noOfPeopleSelected -= 1
            }
            
            if let idOfSelectedCellUser = user.id {
                if let index = self.idsOfSelectedUsers.index(of: idOfSelectedCellUser) {
                    idsOfSelectedUsers.remove(at: index)
                }
                
            }
            
        }
        
        let rightBarButton = self.navigationItem.rightBarButtonItem
        rightBarButton?.addBadge(text: String(self.noOfPeopleSelected))
        print("No. of selected Users : \(self.noOfPeopleSelected)")
          print("Ids. of selected Users : \(self.idsOfSelectedUsers)")
        
    }

    
    
    
}

extension AddMembersVC : GroupNamePopUpVCDelegate {
    func setGroupName(grpName: String,isDataValid : Bool) {
        self.GroupName = grpName
        if isDataValid == true {
            let destinationVC = self.storyboard?.instantiateViewController(withIdentifier: "MessagesChatVCID") as! MessagesChatVC
            destinationVC.typeOfChat = "group"
            destinationVC.idsOfUsersInTheGroup = self.idsOfSelectedUsers
            destinationVC.isComingFromSearchUserVC = true
            destinationVC.groupName = grpName
            destinationVC.otherUserName = grpName
            self.navigationController?.pushViewController(destinationVC, animated: true)
            
        }
        
        
    }
    
    
}

class AddGroupMembersTblCell: UITableViewCell {
    
    @IBOutlet weak var mUserImgView: UIImageView!
    @IBOutlet weak var mUserNameLbl: UILabel!
    @IBOutlet weak var mRoleLbl: UILabel!
    @IBOutlet weak var mTickBtnOutl: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
}

