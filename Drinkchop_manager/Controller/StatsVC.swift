//
//  StatsVC.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 22/02/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit
import SVProgressHUD
import Charts
import SVProgressHUD
import Alamofire
import NotificationBannerSwift

class StatsVC: UIViewController, UITextFieldDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate {
    
    
    @IBOutlet weak var bFromChart: PieChartView!
    @IBOutlet weak var bartenderView: UIView!
    @IBOutlet weak var bOrders: UILabel!
    @IBOutlet weak var bTableRequest: UILabel!
    @IBOutlet weak var bSales: UILabel!
    @IBOutlet weak var bTotalOrderYellow: UIView!
    @IBOutlet weak var bTotalOrderRed: UIView!

    @IBOutlet weak var bSalesBreakdownChart: PieChartView!

    @IBOutlet weak var bTotalSalesYellow: UIView!
    @IBOutlet weak var bOrderFrequencyChart: LineChartView!
    @IBOutlet weak var bTotalTaxGreen: UIView!
    @IBOutlet weak var bTotalTipsRed: UIView!
    @IBOutlet weak var bTotalOrdersYellow: UIView!
    @IBOutlet weak var bTotalTimeRed: UIView!
    @IBOutlet weak var bTableRequestChart: LineChartView!
    @IBOutlet weak var bTableRequestYellow: UIView!
    @IBOutlet weak var bTableRequestRed: UIView!
    
    @IBOutlet weak var doorpersonView: UIView!
    @IBOutlet weak var dEntranceFrequencyChart: LineChartView!
    @IBOutlet weak var dManuallyQRCodeChart: PieChartView!
    
    
    @IBOutlet weak var dmanuallyRed: UIView!
    @IBOutlet weak var dqrcodeYellow: UIView!
    @IBOutlet weak var duserYellow: UIView!
    @IBOutlet weak var dtimeRed: UIView!
    @IBOutlet weak var selectEmployeeDropDown: UITextField!
    @IBOutlet weak var totalCoverFeeLbl: UILabel!
    @IBOutlet weak var totalEntranceLbl: UILabel!
    @IBOutlet weak var toTf: UITextField!
    @IBOutlet weak var fromTF: UITextField!
    @IBOutlet weak var employeeNameTF: UITextField!
    
    var tableRequestChartPoint = ["11.59 pm", "12.00 am"]
    var bTableRequestChartValues = [Double]()

    var dEntranceFrequencyChartValues = [Double]()
    var DentranceFrequencyChartPoint = ["11.59 pm", "12.00 am"]
    var bOrdrFrequencyChartValues = [Double]()
    var bOrdrFrequencyChartPoint = ["11.59 pm", "12.00 am"]
    var bSalesBreakdownChartPoint = ["Sales", "Tax", "Tips"]
    var salesBreakdownValues = [Double]()
     var dEntranceChartValue = [Double]()
    var bFromchartValues = [Double]()
    let pieChartPointsArry = ["Jan", "Feb"]
    var newStrtData = String()
    var newEndData = String()
    var modelArry = [User!]()
    var EmployeeAccounts = [[String:AnyObject]]()
    var employeeNameAry = [String]()
    var doorpersonAry = [String]()
    var bartenderAry = [String]()
    var pickerView = UIPickerView()
    var isotimeFromString : String?
    var isotimeToString : String?
    var isoDateString : String?
    let datePicker: UIDatePicker = UIDatePicker()
    private var userApi : UserAPI!
    var user: User?
    var validator = Validators()
    let dateFormat: DateFormatter = DateFormatter()
    var toolBar = UIToolbar()
    private var fileUploadAPI:FileUpload!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        doorpersonView.isHidden = true
     //   bartenderView.isHidden = true
         getUEmployeeAccounts()
        
        self.userApi = UserAPI.sharedInstance
        toTf.inputView = datePicker
        fromTF.inputView = datePicker
        toTf.inputAccessoryView = toolBar
        fromTF.inputAccessoryView = toolBar
        toTf.delegate = self
        fromTF.delegate = self
        selectEmployeeDropDown.delegate = self
        toolBar.sizeToFit()
        employeeNameTF.delegate = self
        pickerView.delegate = self
        pickerView.dataSource = self
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolBar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        // Make ui view Round
        bTotalOrderYellow.layer.cornerRadius = bTotalOrderYellow.frame.size.width/2
        bTotalOrderYellow.clipsToBounds = true
        
        bTotalOrderRed.layer.cornerRadius = bTotalOrderRed.frame.size.width/2
        bTotalOrderRed.clipsToBounds = true
        
        bTotalSalesYellow.layer.cornerRadius = bTotalSalesYellow.frame.size.width/2
        bTotalSalesYellow.clipsToBounds = true
        
        bTotalTaxGreen.layer.cornerRadius = bTotalTaxGreen.frame.size.width/2
        bTotalTaxGreen.clipsToBounds = true
        
        bTotalTipsRed.layer.cornerRadius = bTotalTipsRed.frame.size.width/2
        bTotalTipsRed.clipsToBounds = true
        
        bTotalOrdersYellow.layer.cornerRadius = bTotalOrdersYellow.frame.size.width/2
        bTotalOrdersYellow.clipsToBounds = true
        
        bTotalTimeRed.layer.cornerRadius = bTotalTimeRed.frame.size.width/2
        bTotalTimeRed.clipsToBounds = true
        
        bTableRequestYellow.layer.cornerRadius = bTableRequestYellow.frame.size.width/2
        bTableRequestYellow.clipsToBounds = true
        
        bTableRequestRed.layer.cornerRadius = bTableRequestRed.frame.size.width/2
        bTableRequestRed.clipsToBounds = true
        
        
    dmanuallyRed.layer.cornerRadius = dmanuallyRed.frame.size.width/2
        dmanuallyRed.clipsToBounds = true
        
        dqrcodeYellow.layer.cornerRadius = dqrcodeYellow.frame.size.width/2
        dqrcodeYellow.clipsToBounds = true
        
        duserYellow.layer.cornerRadius = duserYellow.frame.size.width/2
        duserYellow.clipsToBounds = true
        
        dtimeRed.layer.cornerRadius = dtimeRed.frame.size.width/2
        dtimeRed.clipsToBounds = true
    

        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (employeeNameTF.text?.contains("doorPerson"))! {
            print("doorPerson")
            self.doorpersonView.isHidden = false
            self.bartenderView.isHidden = true
        }
        if (employeeNameTF.text?.contains("bartender"))! {
            print("bartender")
            self.doorpersonView.isHidden = true
            self.bartenderView.isHidden = false
        }
    }

    @IBAction func EmployeeAccountsBtn(_ sender: Any) {
    }
    
    @IBAction func fromTFBtn(_ sender: Any) {
        if fromTF.text != "" && toTf.text != ""
        {
            if (employeeNameTF.text?.contains("doorPerson"))! {
                print("doorPerson")
       self.getDoorpersonUserData()
            }
            if (employeeNameTF.text?.contains("bartender"))! {
                print("bartender")
      self.getBartenderUserData()
            }
            
            
            
    
     
        }
    }
    @IBAction func toTFBtn(_ sender: Any) {
        if fromTF.text != "" && toTf.text != ""
        {
            if (employeeNameTF.text?.contains("doorPerson"))! {
                print("doorPerson")
                self.getDoorpersonUserData()
            }
            if (employeeNameTF.text?.contains("bartender"))! {
                print("bartender")
                self.getBartenderUserData()
            }

        }
//   if fromTF.text != "" && toTf.text != ""
//   {
//    let banner = NotificationBanner(title: "ERROR", subtitle: "Please select from date first", style: .danger)
//    banner.duration = 1
//    banner.show()
//    return
//    }
//    else
//   {
//    if toTf.text != ""
//    {
//    self.getUserData()
//        }
//        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    @objc func donedatePicker(){
        if fromTF.isFirstResponder
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            //"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
            
            fromTF.text = formatter.string(from: datePicker.date)
            self.isoDateString = formatter.string(from: datePicker.date).dateStringToISOString
            print(isoDateString)
            self.view.endEditing(true)
        }
        if toTf.isFirstResponder
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            toTf.text = formatter.string(from: datePicker.date)
            self.isotimeToString = formatter.string(from: datePicker.date).dateStringToISOString
            print(isotimeToString)
            self.view.endEditing(true)
        }
        print("TextField did end editing method called")
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == fromTF {
            datePicker.datePickerMode = .date
            self.selectEmployeeDropDown.inputView = self.pickerView
        }
        if textField == toTf {
            datePicker.datePickerMode = .date
        }
        if textField == selectEmployeeDropDown
        {
            self.selectEmployeeDropDown.inputView = self.pickerView
            
        }
    }
        
    @IBAction func backBtn(_ sender: Any) {
   navigationController?.popViewController(animated: true)
    }

    

}
extension StatsVC : UIPickerViewDelegate, UIPickerViewDataSource {
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return self.employeeNameAry.count
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.employeeNameAry[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.employeeNameTF.text = self.employeeNameAry[row]
        self.employeeNameTF.resignFirstResponder()
        if (employeeNameTF.text?.contains("doorPerson"))! {
            print("doorPerson")
            self.doorpersonView.isHidden = false
            self.bartenderView.isHidden = true
        }
        if (employeeNameTF.text?.contains("bartender"))! {
            print("bartender")
            self.doorpersonView.isHidden = true
            self.bartenderView.isHidden = false
        }

        
    }
    


func getUEmployeeAccounts() {
    let userId = Constants.kUserDefaults.value(forKey: appConstants.userId) as? String
    UserAPI().getBartenderDoorperson(userId: userId!,pageNo: 0) { (data, error) in
        if data[APIConstants.isSuccess.rawValue] as! Bool == true {
             self.EmployeeAccounts = data["items"] as! [[String : AnyObject]]
             self.modelArry = User.modelsFromDictionaryArray(array: self.EmployeeAccounts as NSArray)
                SVProgressHUD.show(withStatus: "Please Wait")
               self.bartenderAry.removeAll()
                self.doorpersonAry.removeAll()
       
                for i in self.modelArry
                {
                    let role = i!.role
                    // print("role of the user is\(role)")
                    if role == "doorPerson"
                    {
                        let name = i!.firstName!
                        let role = i!.role!
                        let combineName = "\(name)(\(role))"
                        
                        self.doorpersonAry.append(combineName)
                    }
                    if role == "bartender"
                    {
                        let name = i!.firstName!
                        let role = i!.role!
                        let combineName = "\(name)(\(role))"
                        
                        
                        self.bartenderAry.append(combineName)
                        print(self.bartenderAry)
                    }
                }
                self.employeeNameAry.removeAll()
                self.employeeNameAry = self.bartenderAry + self.doorpersonAry
                print(self.employeeNameAry)
                self.pickerView.reloadAllComponents()
                SVProgressHUD.dismiss()
                if self.employeeNameTF.text == "" && self.employeeNameAry.isEmpty == false
                {
                    self.employeeNameTF.text = self.employeeNameAry[0]
                }
            
            
        }
        else{
            print("Getting Error")
        }
    }
}
    func entranceFrequencysetLineChart(dataPoints: [String], values: [Double])
    {
        var lineChartEntry = [ChartDataEntry]()
        for i in 0..<dataPoints.count
        {
            print(i)
            let dataEntry = ChartDataEntry(x:Double(i), y:values[i])
            print(i)
            lineChartEntry.append(dataEntry)
            
        }
        print(self.bOrdrFrequencyChartValues)
        let line1 = LineChartDataSet(values: lineChartEntry, label: " ")
        line1.colors = [NSUIColor.red]
        line1.circleColors = [NSUIColor.red]
        line1.circleHoleColor = NSUIColor.red
        line1.valueColors = [NSUIColor.white]
        self.dEntranceFrequencyChart.backgroundColor = .clear
        self.dEntranceFrequencyChart.gridBackgroundColor = .clear
        self.dEntranceFrequencyChart.xAxis.drawGridLinesEnabled = false
        
        self.dEntranceFrequencyChart.rightAxis.enabled = false
        self.dEntranceFrequencyChart.leftAxis.labelTextColor = UIColor.init(displayP3Red: 255.0, green: 172.0, blue: 0.0, alpha: 1.0)
        self.dEntranceFrequencyChart.xAxis.enabled = false
        //  self.mLineCHartView.rightAxis.drawLabelsEnabled = false
        self.dEntranceFrequencyChart.legend.enabled = false
        let data = LineChartData()
        data.addDataSet(line1)
        dEntranceFrequencyChart.data = data
        
        
    }
    
    func orderFrequencysetLineChart(dataPoints: [String], values: [Double])
    {
        var lineChartEntry = [ChartDataEntry]()
        for i in 0..<dataPoints.count
        {
            print(i)
            let dataEntry = ChartDataEntry(x:Double(i), y:values[i])
            print(i)
            lineChartEntry.append(dataEntry)
            
        }
        print(self.dEntranceFrequencyChartValues)
        let line1 = LineChartDataSet(values: lineChartEntry, label: " ")
        line1.colors = [NSUIColor.red]
        line1.circleColors = [NSUIColor.red]
        line1.circleHoleColor = NSUIColor.red
        line1.valueColors = [NSUIColor.white]
        self.bOrderFrequencyChart.backgroundColor = .clear
        self.bOrderFrequencyChart.gridBackgroundColor = .clear
        self.bOrderFrequencyChart.xAxis.drawGridLinesEnabled = false
        
        self.bOrderFrequencyChart.rightAxis.enabled = false
        self.bOrderFrequencyChart.leftAxis.labelTextColor = UIColor.init(displayP3Red: 255.0, green: 172.0, blue: 0.0, alpha: 1.0)
        self.bOrderFrequencyChart.xAxis.enabled = false
        //  self.mLineCHartView.rightAxis.drawLabelsEnabled = false
        self.bOrderFrequencyChart.legend.enabled = false
        let data = LineChartData()
        data.addDataSet(line1)
        bOrderFrequencyChart.data = data
        
        
    }

    func tableRequestsetLineChart(dataPoints: [String], values: [Double])
    {
        var lineChartEntry = [ChartDataEntry]()
        print(self.bTableRequestChartValues)
        for i in 0..<dataPoints.count
        {
            print(i)
            let dataEntry = ChartDataEntry(x:Double(i), y:values[i])
                        print(i)
                        lineChartEntry.append(dataEntry)
            
            
        }
         print(self.pieChartPointsArry)
        let line1 = LineChartDataSet(values: lineChartEntry, label: " ")
        line1.colors = [NSUIColor.red]
        line1.circleColors = [NSUIColor.red]
        line1.circleHoleColor = NSUIColor.red
        line1.valueColors = [NSUIColor.white]
        self.bTableRequestChart.backgroundColor = .clear
        self.bTableRequestChart.gridBackgroundColor = .clear
        self.bTableRequestChart.xAxis.drawGridLinesEnabled = false
        
        self.bTableRequestChart.rightAxis.enabled = false
        self.bTableRequestChart.leftAxis.labelTextColor = UIColor.init(displayP3Red: 255.0, green: 172.0, blue: 0.0, alpha: 1.0)
        self.bTableRequestChart.xAxis.enabled = false
        //  self.mLineCHartView.rightAxis.drawLabelsEnabled = false
        self.bTableRequestChart.legend.enabled = false
        let data = LineChartData()
        data.addDataSet(line1)
        bTableRequestChart.data = data
        
        
    }

//    func tableRequestsetPieChart(dataPoints: [String], values: [Double]) {
//        bTableRequestChart.noDataText = "You need to provide data for the chart."
//        bTableRequestChart.legend.form = .none
//        bTableRequestChart.xAxis.labelPosition = .bottom
//        bTableRequestChart.leftAxis.drawLabelsEnabled = true
//        bTableRequestChart.leftAxis.drawGridLinesEnabled = true
//        bTableRequestChart.leftAxis.labelCount = 10
//        bTableRequestChart.rightAxis.drawGridLinesEnabled = false
//        bTableRequestChart.rightAxis.drawAxisLineEnabled = true
//        bTableRequestChart.xAxis.drawAxisLineEnabled = false
//        bTableRequestChart.xAxis.drawGridLinesEnabled = false
//
//        bTableRequestChart.xAxis.labelCount = 7
//        bTableRequestChart.rightAxis.drawLabelsEnabled = true
//        bTableRequestChart.chartDescription?.text = ""
//bTableRequestChart.leftAxis.labelTextColor = UIColor.yellow
//        bTableRequestChart.rightAxis.labelTextColor = UIColor.yellow
//
//         var dataEntries: [ChartDataEntry] = []
//
//        for i in 0..<dataPoints.count {
//            let dataEntry = BarChartDataEntry(x:Double(i), y:values[i])
//            print(Double(i))
//            print(values[i])
//            print(dataEntry)
//            dataEntries.append(dataEntry)
//        }
//
//        let chartDataSet = BarChartDataSet(values: dataEntries, label: "")
//        let chartData = BarChartData()
//        let colors: [UIColor] = [UIColor.init(red:0.71, green:0.24, blue:0.15, alpha:1.0)]
//                chartDataSet.colors = colors
//        chartDataSet.drawValuesEnabled = true
//      //  let minn = Double(values.min()!) - 0.1
//      //  bTableRequestChart.leftAxis.axisMinimum = Double(values.min()! - 0.0)
//     //    bTableRequestChart.leftAxis.axisMaximum = Double(values.max()! + 1)
//        chartData.addDataSet(chartDataSet)
//        bTableRequestChart.data = chartData
//        let xAxisValue = bTableRequestChart.xAxis
//     //   xAxisValue.valueFormatter = barChartAxisFormatDelegate
//
//    }
    
//    func tableRequestsetPieChart(dataPoints: [String], values: [Double]) {
//
//
//        //        self.mPieChartView.maxAngle = 180
//        //        self.mPieChartView.rotationAngle = 180
//        self.bTableRequestChart.legend.enabled = false
//        self.bTableRequestChart.chartDescription?.enabled = false
////        self.bTableRequestChart.drawHoleEnabled = false
////        self.bTableRequestChart.rotationEnabled = false
////        self.bTableRequestChart.rotationAngle = 0
//        self.bTableRequestChart.isUserInteractionEnabled = false
//        self.bTableRequestChart.setExtraOffsets(left: -200, top: 0, right: -200, bottom: 0)
//        self.bTableRequestChart.setNeedsDisplay()
//        self.bTableRequestChart.contentMode = .scaleAspectFill
//        self.bTableRequestChart.notifyDataSetChanged()
//   //     self.bTableRequestChart.usePercentValuesEnabled = true
////        self.bTableRequestChart.xAxis.granularityEnabled = true
////        self.bTableRequestChart.xAxis.granularity = 1.0 //default granularity is 1.0, but it is better to be explicit
////        self.bTableRequestChart.xAxis.decimals = 0
//
//        var dataEntries: [ChartDataEntry] = []
//
//        for i in 0...dataPoints.count - 1  {
//            let dataEntry = ChartDataEntry(x: Double(i), y: values[i], data: dataPoints[i] as AnyObject)
//            dataEntries.append(dataEntry)
//        }
//        print(dataEntries)
//        let chartDataSet:BarChartDataSet!
//        chartDataSet = BarChartDataSet(values: dataEntries, label: "BarChart Data")
//        let chartData = BarChartData(dataSet: chartDataSet)
//        self.bTableRequestChart.data = chartData
//        self.bTableRequestChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: dataPoints)
//        self.bTableRequestChart.xAxis.granularityEnabled = true
//        self.bTableRequestChart.xAxis.drawGridLinesEnabled = false
//        self.bTableRequestChart.xAxis.labelPosition = .bottom
//        self.bTableRequestChart.xAxis.labelCount = 30
//        self.bTableRequestChart.xAxis.granularity = 2
//        self.bTableRequestChart.leftAxis.enabled = true
//        //barChartView.leftAxis.labelPosition = .outsideChart
//        //barChartView.leftAxis.decimals = 0
//        let minn = Double(values.min()!) - 0.1
//        self.bTableRequestChart.leftAxis.axisMinimum = Double(values.min()! - 0.1)
//        //barChartView.leftAxis.granularityEnabled = true
//        //barChartView.leftAxis.granularity = 1.0
//        //barChartView.leftAxis.labelCount = 5
//        self.bTableRequestChart.leftAxis.axisMaximum = Double(values.max()! + 0.05)
//        self.bTableRequestChart.data?.setDrawValues(false)
//        self.bTableRequestChart.pinchZoomEnabled = true
//        self.bTableRequestChart.scaleYEnabled = true
//        self.bTableRequestChart.scaleXEnabled = true
//        self.bTableRequestChart.highlighter = nil
//        self.bTableRequestChart.doubleTapToZoomEnabled = true
//        self.bTableRequestChart.chartDescription?.text = ""
//        self.bTableRequestChart.rightAxis.enabled = false
//
//
//      // barChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInOutQuart)
//
////        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "")
////        pieChartDataSet.selectionShift = 0.0
////        pieChartDataSet.drawValuesEnabled = true
////        pieChartDataSet.xValuePosition = .insideSlice
////        pieChartDataSet.yValuePosition = .insideSlice
////
////        let formatter = NumberFormatter()
////        formatter.numberStyle = .percent
////        formatter.maximumFractionDigits = 1
////        formatter.multiplier = 1.0
////
////
////        let pieChartData = PieChartData(dataSet: pieChartDataSet)
////        self.bTableRequestChart.data = pieChartData
////        pieChartData.setValueFormatter(DefaultValueFormatter(formatter:formatter))
//
//        let colors: [UIColor] = [UIColor.init(red:0.99, green:0.67, blue:0.19, alpha:1.0), UIColor.init(red:0.71, green:0.24, blue:0.15, alpha:1.0)]
//        chartDataSet.colors = colors
//    }
    
    func salesBreakdownsetPieChart(dataPoints: [String], values: [Double]) {
        
        //        self.mPieChartView.maxAngle = 180
        //        self.mPieChartView.rotationAngle = 180
        self.bSalesBreakdownChart.legend.enabled = false
        self.bSalesBreakdownChart.chartDescription?.enabled = false
        self.bSalesBreakdownChart.drawHoleEnabled = false
        self.bSalesBreakdownChart.rotationEnabled = false
        self.bSalesBreakdownChart.rotationAngle = 0
        self.bSalesBreakdownChart.isUserInteractionEnabled = false
        self.bSalesBreakdownChart.setExtraOffsets(left: -200, top: 0, right: -200, bottom: 0)
        self.bSalesBreakdownChart.setNeedsDisplay()
        self.bSalesBreakdownChart.contentMode = .scaleAspectFill
        self.bSalesBreakdownChart.notifyDataSetChanged()
        self.bSalesBreakdownChart.usePercentValuesEnabled = true
        
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0...dataPoints.count - 1  {
            let dataEntry = ChartDataEntry(x: Double(i), y: values[i], data: dataPoints[i] as AnyObject)
            dataEntries.append(dataEntry)
        }
        
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "")
        pieChartDataSet.selectionShift = 0.0
        pieChartDataSet.drawValuesEnabled = true
        pieChartDataSet.xValuePosition = .insideSlice
        pieChartDataSet.yValuePosition = .insideSlice
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 1
        formatter.multiplier = 1.0
        
        
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        self.bSalesBreakdownChart.data = pieChartData
        pieChartData.setValueFormatter(DefaultValueFormatter(formatter:formatter))
        
        let colors: [UIColor] = [UIColor.init(red:0.99, green:0.67, blue:0.19, alpha:1.0), UIColor.init(red:0, green:249, blue:0, alpha:0.8), UIColor.init(red:0.71, green:0.24, blue:0.15, alpha:1.0)]
        pieChartDataSet.colors = colors
    }
    
    func entrancesetPieChart(dataPoints: [String], values: [Double]) {
        
        //        self.mPieChartView.maxAngle = 180
        //        self.mPieChartView.rotationAngle = 180
        self.bSalesBreakdownChart.legend.enabled = false
        self.bSalesBreakdownChart.chartDescription?.enabled = false
        self.bSalesBreakdownChart.drawHoleEnabled = false
        self.bSalesBreakdownChart.rotationEnabled = false
        self.bSalesBreakdownChart.rotationAngle = 0
        self.bSalesBreakdownChart.isUserInteractionEnabled = false
        self.bSalesBreakdownChart.setExtraOffsets(left: -200, top: 0, right: -200, bottom: 0)
        self.bSalesBreakdownChart.setNeedsDisplay()
        self.bSalesBreakdownChart.contentMode = .scaleAspectFill
        self.bSalesBreakdownChart.notifyDataSetChanged()
        self.bSalesBreakdownChart.usePercentValuesEnabled = true
        
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0...dataPoints.count - 1  {
            let dataEntry = ChartDataEntry(x: Double(i), y: values[i], data: dataPoints[i] as AnyObject)
            dataEntries.append(dataEntry)
        }
        
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "")
        pieChartDataSet.selectionShift = 0.0
        pieChartDataSet.drawValuesEnabled = true
        pieChartDataSet.xValuePosition = .insideSlice
        pieChartDataSet.yValuePosition = .insideSlice
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 1
        formatter.multiplier = 1.0
        
        
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        self.bSalesBreakdownChart.data = pieChartData
        pieChartData.setValueFormatter(DefaultValueFormatter(formatter:formatter))
        
        let colors: [UIColor] = [UIColor.init(red:0.99, green:0.67, blue:0.19, alpha:1.0), UIColor.init(red:0, green:249, blue:0, alpha:0.8), UIColor.init(red:0.71, green:0.24, blue:0.15, alpha:1.0)]
        pieChartDataSet.colors = colors
    }
    
    func setPieChart(dataPoints: [String], values: [Double]) {
        
        //        self.mPieChartView.maxAngle = 180
        //        self.mPieChartView.rotationAngle = 180
        self.bFromChart.legend.enabled = false
        // self.mpieChartView.animate(xAxisDuration: 1.0)
        self.bFromChart.chartDescription?.enabled = false
        self.bFromChart.drawHoleEnabled = false
        self.bFromChart.rotationEnabled = false
        self.bFromChart.rotationAngle = 0
        self.bFromChart.isUserInteractionEnabled = false
        self.bFromChart.setExtraOffsets(left: -200, top: 0, right: -200, bottom: 0)
        self.bFromChart.setNeedsDisplay()
        self.bFromChart.contentMode = .scaleAspectFill
        self.bFromChart.notifyDataSetChanged()
        
        self.bFromChart.usePercentValuesEnabled = true
        
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0...dataPoints.count - 1  {
            let dataEntry = ChartDataEntry(x: Double(i), y: values[i], data: dataPoints[i] as AnyObject)
            dataEntries.append(dataEntry)
        }
        
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "")
        pieChartDataSet.selectionShift = 0.0
        pieChartDataSet.drawValuesEnabled = true
        pieChartDataSet.xValuePosition = .insideSlice
        pieChartDataSet.yValuePosition = .insideSlice
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 1
        formatter.multiplier = 1.0
        
        
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        self.bFromChart.data = pieChartData
        pieChartData.setValueFormatter(DefaultValueFormatter(formatter:formatter))
        
        let colors: [UIColor] = [UIColor.init(red:0.99, green:0.67, blue:0.19, alpha:1.0), UIColor.init(red:0.71, green:0.24, blue:0.15, alpha:1.0)]
        pieChartDataSet.colors = colors
    }
    
    func getBartenderUserData() {
        SVProgressHUD.setStatus("Loading.......")
        SVProgressHUD.setDefaultMaskType(.clear)
        let tokenString = Constants.kUserDefaults.value(forKey: "token")
        
        let headers: HTTPHeaders = [
            "x-access-token": tokenString as! String,
            ]
        var currentEmployeeId = String()
        for i in self.modelArry
        {
             let fName = i!.firstName!
            let role = i!.role!
            print(fName)
            print(role)
            let combineName = fName + "(" + role + ")"
            print(combineName)
            print(employeeNameTF.text!)
            let startDate = fromTF.text
            let endDate = toTf.text
            self.newStrtData = startDate!.dateStringToUTCString
            print(self.newStrtData)
            self.newEndData = endDate!.dateStringToUTCString
            print(self.newEndData)
           
            if self.employeeNameTF.text == combineName
            {
                 currentEmployeeId = i!.id!
                print("curr emp id is  \(currentEmployeeId)")
            }
        }
        let urlString = remoteConfig.mBaseUrl + "orders/search/user?firstName=" + currentEmployeeId + "&startDate=" + self.newStrtData + "&endDate=" + self.newStrtData
        print(urlString)
        Alamofire.request(urlString, method: .get, parameters: nil, headers: headers).responseJSON {
            response in
            switch response.result{
            case .success(_):
                if let JSON = response.result.value as? NSDictionary{
                    print("json get by id-- \(JSON)")

                    let status:Int = (JSON["isSuccess"] as? Int)!
                    //  BarDictionaryInfo = JSON["data"]
                    
                    if status == 1{
                        //    print(items)
                        let jsonData = JSON["data"] as! [String: AnyObject]
                        print(jsonData)
                     
                        let sales = jsonData["sales"] as! Int
                        let orders = jsonData["orders"] as! Int
                        let tableRequest = jsonData["tableRequest"] as! Int
                        let nonServesAverage = jsonData["nonServesAverage"] as! Double
                        let salesPercentage = jsonData["salesPercentage"] as! Double
                        let taxPercentage = jsonData["taxPercentage"] as! Double
                        let tipsPercentage = jsonData["tipsPercentage"] as! Double
                        print(nonServesAverage)
                        let ServedAverage = jsonData["ServedAverage"] as! Double
                        let tableFrequency = jsonData["tableFrequency"] as! NSArray
                        print(tableFrequency)
                        let ordersFrequency = jsonData["ordersFrequency"] as! NSArray
                        self.bOrdrFrequencyChartValues.removeAll()
                        for orderData in ordersFrequency
                        {
                            let timeSubDir = orderData as! NSDictionary
                            print(orderData)
                            let time = timeSubDir["time"] as! String
                            print(time)
                            let eventTime = time.utcStringToSmartTime1
                            print(eventTime)
                            let tableCount = timeSubDir["orders"] as! Double
                            print(tableCount)
                            
                            if eventTime == "11:59 PM"
                            {
                                 self.bOrdrFrequencyChartValues.append(tableCount)
                            }
                            if eventTime == "12:00 AM"
                            {
                                 self.bOrdrFrequencyChartValues.append(tableCount)
                            }
                        }
                        
                        self.bTableRequestChartValues.removeAll()
                        for timeData in tableFrequency
                        {
                            let timeSubDir = timeData as! NSDictionary
                            print(timeData)
                            let time = timeSubDir["time"] as! String
                            print(time)
                            let eventTime = time.utcStringToSmartTime1
                            print(eventTime)
                            let tableCount = timeSubDir["tableCount"] as! Double
                            print(tableCount)
                            
                            if eventTime == "11:59 PM"
                            {
                                self.bTableRequestChartValues.append(tableCount)
                            }
                            if eventTime == "12:00 AM"
                            {
                                self.bTableRequestChartValues.append(tableCount)
                            }
                            
                            

                        }
                        self.bOrders.text = orders.description
                        self.bTableRequest.text = tableRequest.description
                        self.bSales.text = sales.description
                        self.bFromchartValues = [ServedAverage]
                        self.bFromchartValues.append(nonServesAverage)
                        
                        self.salesBreakdownValues = [salesPercentage]
                        self.salesBreakdownValues.append(taxPercentage)
                        self.salesBreakdownValues.append(tipsPercentage)
                        
                        print(self.salesBreakdownValues)
                        if ServedAverage != 0.0 || nonServesAverage != 0.0
                        {
                           self.setPieChart(dataPoints: self.pieChartPointsArry, values: self.bFromchartValues)
                        }
                        if salesPercentage != 0.0
                        {
                            self.salesBreakdownsetPieChart(dataPoints: self.bSalesBreakdownChartPoint, values: self.salesBreakdownValues)
                        }
                        print(self.tableRequestChartPoint)
                        print(self.bTableRequestChartValues)
                        self.tableRequestsetLineChart(dataPoints: self.tableRequestChartPoint, values: self.bTableRequestChartValues)
                        self.orderFrequencysetLineChart(dataPoints: self.bOrdrFrequencyChartPoint, values: self.bOrdrFrequencyChartValues)


                        SVProgressHUD.dismiss()
                    }else{
                        SVProgressHUD.dismiss()
                    }
                }
            case .failure(_):
                print("Faield Trinash")
                let alert = UIAlertController(title: "Oops!", message: "Server Error....", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }}
        
    }
 
    func getDoorpersonUserData() {
        SVProgressHUD.setStatus("Loading.......")
        SVProgressHUD.setDefaultMaskType(.clear)
        let tokenString = Constants.kUserDefaults.value(forKey: "token")
        
        let headers: HTTPHeaders = [
            "x-access-token": tokenString as! String,
            ]
        var currentEmployeeId = String()
        for i in self.modelArry
        {
            let fName = i!.firstName!
            let role = i!.role!
            print(fName)
            print(role)
            let combineName = fName + "(" + role + ")"
            print(combineName)
            print(employeeNameTF.text!)
            let startDate = fromTF.text
            let endDate = toTf.text
            self.newStrtData = startDate!.dateStringToUTCString
            print(self.newStrtData)
            self.newEndData = endDate!.dateStringToUTCString
            print(self.newEndData)
            
            if self.employeeNameTF.text == combineName
            {
                currentEmployeeId = i!.id!
                print("curr emp id is  \(currentEmployeeId)")
            }
        }

      let urlString = remoteConfig.mBaseUrl + "covers/search/user?firstName=" + currentEmployeeId + "&startDate=" + self.newStrtData + "&endDate=" + self.newStrtData
        print(urlString)
        Alamofire.request(urlString, method: .get, parameters: nil, headers: headers).responseJSON {
            response in
            switch response.result{
            case .success(_):
                if let JSON = response.result.value as? NSDictionary{
                    print("json get by id-- \(JSON)")
                    
                    let status:Int = (JSON["isSuccess"] as? Int)!
                    
                    if status == 1{
                        let jsonData = JSON["data"] as! [String: AnyObject]
                        print(jsonData)
                        
                        let coverfee = jsonData["coverfee"] as! Int
                        let entrance = jsonData["entrance"] as! Int
                        let qrcountave = jsonData["qrcountave"] as! Double
                        let verifycountave = jsonData["verifycountave"] as! Double
                      
                        let frequency = jsonData["Frequency"] as! NSArray
                        print(frequency)
     
                        self.dEntranceFrequencyChartValues.removeAll()
                        for orderData in frequency
                        {
                            let timeSubDir = orderData as! NSDictionary
                            print(timeSubDir)
                            let time = timeSubDir["time"] as! String
                            print(time)
                            let eventTime = time.utcStringToSmartTime1
                            print(eventTime)
                            let entrannce = timeSubDir["entrannce"] as! Double
                            print(entrannce)
                            
                            if eventTime == "11:59 PM"
                            {
                                self.dEntranceFrequencyChartValues.append(entrannce)
                            }
                            if eventTime == "12:00 AM"
                            {
                                self.dEntranceFrequencyChartValues.append(entrannce)
                            }
                        }
                        
                        self.totalEntranceLbl.text = entrance.description
                        self.totalCoverFeeLbl.text = coverfee.description
                        
                     
                        self.dEntranceChartValue = [qrcountave]
                        self.dEntranceChartValue.append(verifycountave)
              
                        if qrcountave != 0.0 || verifycountave != 0.0
                        {
                            self.entrancesetPieChart(dataPoints: self.pieChartPointsArry, values: self.dEntranceChartValue)
                        }
                      
                   
                        self.entranceFrequencysetLineChart(dataPoints: self.DentranceFrequencyChartPoint, values: self.dEntranceFrequencyChartValues)
                        
                        SVProgressHUD.dismiss()
                    }else{
                        SVProgressHUD.dismiss()
                    }
                }
            case .failure(_):
                print("Faield Trinash")
                let alert = UIAlertController(title: "Oops!", message: "Server Error....", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }}
        
    }

    
}
