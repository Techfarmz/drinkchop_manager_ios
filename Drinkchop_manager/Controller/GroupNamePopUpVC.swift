//
//  GroupNamePopUpVC.swift
//  DrinkchopBartender
//
//  Created by Gurpreet Gulati on 21/05/19.
//  Copyright © 2019 Temp. All rights reserved.
//

import UIKit
import Toast_Swift

protocol GroupNamePopUpVCDelegate: class {
    
    func setGroupName(grpName:String, isDataValid : Bool)
    
}


class GroupNamePopUpVC: UIViewController {

    @IBOutlet weak var mGroupNameTxtFld: UITextField!
     weak var delegate : GroupNamePopUpVCDelegate?
    var nameOfTheGroup : String?
    
    var chatsAPI : ChatsAPI!
    var chatModelArray : [Chat]!
    var idsOfSelectedUsers = [String]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
      self.chatsAPI = ChatsAPI.sharedInstance
        self.chatModelArray = [Chat]()
   
    }
    
    
    @IBAction func mCancelBtnTapped(_ sender: UIButton) {
         self.dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func mOkayBtnTapped(_ sender: UIButton) {
        if self.mGroupNameTxtFld.text?.count == 0 || self.mGroupNameTxtFld.text?.isEmpty == true {
            self.view.makeToast("Please Enter A Valid Name")
            return
        }
        
        else {
            
            self.delegate?.setGroupName(grpName: self.mGroupNameTxtFld.text!, isDataValid: true)
             self.nameOfTheGroup = self.mGroupNameTxtFld.text!
            self.dismiss(animated: true, completion: nil)
          
        }
    }
    
 

}
