//
//  SearchNewUserVC.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 22/02/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit

class SearchNewUserVC: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchTableView: UITableView!
    lazy var searchBar = UISearchBar(frame: CGRect.zero)
    var filterStatus = String()
    var userSearchArray : [User]!
    var usersModelArray : [User]!
    var userAPI : UserAPI!
    
    
  //  lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 200, height: 20))
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        //.auth().signInAnonymously(completion: nil)
        navigationController?.navigationBar.isHidden = true
        //    getAllUser()
        searchTableView.delegate = self
        searchTableView.dataSource = self
        //   searchListArr = [SearchChatUser]()
        self.searchTF.attributedPlaceholder = NSAttributedString(string: "Search", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        self.userAPI = UserAPI.sharedInstance
        self.usersModelArray = [User]()
        self.getAllUsers()
        searchTF.delegate = self
        
        
//        navigationItem.titleView = searchBar
//        searchTableView.delegate = self
//        searchTableView.dataSource = self
//        var textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
//
//        textFieldInsideSearchBar?.textColor = UIColor.white
//        textFieldInsideSearchBar?.backgroundColor = UIColor.clear
//        searchBar.barTintColor = UIColor.clear
        
        searchBar.placeholder = "Search here..."
    }

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 86
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filterStatus.isEmpty {
            return self.usersModelArray.count
        }
        else
        {
            return self.userSearchArray.count
            
        }
    }
    

//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell")as! SearchCell
//
//        // Mark Clear user Selection
//        cell.selectionStyle = .none
//        return cell
//    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath as IndexPath) as! SearchCell
        
        //    cell.userImage.image = self.UserImgAry[indexPath.row]
        //        cell.usrName.text = firstNameAry[indexPath.row]
        //        print(searchListArr[indexPath.row].imgUrl)
        //        cell.userImage.sd_setImage(with: URL(string: searchListArr[indexPath.row].imgUrl!), placeholderImage: UIImage(named: "MAN.png"))
        if filterStatus.isEmpty {
            let userr = self.usersModelArray[indexPath.row]
            
            if let userName = userr.firstName, let roleOfUser = userr.role  {
                cell.UserName.text = userName + " (\(roleOfUser))"
            }
            if userr.imgUrl != nil {
                cell.userImg.sd_setImage(with: URL.init(string:((userr.imgUrl!.httpsExtend))), placeholderImage: UIImage(named: "MAN.png"))
            }
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            print(userSearchArray.count)
            let userr = self.userSearchArray[indexPath.row]
            
            if let userName = userr.firstName, let roleOfUser = userr.role  {
                cell.UserName.text = userName + " (\(roleOfUser))"
            }
            if userr.imgUrl != nil {
                cell.userImg.sd_setImage(with: URL.init(string:((userr.imgUrl!.httpsExtend))), placeholderImage: UIImage(named: "MAN.png"))
            }
            cell.selectionStyle = .none
            return cell
        }
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "MessagesChatVCID") as! MessagesChatVC
        if filterStatus.isEmpty {
            destinationvc.isComingFromSearchUserVC = true
            let userObj = self.usersModelArray[indexPath.row]
            guard let userIDOfOtherUser = userObj.id
                else {
                    self.view.makeToast("ID Missing")
                    return }
            destinationvc.otherUserID = userIDOfOtherUser
            destinationvc.typeOfChat = "normal"
            destinationvc.otherUserName = userObj.firstName
            
            
        }
        else
        {
            destinationvc.isComingFromSearchUserVC = true
            let userObj = self.userSearchArray [indexPath.row]
            guard let userIDOfOtherUser = userObj.id
                else {
                    self.view.makeToast("ID Missing")
                    return }
            destinationvc.otherUserID = userIDOfOtherUser
            destinationvc.typeOfChat = "normal"
            destinationvc.otherUserName = userObj.firstName
            
        }
        self.navigationController?.pushViewController(destinationvc, animated: true)
    }
    @IBAction func backBtn(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    //MARK: Get All Users
    func getAllUsers(name:String = "") {
        self.userAPI.getAllUsers1(type: name) { (data, error) in
            if data[APIConstants.isSuccess.rawValue] as! Bool == true {
                if error == nil{
                    let userList = data[APIConstants.items.rawValue] as! NSArray
                    self.usersModelArray = User.modelsFromDictionaryArray(array: userList)
                    print("printing user model array : \(self.usersModelArray)")
                    
                    self.searchTableView.reloadData()
                    
                }}
            else{
                print("Getting Error")
                
            }
            
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = textField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        var updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        updatedText = updatedText.replacingOccurrences(of: "[\\[\\]^+<>|]-=$#@", with: "%20", options: .regularExpression, range: nil)
        
        print("updatedText-- \(updatedText)")
        
        if updatedText.count == 0 {
            print("updatedText is empty")
            //   usersModelArray = userSearchArray
            self.filterStatus = ""
            self.getAllUsers()
            
        }else{
            filterStatus = updatedText
            userSearchArray = self.usersModelArray.filter { ($0.firstName)?.range(of: updatedText, options: [.diacriticInsensitive, .caseInsensitive]) != nil }
            print(userSearchArray)
        }
        searchTableView.reloadData()
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    
}
class SearchCell: UITableViewCell {
    
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var UserName: UILabel!
    
    
}
