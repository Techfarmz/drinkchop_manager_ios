//
//  PaymentTransferVC.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 12/06/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire


class PaymentTransferVC: UIViewController {
    @IBOutlet weak var amount: UITextField!
    
    var transferAmount = String()
     private var userApi : UserAPI!
    var barId = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
           self.userApi = UserAPI.sharedInstance
        barId = Constants.kUserDefaults.string(forKey: "barId")!
      self.GetPaymentInfo()
        // Do any additional setup after loading the view.
    }
    

    @IBAction func transferBtn(_ sender: Any) {
        self.transferAmount = (amount.text?.filter("01234567890.".contains))!
        
        if amount.text != nil && transferAmount != "0"
        {
            self.transferAmountApi()
        }
    
    }
    @IBAction func backBtn(_ sender: Any) {
    navigationController?.popViewController(animated: true)
    }
    
    //   http://159.89.226.199:8080/api/userPaymants?barId=5d0b29ecd56ff974db7652cf
    
    func GetPaymentInfo() {
        SVProgressHUD.setStatus("Loading.......")
        SVProgressHUD.setDefaultMaskType(.clear)
        let tokenString = Constants.kUserDefaults.value(forKey: "token")
        
        let headers: HTTPHeaders = [
            "x-access-token": tokenString as! String,
            ]
        let urlString = remoteConfig.mBaseUrl + "userPaymants?barId=" + self.barId
        print(urlString)
        Alamofire.request(urlString, method: .get, parameters: nil, headers: headers).responseJSON {
            response in
            switch response.result{
            case .success(_):
                if let JSON = response.result.value as? NSDictionary{
                    print("json get by id-- \(JSON)")
                    
                    
                    
                    let status:Int = (JSON["isSuccess"] as? Int)!
                    //  BarDictionaryInfo = JSON["data"]
                    
                    if status == 1{
                      print(JSON)
                  var amountTransfer = JSON["amountTransfer"] as! NSNumber
                            self.amount.text = "$" + amountTransfer.description
                        SVProgressHUD.dismiss()
                    }else{
                        SVProgressHUD.dismiss()
                    }
                }
            case .failure(_):
                  SVProgressHUD.dismiss()

            }}
        
    }

    func transferAmountApi() {
        SVProgressHUD.setStatus("Loading.......")
        SVProgressHUD.setDefaultMaskType(.clear)
        let tokenString = Constants.kUserDefaults.value(forKey: "token")
        
        let headers: HTTPHeaders = [
            "x-access-token": tokenString as! String,
            ]
        let urlString = remoteConfig.mBaseUrl + "userPaymants/transfer/received?barId=" + self.barId
        print(urlString)
        Alamofire.request(urlString, method: .get, parameters: nil, headers: headers).responseJSON {
            response in
            switch response.result{
            case .success(_):
                if let JSON = response.result.value as? NSDictionary{
                    print("json get by id-- \(JSON)")
                    
                    
                    
                    let status:Int = (JSON["isSuccess"] as? Int)!
                    //  BarDictionaryInfo = JSON["data"]
                    
                    if status == 1{
                        print(JSON)
                        var amountTransfer = JSON["amountTransfer"] as! Int
                        self.amount.text = "$" + amountTransfer.description
                        SVProgressHUD.dismiss()
                    }else{
                        SVProgressHUD.dismiss()
                    }
                }
            case .failure(_):
                SVProgressHUD.dismiss()
                
            }}
        
    }
    
//    func transferAmountApi()
//    {
//        let token = Constants.kUserDefaults.string(forKey: appConstants.token)
//        let barid = Constants.kUserDefaults.string(forKey: appConstants.barId)
//        print(barid)
//        var url = "http://159.89.226.199:8080/api/bars/"
//        let urlString = remoteConfig.mBaseUrl + "userPaymants/transfer/received"
//        print(urlString)
//        let _headers : HTTPHeaders = ["x-access-token": token!]
//        let params : Parameters = ["coverFee": self.transferAmount]
//
//        let myurl =  NSURL(string:"url" as String)
//
//        request(urlString, method: .put, parameters: params, encoding: URLEncoding.httpBody , headers: _headers).responseJSON(completionHandler: {
//            response in response
//
//            let jsonResponse = response.result.value as! NSDictionary
//            print(jsonResponse)
//
//        })
//    }


}
