//
//  EditEmployeeAccountVC.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 28/02/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit
import SVProgressHUD
import NotificationBannerSwift
import iOSDropDown


class EditEmployeeAccountVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var roleDropDown: DropDown!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var phoneNumberTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var selectRoleTF: UITextField!
    
    private var userApi : UserAPI!
    var user: User?
    var validator = Validators()
     var dropdownValue = ["Bartender", "DoorPerson"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectRoleTF.delegate = self
        self.userApi = UserAPI.sharedInstance
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        roleDropDown.optionArray = dropdownValue
        roleDropDown.didSelect{(selectedText , index ,id) in
            self.selectRoleTF.text = selectedText
    
        }
        getUserDetails()
   
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }
    @IBAction func backBtn(_ sender: Any) {
    navigationController?.popViewController(animated: true)
    }
    @IBAction func selectRoleDropDownBtn(_ sender: Any) {
    }
    @IBAction func saveBtn(_ sender: Any) {
        guard validator.validators(TF1: self.firstNameTF,fieldName: "First Name") == false ||
            validator.validators(TF1: self.lastNameTF,fieldName: "Last Name") == false ||
            validator.validators(TF1: self.passwordTF,fieldName: "Password") == false || validator.validators(TF1: self.emailTF,fieldName: "Email") == false || validator.validators(TF1: self.selectRoleTF,fieldName: "Select Role") == false || validator.validators(TF1: self.phoneNumberTF,fieldName: "Phone Number") == false
            
            
            else
        
        {
            if (phoneNumberTF.text?.count)! > 9
            {
                    // Hit APi Here
                    self.user = User.init(dictionary: NSDictionary())
                    print(Constants.kUserDefaults.string(forKey: appConstants.userId))
              let usrid = Constants.kUserDefaults.string(forKey: "currentUserId")
                
                     self.user?.id  = usrid!
                    self.user?.firstName = firstNameTF.text
                    self.user?.lastName = lastNameTF.text
                    self.user?.email = emailTF.text
                    self.user?.password = passwordTF.text
                    self.user?.role = selectRoleTF.text
                    self.user?.phone = phoneNumberTF.text
                
                self.userUpdateProfile(userDict: user!)
            }
            else
            {
                let banner = NotificationBanner(title: "ERROR", subtitle: "Please Enter Valid Phone Number", style: .danger)
                banner.duration = 1
                banner.show()
            }
            return
        }
    }
    @IBAction func disableBtn(_ sender: Any) {
    }
    @IBAction func deleteBtn(_ sender: Any) {
    }
    // MARK: User Update Function
    func userUpdateProfile(userDict:User){
        SVProgressHUD.show(withStatus: "Please Wait")
        userApi.userUpdateProfileWithUserID(userDetials: userDict){ (isSuccess,response, error) -> Void in
            SVProgressHUD.dismiss()
            
            if (isSuccess){
                SVProgressHUD.dismiss()
                kAppDelegate.showNotification(text: "Profile Updated Successfully")
             let response = response
                print(response)
                let data = response!["data"] as! NSDictionary
//                                let newEventId = data["id"] as! String
//                                print(newEventId)
//                                Constants.kUserDefaults.set(newEventId, forKey: "newEventId")
                
                   self.navigationController?.popViewController(animated: true)
            }else{
                SVProgressHUD.dismiss()
                if error != nil{
                    //kAppDelegate.showNotification(text: error!)
                }else{
                    let response = response
                    
                    let message = response?["message"] as! String
                    print(message)
                    self.view.makeToast(message)
                }
            }
            
        }
        
        
    }

    // Get user Details
    func getUserDetails() {
        let userId = Constants.kUserDefaults.value(forKey: appConstants.userId) as? String
        UserAPI().getuserID(userId: userId!,pageNo: 0) { (data, error) in
            if data[APIConstants.isSuccess.rawValue] as! Bool == true {
                let userDict = data["data"]
                let dataProfile = NSKeyedArchiver.archivedData(withRootObject: userDict)
                //    Constants.kUserDefaults.set(dataProfile, forKey: appConstants.profile)
                
                let firstName = userDict!["firstName"] as! String
                let lastName = userDict!["lastName"] as! String
                let email = userDict?["email"] as? String
                let phone = userDict?["phone"] as? String
                let role = userDict?["role"] as? String
                
                if firstName != nil {
                    self.firstNameTF.text = firstName
                    self.lastNameTF.text = lastName
                    self.emailTF.text = email
                    self.phoneNumberTF.text = phone
                    self.selectRoleTF.text = role
                }
                let myUser = User.init(dictionary: userDict as! NSDictionary)
                
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }



}
