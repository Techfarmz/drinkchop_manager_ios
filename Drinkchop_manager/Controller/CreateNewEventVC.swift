//
//  CreateNewEventVC.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 21/02/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit
import SVProgressHUD
import CropViewController
import NotificationBannerSwift
import iOSDropDown
import Alamofire
import DateTimePicker
import ADDatePicker

class CreateNewEventVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {


    @IBOutlet weak var countryTF: DropDown!
    @IBOutlet weak var saveBtnOutlet: UIButton!
    @IBOutlet weak var countryTableview: UITableView!
    @IBOutlet weak var stateDropDown: DropDown!
    @IBOutlet weak var countryDropDown: DropDown!
    @IBOutlet weak var zipcodeTF: UITextField!
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var ststeTF: UITextField!
    @IBOutlet weak var addressTF: UITextField!
    @IBOutlet weak var instaLinkTF: UITextField!
    @IBOutlet weak var fbLinkTF: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var eventPicture: UIImageView!
    @IBOutlet weak var eventTimetoTF: UITextField!
    @IBOutlet weak var eventTimeFromTF: UITextField!
    @IBOutlet weak var eventDateTF: UITextField!
    @IBOutlet weak var enentTitleTF: UITextField!
   
     var drinksId = [String]()
    var isEditEvent = Bool()
    var countryModel = [countries]()
    let picker = DateTimePicker()
    var stateNameAry = [String]()
    var countrynameAry = [String]()
    var countryAry = [[String: AnyObject]]()
    var isotimeFromString : String?
    var newisotimeFromString : String?
    var isotimeToString : String?
    var newIsotimeToString : String?
    var isoDateString : String?
     var newIsoDateString : String?
    var myMediaType : String?
    let datePicker: UIDatePicker = UIDatePicker()
    private var userApi : UserAPI!
    var user: User?
    var isCountry = 1
    var validator = Validators()
    let dateFormat: DateFormatter = DateFormatter()
    var toolBar = UIToolbar()
    var compressedImage: UIImage!
    
    var idOfSelctedCountry : String?
    var selectedCountry : String?
    private var fileUploadAPI:FileUpload!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        enentTitleTF.delegate = self
        getCountryistAPI()
        if isEditEvent == true
        {
            GetEditEventApi()
            saveBtnOutlet.setTitle("UPDATE", for: .normal)
            self.navigationItem.title = "Edit Event"
        }
        countryTableview.delegate = self
        countryTableview.dataSource = self
        stateDropDown.delegate = self
        countryDropDown.delegate = self

        
        self.fileUploadAPI = FileUpload.sharedInstance
        self.userApi = UserAPI.sharedInstance
      //  eventDateTF.inputView = datePicker
       
        toolBar.sizeToFit()
 
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolBar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        eventDateTF.inputAccessoryView = toolBar
        eventDateTF.inputView = datePicker
        eventTimeFromTF.inputAccessoryView = toolBar
         eventTimeFromTF.inputView = datePicker
        eventTimetoTF.inputAccessoryView = toolBar
        eventTimetoTF.inputView = datePicker
        
//
        eventDateTF.delegate = self
        eventTimeFromTF.delegate = self
        eventTimetoTF.delegate = self
        fbLinkTF.delegate = self
     //   self.countryDropDown.arrowColor = .orange
        self.countryDropDown.textColor = UIColor.white
        
    }
    // Set maximum limit of textfield
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == enentTitleTF
        {
            let currentCharacterCount = enentTitleTF.text?.count ?? 0
            if range.length + range.location > currentCharacterCount {
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            return newLength <= 30
            
        }
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == countryDropDown
        {
            return false

        }
        if textField == stateDropDown
        {
            return false
            
        }
        return true
    }
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        return false
//    }
    @objc func donedatePicker(){
        if eventDateTF.isFirstResponder
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            //"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"

            eventDateTF.text = formatter.string(from: datePicker.date)
            self.isoDateString = formatter.string(from: datePicker.date).dateStringToISOString1
            print(isoDateString)
            
             self.newIsoDateString = formatter.string(from: datePicker.date).dateStringToISOString2
            print(newIsoDateString)
            
            self.view.endEditing(true)
        }
        if eventTimeFromTF.isFirstResponder
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "h:mm a"
            eventTimeFromTF.text = formatter.string(from: datePicker.date)
            self.isotimeFromString = formatter.string(from: datePicker.date).timeStringToISOString1
          if isoDateString != nil
          {
            self.newisotimeFromString = isoDateString! + isotimeFromString!
            print(newisotimeFromString)
            self.view.endEditing(true)
            }
            else
          {
            self.eventTimeFromTF.text = ""
            let banner = NotificationBanner(title: "ERROR", subtitle: "please select event date first", style: .success)
            banner.duration = 1
            banner.show()
            }
              self.view.endEditing(true)
        }
            if eventTimetoTF.isFirstResponder
            {
                let formatter = DateFormatter()
                formatter.dateFormat = "h:mm a"
                eventTimetoTF.text = formatter.string(from: datePicker.date)
            self.isotimeToString = formatter.string(from: datePicker.date).timeStringToISOString1
             
                if isoDateString != nil
                {
                    self.newIsotimeToString = isoDateString! + isotimeToString!
                    print(newIsotimeToString)
                    self.view.endEditing(true)
                }
                else
                {
                     self.eventTimetoTF.text = ""
                    let banner = NotificationBanner(title: "ERROR", subtitle: "please select event date first", style: .success)
                    banner.duration = 1
                    banner.show()
                }
                
                self.view.endEditing(true)
            }
        
        print("TextField did end editing method called")

        
        
//        eventTimeFromTF.text = formatter.string(from: datePicker.date)
//        eventTimetoTF.text = formatter.string(from: datePicker.date)
//        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == eventDateTF {
            datePicker.datePickerMode = .date
            
        }
        if textField == eventTimeFromTF {
            datePicker.datePickerMode = .time
        }
        if textField == eventTimetoTF {
            datePicker.datePickerMode = .time
        }
    }
    @IBAction func timeToBtn(_ sender: Any) {
      //  picker.isTimePickerOnly = true
    //    picker.show()
    }
    @IBAction func timeFromBtn(_ sender: Any) {
    
    }
    @IBAction func eventDateBtn(_ sender: Any) {
//  picker.isDatePickerOnly = true
//        picker.show()
        
    }
    @IBAction func backBtn(_ sender: Any) {
   navigationController?.popViewController(animated: true)
    }
    
    @IBAction func stateDropdownBtn(_ sender: Any) {
        
    }
    @IBAction func countryDropDownBtn(_ sender: Any) {

        
    }
    @IBAction func eventPictureBtn(_ sender: Any) {
        addImage()
    }
    
    
    @IBAction func saveBtn(_ sender: Any) {
     
        guard validator.validators(TF1: self.zipcodeTF,fieldName: "Zip Code") == false ||
            validator.validators(TF1: self.cityTF,fieldName: "City") == false ||
            validator.validators(TF1: self.ststeTF,fieldName: "State") == false || validator.validators(TF1: self.addressTF,fieldName: "Address") == false || validator.validators(TF1: self.eventTimetoTF,fieldName: "Event Time") == false
            || validator.validators(TF1: self.eventTimeFromTF,fieldName: "Event Time") == false
            || validator.validators(TF1: self.eventDateTF,fieldName: "Event Date") == false
            || validator.validators(TF1: self.enentTitleTF,fieldName: "Event Title") == false
            
            
            else
            
        {
            
                if descriptionTextView.text != ""
                {
                    // Hit APi Here
                    self.user = User.init(dictionary: NSDictionary())
                    let locDict = Eventadress.init(dictionary: NSDictionary())
                    locDict?.state = ststeTF.text
                    locDict?.city = cityTF.text
                    locDict?.zipCode = Int(zipcodeTF.text!)
                    locDict?.address = addressTF.text
                    locDict?.country = countryTF.text
                    
                    
                    let dateUtc = eventDateTF.text?.dateStringToUTCString
                     let barid = Constants.kUserDefaults.string(forKey: appConstants.barId)
                    print(Constants.kUserDefaults.string(forKey: appConstants.userId))
                    self.user?.id  = Constants.kUserDefaults.string(forKey: appConstants.userId)
                    self.user?.name = enentTitleTF.text
                    self.user?.description = descriptionTextView.text
                    print(user?.description)
                    print(descriptionTextView.text)
                    self.user?.date = newIsoDateString
                    self.user?.availbilityStart = newisotimeFromString
                    self.user?.availbilityTill = newIsotimeToString
                    self.user?.fbLink = fbLinkTF.text
                    self.user?.instaLink = instaLinkTF.text
                    self.user?.twitterLink = "aaaaaaaa"
                    self.user?.eventadress = locDict
                    self.user?.status = "I’m going"
                    self.user?.barId = barid
                    
                    if isEditEvent == true
                    {
                 self.UpdateEventApi(userDict: user!)
                    }
                    else
                    {
                        self.createEventApi(userDict: user!)

                    }
                }
                    
                else
                {
                    let banner = NotificationBanner(title: "ERROR", subtitle: "please make sure to enter description", style: .success)
                    banner.duration = 1
                    banner.show()
                }
                print("valid ph no")
            
           
            return
        }
        

    }
    
    // Get events Details Api()
    func GetEditEventApi() {
        let userId = Constants.kUserDefaults.value(forKey: appConstants.userId) as? String
        UserAPI().getEventsDetilsId(userId: userId!,pageNo: 0) { (data, error) in
            if data[APIConstants.isSuccess.rawValue] as! Bool == true {
                if let items = data["data"] as? [String:AnyObject]
                {
                    print(items)
                    var name = items["name"] as? String
                    var status = items["status"] as? String
                    
                    self.user = User.init(dictionary: items as NSDictionary)
                    
                    print(self.user?.eventadress?.city)
                    
                    self.enentTitleTF.text = self.user?.name
                    
                    let dateUtc = self.user?.date?.UTCToDateString
                    print(dateUtc)
                    self.eventDateTF.text = dateUtc
                    
                     print(self.user?.availbilityStart)
                    
                      //  UTCToTimeString
                    let avlStart = self.user?.availbilityStart!.UTCToTimeString
                     let avlTill = self.user?.availbilityTill!.UTCToTimeString
                    self.eventTimeFromTF.text = avlStart
                    self.eventTimetoTF.text = avlTill
                    
                    
                     self.descriptionTextView.text = self.user?.description
                     self.fbLinkTF.text = self.user?.fbLink
                    self.instaLinkTF.text = self.user?.instaLink
                    self.addressTF.text = self.user?.eventadress?.address
                    print(self.user?.eventadress?.country)
                    print(self.user?.description)
                    self.countryDropDown.text = self.user?.eventadress?.country
                    self.stateDropDown.text = self.user?.eventadress?.state
                    self.cityTF.text = self.user?.eventadress?.city
                    self.zipcodeTF.text = self.user?.eventadress?.zipCode?.description

                    
                    if let imgUrl = self.user?.imgUrl {
                        print(imgUrl)
                        
                        self.eventPicture.sd_setImage(with: URL(string: imgUrl ), placeholderImage: UIImage(named: "eventDummy"))
                    }
            //        self.descriptionTextView.text = description

                //    self.addressTF.text = combineAddress
                }
                
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }
    
    // Create new events Api()
    func createEventApi(userDict:User){
        print(userDict)
        SVProgressHUD.show(withStatus: "Please Wait")
        userApi.newEvent(userDetials: userDict){ (isSuccess,response, error) -> Void in
           
            
            if (isSuccess){
                SVProgressHUD.dismiss()
                let data = response?["data"] as? NSDictionary
                let newEventId = data?["id"] as! String
                
                Constants.kUserDefaults.set(newEventId, forKey: "newEventId")
                
        if self.compressedImage != nil
        {
            self.fileUploadAPI.uploadImageNewEvent(image: self.compressedImage!){ dataImage, error -> Void in
                if error == nil{
                    var picUrl:String?
                    picUrl = dataImage

                    Constants.kUserDefaults.set(dataImage, forKey: "picUrls")
                    let fetchUrl = Constants.kUserDefaults.url(forKey: "picUrls")
                    print(fetchUrl)
                    
                }
            }
                }

                self.navigationController?.popViewController(animated: true)
            }else{
                SVProgressHUD.dismiss()
                if error != nil{
                    kAppDelegate.showNotification(text: "Something went wrong!")
                    //kAppDelegate.showNotification(text: error!)
                }else{
                    kAppDelegate.showNotification(text: "Something went wrong!")
                }
            }
            
        }
        
    }
    
    // Create new events Api()
    func UpdateEventApi(userDict:User){
        print(userDict)
        SVProgressHUD.show(withStatus: "Please Wait")
        userApi.UpdateEvent(userDetials: userDict){ (isSuccess,response, error) -> Void in
            
            
            if (isSuccess){
                SVProgressHUD.dismiss()
             self.isEditEvent = false
                if self.compressedImage != nil
                {
                    self.fileUploadAPI.uploadImageNewEvent(image: self.compressedImage!){ dataImage, error -> Void in
                        if error == nil
                    {
                            var picUrl:String?
                            picUrl = dataImage
                            //  self.PictureURLVar = self.picUrl
                            Constants.kUserDefaults.set(dataImage, forKey: "picUrls")
                            let fetchUrl = Constants.kUserDefaults.url(forKey: "picUrls")
                            print(fetchUrl)
                            if self.compressedImage != nil
                            {
                                self.fileUploadAPI.uploadImageNewEvent(image: self.compressedImage!){ dataImage, error -> Void in
                                    if error == nil{
                                        var picUrl:String?
                                        picUrl = dataImage
                                        
                                        Constants.kUserDefaults.set(dataImage, forKey: "picUrls")
                                        let fetchUrl = Constants.kUserDefaults.url(forKey: "picUrls")
                                        print(fetchUrl)
                                        
                                    }
                                }
                            }

                        }
                    }
                }
                
                self.navigationController?.popViewController(animated: true)
            }else{
                SVProgressHUD.dismiss()
                if error != nil{
                    kAppDelegate.showNotification(text: "Something went wrong!")
                    //kAppDelegate.showNotification(text: error!)
                }else{
                    kAppDelegate.showNotification(text: "Something went wrong!")
                }
            }
            
        }
        
    }
    
    // 3.89.56.249:8010/v1/admin/state?type=1
    func getCountryistAPI() {
        //SVProgressHUD.show(withStatus: "Loading...")
        let tokenString = Constants.kUserDefaults.value(forKey: "token")
        
        let headers: HTTPHeaders = [
            "x-access-token": tokenString as! String,
            ]
        print(headers)
        let urlString = "http://159.89.226.199:8010/v1/admin/state?type=" + isCountry.description
        print(urlString)
        Alamofire.request(urlString, method: .get, headers: headers).responseJSON {
            response in
            switch response.result{
            case .success(_):
                let jsonData = response.result.value
                print(jsonData!)
//                var allcountries = jsonData?["data"] as? [[String: AnyObject]]
//                print(allcountries)
                if let JSON = response.result.value as? NSDictionary
                {
                    let status:Int = (JSON["success"] as? Int)!
                    print(JSON)
                    if status == 1{
                        SVProgressHUD.dismiss()
                        let jsonArray:NSArray = (JSON["data"] as? NSArray)!
                        print(jsonArray)
                  countries.modelsFromDictionaryArray(array: jsonArray)
                        self.countrynameAry.removeAll()

                        for countryData in jsonArray
                        {
                            let conData = countryData as! [String: AnyObject]
                           let name = conData["name"] as? String
                            print(name)
                            self.countrynameAry.append(name!)
                        }
                        print(self.countrynameAry)
                        self.countryModel = countries.modelsFromDictionaryArray(array: jsonArray)
                        print(self.countryModel)
                        self.countryDropDown.optionArray = self.countrynameAry
                        self.countryDropDown.didSelect{(selectedText , index ,id) in
                            self.countryDropDown.text = selectedText
                          self.selectedCountry = selectedText

                        
                        for data1 in self.countryModel
                        {
                            print(data1.name)
                            print(data1.id)
                            print(self.selectedCountry)
                            if data1.name == self.selectedCountry! {
                                self.idOfSelctedCountry = data1.id
                            }
                            print(self.idOfSelctedCountry)
                        }
                            if self.idOfSelctedCountry != nil
                            {
                                self.getStateListAPI()
                            }

                        }
                        
                        
                    }else{
                        SVProgressHUD.dismiss()
                        print("Fave api not call succes")
                      
                    }
                }
            case .failure(_):
                print("Faield Trinash")
             
            }}
    }
    
    func getStateListAPI() {
        //SVProgressHUD.show(withStatus: "Loading...")
        let tokenString = Constants.kUserDefaults.value(forKey: "token")
        
        let headers: HTTPHeaders = [
            "x-access-token": tokenString as! String,
            ]
        print(headers)
        let urlString = "http://159.89.226.199:8010/v1/admin/state?type=" + 2.description + "&countryId=\(self.idOfSelctedCountry!)"
        print(urlString)
        Alamofire.request(urlString, method: .get, headers: headers).responseJSON {
            response in
            switch response.result{
            case .success(_):
                let jsonData = response.result.value
                print(jsonData!)
                //                var allcountries = jsonData?["data"] as? [[String: AnyObject]]
                //                print(allcountries)
                if let JSON = response.result.value as? NSDictionary
                {
                    let status:Int = (JSON["success"] as? Int)!
                    print(JSON)
                    if status == 1{
                        SVProgressHUD.dismiss()
                        let jsonArray:NSArray = (JSON["data"] as? NSArray)!
                        print(jsonArray)
                        countries.modelsFromDictionaryArray(array: jsonArray)
                        self.stateNameAry.removeAll()
                        for stateData in jsonArray
                        {
                            let conData = stateData as! [String: AnyObject]
                            let name = conData["name"] as? String
                            print(name)
                            self.stateNameAry.append(name!)
                        }
                        print(self.stateNameAry)
                          self.ststeTF.text = self.stateNameAry[0]
                        self.stateDropDown.optionArray = self.stateNameAry
                        self.stateDropDown.didSelect{(selectedText , index ,id) in
                            self.stateDropDown.text = selectedText
                            
                            
                            
                        }
                        
                        
                    }else{
                        SVProgressHUD.dismiss()
                        print("Fave api not call succes")
                        
                    }
                }
            case .failure(_):
                print("Faield Trinash")
                
            }}
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countryModel.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCell")as! CountryCell
        return cell
    }
}

//MARK: - Extension for Image and Video Picker
extension CreateNewEventVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
//    //MARK: - Capture Media Using Camera
//    func captureMedia(){
//        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
//
//        let recordVideo = UIAlertAction(title: "Record Video", style: .default,handler: {
//            (alert: UIAlertAction!) -> Void in
//            let myPickerController = UIImagePickerController()
//            myPickerController.delegate = self
//            myPickerController.sourceType = UIImagePickerController.SourceType.camera
//         //   myPickerController.mediaTypes = [kUTTypeMovie as String]
//            self.present(myPickerController, animated: true, completion: nil)
//
//        })
//
//
//        let captureImage = UIAlertAction(title: "Capture Image", style: .default,handler: {
//            (alert: UIAlertAction!) -> Void in
//            let myPickerController = UIImagePickerController()
//            myPickerController.delegate = self
//            myPickerController.sourceType = UIImagePickerController.SourceType.camera
//       //     myPickerController.mediaTypes = [kUTTypeImage as String]
//            self.present(myPickerController, animated: true, completion: nil)
//
//        })
//
//        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
//            (alert: UIAlertAction!) -> Void in
//        })
//        optionMenu.addAction(recordVideo)
//        optionMenu.addAction(captureImage)
//        optionMenu.addAction(cancelAction)
//        self.present(optionMenu, animated: true, completion: nil)
//    }
//
//
//    //MARK: - Choose Media From Library
//    func chooseMediaFromLibrary(){
//        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
//
//
//        let chooseImageFromLibrary = UIAlertAction(title: "Choose Image from Library",style: .default,handler: {
//            (alert: UIAlertAction!) -> Void in
//
//            let myPickerController = UIImagePickerController()
//            myPickerController.delegate = self
//            myPickerController.sourceType = .photoLibrary
//        //    myPickerController.mediaTypes = [kUTTypeImage as String]
//            self.present(myPickerController, animated: true, completion: nil)
//
//
//        })
//
//
//        let chooseVideoFromLibrary = UIAlertAction(title: "Choose Video from Library",style: .default,handler: {
//            (alert: UIAlertAction!) -> Void in
//
//            let myPickerController = UIImagePickerController()
//            myPickerController.delegate = self
//            myPickerController.sourceType = .photoLibrary
//         //   myPickerController.mediaTypes = [kUTTypeMovie as String]
//            self.present(myPickerController, animated: true, completion: nil)
//
//
//        })
//
//
//        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
//            (alert: UIAlertAction!) -> Void in
//        })
//        optionMenu.addAction(chooseImageFromLibrary)
//        optionMenu.addAction(chooseVideoFromLibrary)
//        optionMenu.addAction(cancelAction)
//        self.present(optionMenu, animated: true, completion: nil)
//    }
    
    
    func addImage() {
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        let takePic = UIAlertAction(title: "Take Photo", style: .default,handler: {
            (alert: UIAlertAction!) -> Void in
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = UIImagePickerController.SourceType.camera
            self.present(myPickerController, animated: true, completion: nil)
            
        })
        
        let choseAction = UIAlertAction(title: "Choose from Library",style: .default,handler: {
            (alert: UIAlertAction!) -> Void in
            
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(myPickerController, animated: true, completion: nil)
            
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        optionMenu.addAction(takePic)
        optionMenu.addAction(choseAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        print("abc")
        guard let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            else
        {
            return
        }
        
        self.dismiss(animated: false, completion: { [weak self] in
            self?.eventPicture.image = originalImage
            self?.moveToImageCropper(image: originalImage)
        })
    }
    
}


extension CreateNewEventVC : CropViewControllerDelegate {
    
    private func moveToImageCropper(image: UIImage) {
        let cropController = CropViewController(croppingStyle: CropViewCroppingStyle.default, image: image)
        cropController.delegate = self
        cropController.aspectRatioPickerButtonHidden = true
        cropController.aspectRatioLockEnabled = true
        cropController.aspectRatioPreset = .preset3x2
        self.present(cropController, animated: true, completion: nil)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        self.eventPicture.contentMode = .scaleAspectFill
        self.eventPicture.image = image
        let compressData = image.jpegData(compressionQuality: 0.75)
        self.compressedImage = UIImage(data: compressData!)
        

            cropViewController.dismiss(animated: true, completion: nil)
    } }
    

class CountryCell: UITableViewCell {
    
    @IBOutlet weak var countryName: UILabel!
}



