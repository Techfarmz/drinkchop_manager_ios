//
//  DrinkSelection2VC.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 27/06/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import NotificationBannerSwift
import iOSDropDown
import CropViewController
import Toast_Swift

class DrinkSelection2VC: UIViewController, UITableViewDataSource, UITableViewDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var categoryDropdown: DropDown!
    @IBOutlet weak var signatureDrinkPrice: UITextField!
    @IBOutlet weak var SignatureDrinksetPriceOutlet: UIButton!
    @IBOutlet weak var signatureDrinkPriceDoubleLbl: UILabel!
    @IBOutlet weak var signatureDrinkPriceDoubleView: UIView!
    @IBOutlet weak var signatureDrinkPriceDouble: UITextField!
    @IBOutlet weak var signatureDrinkDescription: UITextField!
    @IBOutlet weak var signatureDrinkCategory: UITextField!
    @IBOutlet weak var signatureDrinkNameTF: UITextField!
    @IBOutlet weak var signatureDrinkPic: UIImageView!
    @IBOutlet weak var signatureDrinkView: UIView!
    @IBOutlet weak var signatureDrinkOutlet: UIButton!
    @IBOutlet weak var selectionOutlet: UIButton!
    @IBOutlet weak var baselineOutlet: UIButton!
    @IBOutlet weak var selectionTableView: UITableView!

    var importRegularPriceClicked = false
    var importDoublePriceClicked = false
      var selectedCat = String()
     var compressedImage = UIImage()
    var currentDrinkId = String()
     var drinknameFromApi = String()
    var categoryDropdownAry = [String]()
       var validator = Validators()
     var signatureDrinksAryy = NSArray()
     var signatureDrinkCatNameAry = [String]()
    var importBtnClicked = false
    var drinkAry = [String]()
    var catClicked = false
    var baselineDrinkSelectedAry = [String]()
    var drinkName = String()
    private var fileUploadAPI:FileUpload!

    var baselineCatNameAry = [String]()
    var sectionsWithStatesDicr = [String:String]()
    var signatureSectionsWithStatesDicr = [String:String]()
    var selectedCategoryDict = [String:String]()
      var signatureSelectedCategoryDict = [String:String]()
    var drinkCatModel : [Category] = []
    var selectionCatModel : [SelectionCategory] = []
    var DrinktitleAry: [String] = []
    var currentsecTitle = String()
    var signatureCurrentsecTitle = String()
    //   var selectedIndx = -1
    
    var thereIsCellTapped = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoryDropdown.delegate = self
        signatureDrinkPriceDoubleLbl.isHidden = true
        signatureDrinkPriceDoubleView.isHidden = true
        self.fileUploadAPI = FileUpload.sharedInstance
        categoryDropdown.didSelect{(selectedText , index ,id) in
            self.signatureDrinkCategory.text = selectedText
        }
        
        signatureDrinkView.isHidden = true
        self.getSignatureDrink()
        self.importedDataApi()
        selectionTableView.allowsMultipleSelection = true
        selectionTableView.separatorStyle = .none
        selectionOutlet.backgroundColor = UIColor.brown

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.selectionTableView.reloadData()
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == categoryDropdown
        {
            return false
            
        }
        
        return true
    }
    
    @IBAction func saveBtnClicked(_ sender: Any) {
        self.importBtnClicked = true
        //        DispatchQueue.main.async(execute: {
        //            self.selectionTableView.reloadData()
        //
        //        })
        self.selectionTableView.reloadData()
        // Delay Function
        DispatchQueue.main.asyncAfter(deadline: .now() + 10.0, execute: {
          //  self.importSelectionApi()
            self.importBtnClicked = false
            
        })
    }
    @IBAction func backBtn(_ sender: Any) {
    navigationController?.popViewController(animated: false)
    }
    @IBAction func signatureDrinkCancelBtn(_ sender: Any) {
        signatureDrinkView.isHidden = true

    }
    @IBAction func signatureDrinksetPriceBtn(_ sender: Any) {
       let btnImage = #imageLiteral(resourceName: "uncheckedBox")
        print(btnImage)
        if SignatureDrinksetPriceOutlet.currentBackgroundImage == btnImage
        {
            signatureDrinkPriceDoubleLbl.isHidden = false
            signatureDrinkPriceDoubleView.isHidden = false
           SignatureDrinksetPriceOutlet.setBackgroundImage(#imageLiteral(resourceName: "checkedBox"), for: .normal)
        }
        else
        {
            SignatureDrinksetPriceOutlet.setBackgroundImage(#imageLiteral(resourceName: "uncheckedBox"), for: .normal)
            signatureDrinkPriceDoubleLbl.isHidden = true
            signatureDrinkPriceDoubleView.isHidden = true
        }
        
        
    
    }
    @IBAction func signatureDrinkOkBtn(_ sender: Any) {
        guard validator.validators(TF1: self.signatureDrinkNameTF,fieldName: "Drink Name") == false ||
            validator.validators(TF1: self.signatureDrinkCategory,fieldName: "Drink Category") == false ||
            validator.validators(TF1: self.signatureDrinkDescription,fieldName: "Drink Description") == false || validator.validators(TF1: self.signatureDrinkPriceDouble,fieldName: "Alcohol Percentage") == false || validator.validators(TF1: self.signatureDrinkPrice,fieldName: "Drink Price") == false
            
            
            else
        {
            if signatureDrinkPic.image == nil
            {
                let banner = NotificationBanner(title: "ERROR", subtitle: "Password Do Not Match", style: .danger)
                banner.duration = 1
                banner.show()
            }
            else
            {
                self.addSignatureDrinkApi()
                
            }
            return
        }
       

    }
    // Save Drinks Api
//    func saveDrinksApi()
//    {
//        SVProgressHUD.show(withStatus: "Loading...")
//        SVProgressHUD.setDefaultMaskType(.clear)
//        let token = Constants.kUserDefaults.string(forKey: appConstants.token)
//        let urlString = remoteConfig.mBaseUrl + "selectionCategories/updatedrink/drink"
//        print(urlString)
//        let _headers : HTTPHeaders = ["x-access-token": token!]
//        let params : Parameters = ["drinks": ]
//        print(params)
//        print(self.drinkAry)
//        request(urlString, method: .post, parameters: params, encoding: URLEncoding.httpBody , headers: _headers).responseJSON(completionHandler: {
//            response in response
//
//            print(response.result)
//            if response.result.isSuccess == true
//            {
//                SVProgressHUD.dismiss()
//                self.drinkAry.removeAll()
//                let jsonResponse = response.result.value as! NSDictionary
//                print(jsonResponse)
//                self.view.makeToast("Drink imported successfully")
//            }
//        })
//        SVProgressHUD.dismiss()
//    }
//
    
    // Add signatureDrink Api()
    func addSignatureDrinkApi()
    {
        let token = Constants.kUserDefaults.string(forKey: appConstants.token)
        let urlString = remoteConfig.mBaseUrl + "selectionCategories/data/next"
        print(urlString)
        let _headers : HTTPHeaders = ["x-access-token": token!]
        let params : Parameters = ["alcoholPercentage": signatureDrinkPriceDouble.text!,"name": signatureDrinkNameTF.text!,"description": signatureDrinkDescription.text!,"categoriesname": signatureDrinkCategory.text!,"regularPrice": signatureDrinkPrice.text!,"doublePrice": signatureDrinkPriceDouble.text!]
        print(params)
        request(urlString, method: .post, parameters: params, encoding: URLEncoding.httpBody , headers: _headers).responseJSON(completionHandler: {
            response in response
            
            print(response.result)
            if response.result.isSuccess == true
            {
                  self.signatureDrinkView.isHidden = true
                self.view.makeToast("Drink added successfully")

                self.drinkAry.removeAll()
                let jsonResponse = response.result.value as! NSDictionary
                print(jsonResponse)
                let jsonData = jsonResponse["data"] as! [String: AnyObject]
                print(jsonData)
                let drinks = jsonData["drinks"] as! NSArray
                for drnk in drinks
                {
                    print(drnk)
                    let drnkDic = drnk as! [String: AnyObject]
                    print(drnkDic)
                    let currentName = self.signatureDrinkNameTF.text!
                    self.drinknameFromApi = drnkDic["name"] as! String
                    if self.signatureDrinkNameTF.text! == self.drinknameFromApi
                    {
                        self.currentDrinkId = drnkDic["id"] as! String
                        self.fileUploadAPI.uploadImageaaaRemote(image: self.compressedImage){ dataImage, error -> Void in
                            if error == nil{
                                var picUrl:String?
                                picUrl = dataImage
                                //  self.PictureURLVar = self.picUrl
                             
                            }
                        }

                    }
                   print(self.currentDrinkId)
                }
            }
            
            
        })
    }
    
    @IBAction func selectionBtn(_ sender: Any) {
    
    }
    @IBAction func signatureDrinkAddImageBtn(_ sender: Any) {
        self.addImage()
    }
    
    @IBAction func signatureDrinkBtn(_ sender: Any) {
   signatureDrinkView.isHidden = false
    }
    @IBAction func baselineBtn(_ sender: Any) {
        navigationController?.popViewController(animated: false)
        
    }
    
    func checkSectionAndReturn (value: Int, noOfSections:Int) -> CGFloat {
        
        for i in 0...noOfSections {
            if value == i {
                if self.sectionsWithStatesDicr[self.currentsecTitle] == "true" {return 50} else {return 0}
            }
                
            else if value == i {
                if self.sectionsWithStatesDicr[self.currentsecTitle] == "true" {return 50} else {return 0}
            }
            
        }
        return 0
    }
    func signatureCheckSectionAndReturn (value: Int, noOfSections:Int) -> CGFloat {
       
        for i in 0...noOfSections {
             print(i)
            if value == i {
                if self.signatureSectionsWithStatesDicr[self.signatureCurrentsecTitle] == "true" {return 50} else {return 0}
            }
                
            else if value == i {
                if self.signatureSectionsWithStatesDicr[self.signatureCurrentsecTitle] == "true" {return 50} else {return 0}
            }
            
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var currentIndex = Int()
        if indexPath.section >= selectionCatModel.count {
            for (index,i) in self.drinkCatModel.enumerated() {
                currentIndex = index
            }
            //  return (self.drinkCatModel[currentIndex-1].drinks?.count)!
            
            let currentsecTitleForRow = self.drinkCatModel[currentIndex].name

            self.signatureCurrentsecTitle = (currentsecTitleForRow?.description)!

            return self.signatureCheckSectionAndReturn(value: currentIndex, noOfSections: self.drinkCatModel.count - 1)
        }
        else
        {
            let currentsecTitleForRow = self.selectionCatModel[indexPath.section].categoriesname
            
            self.currentsecTitle = (currentsecTitleForRow?.description)!
            
            return self.checkSectionAndReturn(value: indexPath.section, noOfSections: self.selectionCatModel.count - 1)
        }
        }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        var secCountv = self.selectionCatModel.count - 1
        if section == secCountv {
        return 50
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerCell = tableView.dequeueReusableCell(withIdentifier: "DrinkSelectionCell2ID") as! DrinkSelectionCell2
        
       
        var secCountv = self.selectionCatModel.count - 1
        if section == secCountv {
            var uiLabel = UILabel()
            
            uiLabel.text = "Signature Details"
            uiLabel.textColor = UIColor.orange
            //footerCell.dropDown.isHidden = true
                return uiLabel
        }
        
        else {
            return nil
        }
    
    
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
     
        var currentIndex = Int()
        if section >= selectionCatModel.count {
            print(self.drinkCatModel.enumerated())
            for (index,i) in self.drinkCatModel.enumerated() {
                currentIndex = index
            }
          //  return (self.drinkCatModel[currentIndex-1].drinks?.count)!
        
        
   let headerCell = selectionTableView.dequeueReusableCell(withIdentifier: "DrinkSelectionCell3ID")as! DrinkSelectionCell3
        
        print(section - selectionCatModel.count)
            print("current index is = \(currentIndex)")
        headerCell.name.text = self.drinkCatModel[section - selectionCatModel.count].name
        self.signatureCurrentsecTitle = self.drinkCatModel[section - selectionCatModel.count].name!
        print(self.signatureCurrentsecTitle)

        headerCell.selectionImg.tag = section
        headerCell.dropDown.tag = section
        headerCell.dropDown.isHidden = false
        headerCell.drinkPriceView.isHidden = true
        headerCell.selectionLeadingConst.constant = 21.0
          headerCell.deleteBtn.isHidden = true
            headerCell.drinkPriceview2.isHidden = true

            
        //  headerCell.selectionImg.currentBackgroundImage = UIImage.init(cgImage: "CircleFill")
            
        headerCell.dropDown.addTarget(self, action: #selector(DrinkSelection2VC.signatureBtnSectionClick(sender:)), for: .touchUpInside)
            headerCell.selectionImg.addTarget(self, action: #selector(DrinkSelection2VC.btnSelectionDrink(sender:)), for: .touchUpInside)
            if self.catClicked == true
            {
                let myCurrentSection = self.signatureCurrentsecTitle
                print(myCurrentSection)
                print(self.signatureSelectedCategoryDict)
                if self.signatureSelectedCategoryDict[myCurrentSection]  == "false" {
                    headerCell.selectionImg.setImage(UIImage(named: "CircleBlank.png"), for: .normal)
                    
                }
                else if  self.signatureSelectedCategoryDict[myCurrentSection] == "true"{
                    headerCell.selectionImg.setImage(UIImage(named: "CircleFill.png"), for: .normal)
                    
                }
            }
            
            
       
        return headerCell
        }
       
        else
       {
        let headerCell2 = tableView.dequeueReusableCell(withIdentifier: "DrinkSelectionCell2ID") as! DrinkSelectionCell2
         headerCell2.name.text = self.selectionCatModel[section].categoriesname
        self.currentsecTitle = self.selectionCatModel[section].categoriesname!
        print(self.currentsecTitle)
        
           print("section is = " + section.description)
       
        headerCell2.selectionImg.tag = section
        headerCell2.dropDown.tag = section
        headerCell2.dropDown.isHidden = false
        headerCell2.drinkPriceView.isHidden = true
        headerCell2.deleteBtn.isHidden = true
        headerCell2.drinkPriceView2.isHidden = true
        headerCell2.selectionLeadingConst.constant = 21.0
        
        //  headerCell.selectionImg.currentBackgroundImage = UIImage.init(cgImage: "CircleFill")
        
        headerCell2.dropDown.addTarget(self, action: #selector(DrinkSelection2VC.btnSectionClick(sender:)), for: .touchUpInside)
        headerCell2.selectionImg.addTarget(self, action: #selector(DrinkSelection2VC.btnSelectionDrink(sender:)), for: .touchUpInside)

 
        
     
        let myCurrentSection = self.currentsecTitle
        
        if self.selectedCategoryDict[myCurrentSection]  == "false" {
            headerCell2.selectionImg.setImage(UIImage(named: "CircleBlank.png"), for: .normal)
            
        }
        else if  self.selectedCategoryDict[myCurrentSection] == "true"{
            headerCell2.selectionImg.setImage(UIImage(named: "CircleFill.png"), for: .normal)
            
        }
       return headerCell2
        }
       
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        var topSectionsCount = self.selectionCatModel.count
        var bottomSectionsCount = self.drinkCatModel.count
       var totalCount = topSectionsCount + bottomSectionsCount
        
        //var secCount = (self.selectionCatModel.count  + drinkCatModel.count ) - 2
     
             return totalCount
       
        //return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        var currentIndex = Int()
        
        
          if section >= selectionCatModel.count {
            for (index,i) in self.drinkCatModel.enumerated() {
                currentIndex = index
            }
              return (self.drinkCatModel[currentIndex-1].drinks?.count)!
        }
            
            
        else
          {
        return (self.selectionCatModel[section].drinks?.count)!
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var currentIndex = Int()
        if indexPath.section >= selectionCatModel.count {
            for (index,i) in self.drinkCatModel.enumerated() {
                currentIndex = index
            }
                     print("current index is = \(currentIndex)")
            //  return (self.drinkCatModel[currentIndex-1].drinks?.count)!
            let cell = selectionTableView.dequeueReusableCell(withIdentifier: "DrinkSelectionCell3ID")as! DrinkSelectionCell3
            cell.dropDown.isHidden = true
            cell.name.textAlignment = .right
            
            cell.selectionLeadingConst.constant = 1.0
            view.layoutIfNeeded()
            cell.selectionStyle = .none
            //  cell.selectionImg.tag = indexPath.row
            cell.selectionImg.tag = (indexPath.section * 1000) + indexPath.row
            
            cell.selectionImg.addTarget(self, action: #selector(DrinkSelection2VC.signatureSelectDrinkClick(sender:)), for: .touchUpInside)
            print("Section is \(indexPath.section) Row is: \(indexPath.row)")
            //   print("Drink at \(indexPath.row) is \(self.drinkCatModel[indexPath.row].drinks![indexPath.row].name)")
            
            var currentIndex = Int()
                
                self.signatureCurrentsecTitle = self.drinkCatModel[currentIndex].name!
                print(self.signatureCurrentsecTitle)
                let myCurrentSection = self.signatureCurrentsecTitle
                if self.catClicked == true
                {
                    print(drinkCatModel[indexPath.section].name)
                    //    print(self.selectedCat)
                    let drnkkMdl = drinkCatModel[indexPath.section].name
                    if drnkkMdl == self.selectedCat
                    {
                        print(self.selectedCat)
                        if self.selectedCategoryDict[self.selectedCat]  == "false"
                        {
                            
                            cell.selectionImg.setBackgroundImage(#imageLiteral(resourceName: "CircleBlank"), for: .normal)
                        }
                        else if  self.selectedCategoryDict[self.selectedCat] == "true"
                        {
                            cell.selectionImg.setBackgroundImage(#imageLiteral(resourceName: "CircleFill"), for: .normal)
                        }
                    }
                }
                cell.name.text = self.drinkCatModel[currentIndex].drinks![currentIndex].name

            if self.importBtnClicked == true
            {
                let btnImage = #imageLiteral(resourceName: "CircleBlank")
                
                if cell.selectionImg.currentBackgroundImage == btnImage
                {
                    
                }
                else
                {
                    let alldrinksid = self.drinkCatModel[indexPath.row].drinks![indexPath.row].id
                    print(alldrinksid)
                    //  let drinkId = alldrinks!["id"] as! String
                    self.drinkAry.append(alldrinksid!)
                    print("Values of drinkarray is \(drinkAry)")
                    
                }
            }
            return cell
        }
        else
        {
            print("cell 2 called")
        
        let cell = selectionTableView.dequeueReusableCell(withIdentifier: "DrinkSelectionCell2ID")as! DrinkSelectionCell2
        cell.dropDown.isHidden = true
        cell.name.textAlignment = .right
            
         //   cell.drinkPriceLbl.isEnabled = false
         //   cell.drinkPrice2.isEnabled = false
        cell.selectionLeadingConst.constant = 1.0
        view.layoutIfNeeded()
        cell.selectionStyle = .none
        cell.selectionImg.tag = (indexPath.section * 1000) + indexPath.row
        cell.deleteBtn.tag = (indexPath.section * 1000) + indexPath.row
            
        cell.selectionImg.addTarget(self, action: #selector(DrinkSelection2VC.selectDrinkClick(sender:)), for: .touchUpInside)
        cell.deleteBtn.addTarget(self, action: #selector(DrinkSelection2VC.deleteImportedDrink(sender:)), for: .touchUpInside)
        //    cell.drinkPriceLbl.addTarget(self, action: #selector(importregularPriceBtn), for: .touchDown)
        //    cell.drinkPrice2.addTarget(self, action: #selector(importDoublePiceBtn), for: .touchDown)

            cell.drinkPriceLbl.delegate = self
            cell.drinkPrice2.delegate = self
        print("Section is \(indexPath.section) Row is: \(indexPath.row)")
        //   print("Drink at \(indexPath.row) is \(self.drinkCatModel[indexPath.row].drinks![indexPath.row].name)")
       
            var currentIndex = Int()
            
            if indexPath.section >= selectionCatModel.count {
                for (index,i) in self.drinkCatModel.enumerated() {
                    currentIndex = index
                }
                //  return (self.drinkCatModel[currentIndex-1].drinks?.count)!
            
            self.signatureCurrentsecTitle = self.drinkCatModel[currentIndex].name!
            print(self.signatureCurrentsecTitle)
            let myCurrentSection = self.currentsecTitle
            if self.catClicked == true
            {
                print(drinkCatModel[indexPath.section].name)
                //    print(self.selectedCat)
                let drnkkMdl = drinkCatModel[indexPath.section].name
                if drnkkMdl == self.selectedCat
                {
                if self.signatureSelectedCategoryDict[myCurrentSection]  == "false" {
                    // cell.selectionImg.setImage(UIImage(named: "CircleBlank.png"), for: .normal)
                    cell.selectionImg.setBackgroundImage(#imageLiteral(resourceName: "CircleBlank"), for: .normal)
                    cell.drinkPriceLbl.isEnabled = false
                    cell.drinkPrice2.isEnabled = false
                }
                else if  self.signatureSelectedCategoryDict[myCurrentSection] == "true"{
                    //  cell.selectionImg.setImage(UIImage(named: "CircleFill.png"), for: .normal)
                    cell.selectionImg.setBackgroundImage(#imageLiteral(resourceName: "CircleFill"), for: .normal)
                    cell.drinkPriceLbl.isEnabled = true
                    cell.drinkPrice2.isEnabled = true
                    }
                }
            }
            cell.name.text = self.drinkCatModel[currentIndex].drinks![currentIndex].name
            }
            else
          {
            if importRegularPriceClicked == true
            {
                let btnImage = #imageLiteral(resourceName: "CircleBlank")
                
                if cell.selectionImg.currentBackgroundImage == btnImage
                {
                    
                }
                else
                {
                    print(cell.drinkPriceLbl.text!)
                   self.selectionCatModel[indexPath.section].drinks![indexPath.row].regularPrice = cell.drinkPriceLbl.text
                    self.selectionCatModel[indexPath.section].drinks![indexPath.row].doublePrice = cell.drinkPrice2.text
                }
                
            }
            let btnImage = #imageLiteral(resourceName: "CircleBlank")
            
            if cell.selectionImg.currentBackgroundImage == btnImage
            {
                cell.drinkPriceLbl.isEnabled = false
                cell.drinkPrice2.isEnabled = false
            }
            else
            {
                cell.drinkPriceLbl.isEnabled = true
                cell.drinkPrice2.isEnabled = true
                
               

                
            }

        self.currentsecTitle = self.selectionCatModel[indexPath.section].categoriesname!
        print(self.currentsecTitle)
            print(self.signatureCurrentsecTitle)
//        let myCurrentSection = self.currentsecTitle
//            let myCurrentSection = self.signatureCurrentsecTitle
            if self.catClicked == true
            {
                
                print(self.selectedCat)
                let drnkkMdl = selectionCatModel[indexPath.section].categoriesname
                print(drnkkMdl)
                if drnkkMdl == self.selectedCat
                {
                    print(self.selectedCat)
                    if self.selectedCategoryDict[self.selectedCat]  == "false"
                    {
                        
                        cell.selectionImg.setBackgroundImage(#imageLiteral(resourceName: "CircleBlank"), for: .normal)
                        cell.drinkPriceLbl.isEnabled = false
                        cell.drinkPrice2.isEnabled = false

                    }
                    else if  self.selectedCategoryDict[self.selectedCat] == "true"
                    {
                        cell.selectionImg.setBackgroundImage(#imageLiteral(resourceName: "CircleFill"), for: .normal)
                        cell.drinkPriceLbl.isEnabled = true
                        cell.drinkPrice2.isEnabled = true
                        
                    }
                }
            }
            
            cell.name.text = self.selectionCatModel[indexPath.section].drinks![indexPath.row].name
            cell.drinkPriceLbl.text = self.selectionCatModel[indexPath.section].drinks![indexPath.row].regularPrice
            cell.drinkPrice2.text = self.selectionCatModel[indexPath.section].drinks![indexPath.row].doublePrice
            
            }
        if self.importBtnClicked == true
        {
            let btnImage = #imageLiteral(resourceName: "CircleBlank")
            
            if cell.selectionImg.currentBackgroundImage == btnImage
            {
                
            }
            else
            {
                let alldrinksid = self.selectionCatModel[indexPath.row].drinks![indexPath.row].id
                print(alldrinksid)
                //  let drinkId = alldrinks!["id"] as! String
                self.drinkAry.append(alldrinksid!)
                print("Values of drinkarray is \(drinkAry)")
                
            }
            
        }
            
//           if importRegularPriceClicked == true
//           {
//            let btnImage = #imageLiteral(resourceName: "CircleBlank")
//
//            if cell.selectionImg.currentBackgroundImage == btnImage
//            {
//                cell.drinkPriceLbl.isEnabled = false
//                cell.drinkPrice2.isEnabled = false
//            }
//            else
//            {
//
//                cell.drinkPriceLbl.isEnabled = true
//                 cell.drinkPrice2.isEnabled = true
//                cell.drinkPriceLbl.becomeFirstResponder()
////                let alldrinksid = self.drinkCatModel[indexPath.row].drinks![indexPath.row].id
////                print(alldrinksid)
////                //  let drinkId = alldrinks!["id"] as! String
////                self.drinkAry.append(alldrinksid!)
////                print("Values of drinkarray is \(drinkAry)")
//
//            }
//
//            }
        
//            if importDoublePriceClicked == true
//            {
//
//
//            }
            
        return cell
    }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        print(indexPath.section)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
  //      self.importRegularPriceClicked = true
//         self.selectionTableView.reloadData()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        importRegularPriceClicked = true
        self.selectionTableView.reloadData()
    }
    @objc func signatureSelectDrinkClick(sender:UIButton!)
    {
        var currentIndex = Int()
        if sender.tag >= selectionCatModel.count {
            for (index,i) in self.drinkCatModel.enumerated() {
                currentIndex = index
            }
        self.catClicked = false
        
        print("selected index",sender.tag)
        //  self.currentsecTitle = self.baselineCatNameAry[sender.tag]
        //   print(self.currentsecTitle)
        //   let myCurrentSection = self.currentsecTitle
        
        let row = sender.tag % 1000
        let section = sender.tag / 1000
        
        let indexPath = IndexPath(row: row, section:section )
        let cell = selectionTableView.cellForRow(at: indexPath) as! DrinkSelectionCell3
        let btnImage = #imageLiteral(resourceName: "CircleBlank")
        if cell.selectionImg.currentBackgroundImage == btnImage
        {
            cell.selectionImg.setBackgroundImage(#imageLiteral(resourceName: "CircleFill"), for: .normal)
        }
        else
        {
            cell.selectionImg.setBackgroundImage(#imageLiteral(resourceName: "CircleBlank"), for: .normal)
            
        }
        self.selectionTableView.reloadData()
        
    }
    }
    @objc func importregularPriceBtn(sender:UIButton!)
    {
     self.importRegularPriceClicked = true
        self.selectionTableView.reloadData()
    }
    
    @objc func importDoublePiceBtn(sender:UIButton!)
    {
        self.importDoublePriceClicked = true
        self.selectionTableView.reloadData()

    }
    
    @objc func selectDrinkClick(sender:UIButton!)
    {
        self.catClicked = false
        
        print("selected index",sender.tag)
        //  self.currentsecTitle = self.baselineCatNameAry[sender.tag]
        //   print(self.currentsecTitle)
        //   let myCurrentSection = self.currentsecTitle
        
        let row = sender.tag % 1000
        let section = sender.tag / 1000
        
        let indexPath = IndexPath(row: row, section:section )
        let cell = selectionTableView.cellForRow(at: indexPath) as! DrinkSelectionCell2
        let btnImage = #imageLiteral(resourceName: "CircleBlank")
        print(btnImage)
        if cell.selectionImg.currentBackgroundImage == btnImage
        {
            cell.selectionImg.setBackgroundImage(#imageLiteral(resourceName: "CircleFill"), for: .normal)
        }
        else
        {
            cell.selectionImg.setBackgroundImage(#imageLiteral(resourceName: "CircleBlank"), for: .normal)
            
        }
        self.selectionTableView.reloadData()
        
    }
    @objc func deleteImportedDrink(sender:UIButton!)
    {
      //  self.selectionTableView.reloadData()
        print(sender.tag)
        let row = sender.tag % 1000
        let section = sender.tag / 1000
        
        let indexPath = IndexPath(row: row, section:section )
        print(indexPath)
        let cell = selectionTableView.cellForRow(at: indexPath) as! DrinkSelectionCell2
        
       let delDrinkId = self.selectionCatModel[indexPath.section].drinks![indexPath.row].id
        print(delDrinkId)
        
        self.deleteImportDrinkApi(DrinkId: delDrinkId!)
    }
    @objc func btnSectionClick(sender:UIButton!)
    {

        print("selected index",sender.tag)
        self.currentsecTitle = self.selectionCatModel[sender.tag].categoriesname!
        print(self.currentsecTitle)
        
        let myCurrentSection = self.currentsecTitle
        
        
        if self.sectionsWithStatesDicr[myCurrentSection]  == "false" {
            self.sectionsWithStatesDicr[myCurrentSection] = "true"
        }
            
        else if  self.sectionsWithStatesDicr[myCurrentSection] == "true"{
            self.sectionsWithStatesDicr[myCurrentSection]  = "false"
            
        }
        self.selectionTableView.reloadData()
        
    }
   
    @objc func signatureBtnSectionClick(sender:UIButton!)
    {
        
        var currentIndex = Int()
        if sender.tag >= selectionCatModel.count {
            for (index,i) in self.drinkCatModel.enumerated() {
                currentIndex = index
            }
            //  return (self.drinkCatModel[currentIndex-1].drinks?.count)!
            print("CurrentIndex is \(currentIndex )")
        print("selected index",sender.tag - selectionCatModel.count)
        self.signatureCurrentsecTitle = self.drinkCatModel[sender.tag - selectionCatModel.count].name!
        print(self.signatureCurrentsecTitle)
        
        let myCurrentSection = self.signatureCurrentsecTitle
        
        
        if self.signatureSectionsWithStatesDicr[myCurrentSection]  == "false" {
            self.signatureSectionsWithStatesDicr[myCurrentSection] = "true"
        }
            
        else if  self.signatureSectionsWithStatesDicr[myCurrentSection] == "true"{
            self.signatureSectionsWithStatesDicr[myCurrentSection]  = "false"
            
        }
        self.selectionTableView.reloadData()
        }
        
    }
    
    @objc func btnSelectionDrink(sender:UIButton!)
    {
        self.catClicked = true
        self.currentsecTitle = self.selectionCatModel[sender.tag].categoriesname!
        print(self.currentsecTitle)
        let myCurrentSection = self.currentsecTitle
           self.selectedCat = self.currentsecTitle
        print("selected index",sender.tag)
        if self.selectedCategoryDict[myCurrentSection]  == "false" {
            self.selectedCategoryDict[myCurrentSection] = "true"
        
        }
            
        else if  self.selectedCategoryDict[myCurrentSection] == "true"{
            self.selectedCategoryDict[myCurrentSection]  = "false"
            
        }
        self.selectionTableView.reloadData()
        
    }
    
    
    func getSignatureDrink() {
        SVProgressHUD.show(withStatus: "Loading...")
        SVProgressHUD.setDefaultMaskType(.clear)
        let userId = Constants.kUserDefaults.value(forKey: appConstants.userId) as? String
        UserAPI().getSignatureDrnk(userId: userId!,pageNo: 0) { (data, error) in
            if data[APIConstants.isSuccess.rawValue] as! Bool == true {
                SVProgressHUD.dismiss()
                print(data)
                let itemsAry = data["items"] as! NSArray
                print(itemsAry)
                for itemData in itemsAry
                {
                    print(itemData)
                    let itemDta = itemData as! NSDictionary
                    let catName = itemDta["categoriesname"] as! String
                    self.signatureDrinkCatNameAry.append(catName)
                    let id = itemDta["id"] as! String
                    self.signatureDrinksAryy = itemDta["drinks"] as! NSArray
                }
                
                
               
                //        print(self.sectionsWithStatesDicr)
                print(self.selectedCategoryDict)
                self.drinkCatModel = Category.modelsFromDictionaryArray(array: itemsAry as NSArray)
              
                // save all state as false
                var signatureDrinkAry = [String]()
                for drinkscat in self.drinkCatModel
                {
                    print(drinkscat.name)
                    signatureDrinkAry.append(drinkscat.name!)
                }
                
               
                
                //                var drinksss = self.drinkCatModel[indexPath.row].drinks
                //                print(drinksss)
                //
                //                var drinkName = String()
                //                for i in drinksss!
                //                {
                //                    print("Index path is: \(indexPath.row) Name of drink: \(i.name)")
                //                    //            cell.name.text = i.name
                //                    drinkName = i.name!
                //                     self.selectionTableView.reloadData()
                //                }
                
                
                self.selectionTableView.reloadData()
                
                
            }
            else{
                print("Getting Error")
                SVProgressHUD.dismiss()
            }
            
        }
    }
    
    // Delete Imported drink api
    func deleteImportDrinkApi(DrinkId: String)
    {
        SVProgressHUD.show(withStatus: "Loading...")
        SVProgressHUD.setDefaultMaskType(.clear)

        
        let token = Constants.kUserDefaults.string(forKey: appConstants.token)
        print(token)
        
        let urlString = remoteConfig.mBaseUrl + "selectionCategories/" + DrinkId
        print(urlString)
        let _headers : HTTPHeaders = ["x-access-token": token!]

        request(urlString, method: .delete, parameters: nil, encoding: JSONEncoding.default , headers: _headers).responseJSON(completionHandler: {
            response in response
            print(response.result)
            print(response.result.value)
            if response.result.isSuccess == true
            {
                self.view.makeToast("Drink deleted successfully")
                
                let jsonResponse = response.result.value as! NSDictionary
                print(jsonResponse)
              SVProgressHUD.dismiss()
            self.importedDataApi()
            }
            
            
        })
        SVProgressHUD.dismiss()
       
    }
    
  //Get Import Data
    func importedDataApi()
    {
        SVProgressHUD.show(withStatus: "Loading...")
        SVProgressHUD.setDefaultMaskType(.clear)
        let token = Constants.kUserDefaults.string(forKey: appConstants.token)
        let urlString = remoteConfig.mBaseUrl + "selectionCategories"
        print(urlString)
        let _headers : HTTPHeaders = ["x-access-token": token!]
        let params : Parameters = ["drinks": self.drinkAry]
        
        request(urlString, method: .get, parameters: params, encoding: URLEncoding.httpBody , headers: _headers).responseJSON(completionHandler: {
            response in response
            
            print(response.result)
            if response.result.isSuccess == true
            {
                SVProgressHUD.dismiss()
                let jsonDat = response.result.value as! NSDictionary
                let itemsAry = jsonDat["items"] as! NSArray
                print(itemsAry)
                self.selectionCatModel = SelectionCategory.modelsFromDictionaryArray(array: itemsAry as NSArray)
                print(jsonDat)
                
                var drinksAry = [String]()
                for drinkscat in self.selectionCatModel
                {
                    print(drinkscat.categoriesname)
                    drinksAry.append(drinkscat.categoriesname!)
                }
                
                
                for i in drinksAry {
                    self.sectionsWithStatesDicr[i] = "false"
                    
                    self.selectedCategoryDict[i] = "false"
                }
                print(self.sectionsWithStatesDicr)
                
                var selectionsAry = [String]()
                for drinkscat in self.drinkCatModel
                {
                    print(drinkscat.name)
                    selectionsAry.append(drinkscat.name!)
                }
                
                
                for i in selectionsAry {
                    self.signatureSelectedCategoryDict[i] = "false"
                    
                    self.signatureSelectedCategoryDict[i] = "false"
                }
                print(self.signatureSelectedCategoryDict)
                
                
                self.categoryDropdown.optionArray = drinksAry
                if self.drinkAry != nil
                {
                self.signatureDrinkCategory.text = drinksAry[0]
                
                }
                    self.selectionTableView.reloadData()
           
                self.selectionTableView.delegate = self
                self.selectionTableView.dataSource = self
                
            }
            
            
        })
        SVProgressHUD.dismiss()
    }
    // Edit Image
    func addImage() {
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        let takePic = UIAlertAction(title: "Take Photo", style: .default,handler: {
            (alert: UIAlertAction!) -> Void in
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = UIImagePickerController.SourceType.camera
            self.present(myPickerController, animated: true, completion: nil)
            
        })
        
        let choseAction = UIAlertAction(title: "Choose from Library",style: .default,handler: {
            (alert: UIAlertAction!) -> Void in
            
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(myPickerController, animated: true, completion: nil)
            
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        optionMenu.addAction(takePic)
        optionMenu.addAction(choseAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            else
        {
            return
        }
        
        self.dismiss(animated: false, completion: { [weak self] in
            self?.signatureDrinkPic.image = originalImage
            self?.moveToImageCropper(image: originalImage)
        })
    }
}
// Crop Profile Image
extension DrinkSelection2VC : CropViewControllerDelegate {
    
    private func moveToImageCropper(image: UIImage) {
        let cropController = CropViewController(croppingStyle: CropViewCroppingStyle.default, image: image)
        cropController.delegate = self
        cropController.aspectRatioPickerButtonHidden = true
        cropController.aspectRatioLockEnabled = true
        cropController.aspectRatioPreset = .presetSquare
        self.present(cropController, animated: true, completion: nil)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        self.signatureDrinkPic.contentMode = .scaleAspectFill
        self.signatureDrinkPic.image = image
        
        // let image = UIImage()
        let compressData = image.jpegData(compressionQuality: 0.75)
        self.compressedImage = UIImage(data: compressData!)!
        cropViewController.dismiss(animated: true, completion: nil)
    }
    
    
}
class DrinkSelectionCell2: UITableViewCell {
    @IBOutlet weak var drinkPriceView: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var dropDown: UIButton!
    @IBOutlet weak var drinkPriceLbl: UITextField!
    @IBOutlet weak var selectionImg: UIButton!
    @IBOutlet weak var selectionLeadingConst: NSLayoutConstraint!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var drinkPrice2: UITextField!
    @IBOutlet weak var drinkPriceView2: UIView!
    
}

class DrinkSelectionCell3: UITableViewCell {
    @IBOutlet weak var drinkPriceView: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var dropDown: UIButton!
    @IBOutlet weak var drinkPriceLbl: UITextField!
    @IBOutlet weak var selectionImg: UIButton!
    @IBOutlet weak var selectionLeadingConst: NSLayoutConstraint!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var drinkprice2: UITextField!
    @IBOutlet weak var drinkPriceview2: UIView!
    
}
