//
//  HomeVC.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 18/02/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {
    
    
    

    @IBOutlet weak var entranceLabel: UILabel!
    @IBOutlet weak var ordersLabel: UILabel!
    @IBOutlet weak var doorpersonLabel: UILabel!
    @IBOutlet weak var bartenderLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        getUserDetails()
        self.getHomeData()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.barTintColor = UIColor.clear
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        // Do any additional setup after loading the view.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func notificationBtn(_ sender: Any) {
        let destinationvc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(destinationvc, animated: true)
    }
    
    func getUserDetails() {
        let userId = Constants.kUserDefaults.value(forKey: appConstants.userId) as? String
        UserAPI().getAllUserData(userId: userId!,pageNo: 0) { (data, error) in
            if data[APIConstants.isSuccess.rawValue] as! Bool == true {
                let userDict = data["data"]
                let dataProfile = NSKeyedArchiver.archivedData(withRootObject: userDict)
                //    Constants.kUserDefaults.set(dataProfile, forKey: appConstants.profile)
                
                let firstName = userDict!["firstName"] as! String
                let lastName = userDict!["lastName"] as! String
                let email = userDict?["email"] as! String
                let phone = userDict?["phone"] as? String
                let gender = userDict?["gender"] as! String
                 let imgUrl = userDict?["imgUrl"] as? String
                //  let email = userDict?["entrance"] as! String
                
                
                
                // print(usersGivenName, username, role, gender, phone)
                if firstName != nil {
                    //   Constants.kUserDefaults.set(townvar, forKey: appConstants.town)
                    //  var phoneString = String(phone)
                    
                    
                    Constants.kUserDefaults.set(firstName, forKey: "firstName")
                  Constants.kUserDefaults.set(lastName, forKey: "lastName")
                   Constants.kUserDefaults.set(email, forKey: "email")
                    Constants.kUserDefaults.set(imgUrl, forKey: "imgUrl")
                }
            }
            else{
                print("Getting Error")
            }
        }
    }
    // Get Home Data
    func getHomeData() {
        let userId = Constants.kUserDefaults.value(forKey: appConstants.userId) as? String
        UserAPI().getHomeData(userId: userId!,pageNo: 0) { (data, error) in
            if data[APIConstants.isSuccess.rawValue] as! Bool == true {
                let userDict = data["data"]
                let dataProfile = NSKeyedArchiver.archivedData(withRootObject: userDict)
                //    Constants.kUserDefaults.set(dataProfile, forKey: appConstants.profile)
                
                let bartenderCount = userDict!["bartenderCount"] as! Int
                let doorPersonCount = userDict!["doorPersonCount"] as! Int
                let entrance = userDict?["entrance"] as! Int
                let serveOrder = userDict?["serveOrder"] as! Int
               // print(usersGivenName, username, role, gender, phone)
                if bartenderCount != nil {
                    //   Constants.kUserDefaults.set(townvar, forKey: appConstants.town)
                    var bartenderCountString = String(bartenderCount)
                    var doorPersonCountString = String(doorPersonCount)
                    
                    var entranceString = String(entrance)
                    var serveOrderString = String(serveOrder)
                    
                    self.bartenderLabel.text = bartenderCountString
                    self.doorpersonLabel.text = doorPersonCountString
                    
                    self.ordersLabel.text = serveOrderString
                    self.entranceLabel.text = entranceString
                    
                }
                let lgaVar = userDict?["lga"] as? String
                if lgaVar != nil {
                    Constants.kUserDefaults.set(lgaVar, forKey: appConstants.lga)
                }
                
                // self.NameVar = usersGivenName as? String
                //   self.imgUrlVar = imgUrl as? String
                
                let myUser = User.init(dictionary: userDict as! NSDictionary)
                //  self.updateInputParams(user: myUser!)
                
                
                
                
            }
            else{
                print("Getting Error")
                
            }
            
        }
    }
    
    
}
