//
//  UserRemoteReplicator.swift
//  AQUA
//
//  Created by Krishna on 05/04/17.
//  Copyright © 2017 MindfulSas. All rights reserved.
//

import Foundation

    
    class UserRemoteReplicator{
        
        //MARK:- API constants
        private let signUp = "users/signup"
        private let signIn = "users/signin"
        private let update = "users/"
        private let verify = "users/verify"
        private let create = "users/edit/profile/"
        private let createfolder = "folders"
        private let createTask = "tasks"
        private let createCategory = "categories"
        private let amount = "amounts"
        private let todaystask = "tasks?sort=today"
        private let taskByFolderId =  "tasks?folderId="
        private let allTasks = "tasks"
        private let updateTaskStatus = "updateStatus/"
        private let forgotPassword = "users/forgetPassword"
        private let qrcode = "covers/qr"
        private let manualCode = "covers/verify"
        private let contactUs = "contacts"
        private let updateUserProfile = "users/edit/profile/"
        private let bars = "bars/"
        var timeModelAry = [NSDictionary]()
        var timeAry = [String]()
        var frequencyAry = [Int]()
        
        private let baseUrl1 = remoteConfig.mBaseUrl
        private var remoteRepo:RemoteRepository!
        
        init(){
            self.remoteRepo = RemoteRepository()
        }
        
        // MARK: Check COde Manually
        func checkManually(userDetials: Dictionary<String, AnyObject>, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
            let urlString =  "\(baseUrl1)\(manualCode.html)"
            remoteRepo.remotePOSTServiceWithParameters(urlString: urlString, params: userDetials) { (data, error) -> Void in
                callback(data , error )
                
            }
        }
        
        // MARK: QrCode Scan
        func scanQrcode(userDetials: Dictionary<String, AnyObject>, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
            let urlString =  "\(baseUrl1)\(qrcode.html)"
            remoteRepo.remotePOSTServiceWithParameters(urlString: urlString, params: userDetials) { (data, error) -> Void in
                callback(data , error )
                
            }
        }
        
        // MARK: SignIn With Email
        func userSignIn(userDetials: Dictionary<String, AnyObject>, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
            let urlString =  "\(baseUrl1)\(signIn.html)"
            remoteRepo.remotePOSTServiceWithParameters(urlString: urlString, params: userDetials) { (data, error) -> Void in
                callback(data , error )
                
            }
        }
        
        // MARK: Contact Us
        func UserContactUs(userDetials: Dictionary<String, AnyObject>, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
            let urlString =  "\(baseUrl1)\(contactUs.html)"
            remoteRepo.remotePOSTWithHeaderServiceWithParameters(urlString: urlString, params: userDetials) { (data, error) -> Void in
                callback(data , error?.description )
                
            }
        }
        
        //MARK: VarificationCode With Email
        func userVarificationCode(userDetials: Dictionary<String, AnyObject>, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
            
            let urlString =  "\(baseUrl1)\(verify.html)"
            
            remoteRepo.remotePOSTServiceWithParameters(urlString: urlString, params: userDetials as Dictionary<String, AnyObject>) { (data, error) -> Void in
                callback(data , error )
            }
        }
        
        
        
        
        // MARK: SignUp With Email
        func userSignUp(userDetials: Dictionary<String, AnyObject>, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
            let urlString =  "\(baseUrl1)\(signUp.html)"
            remoteRepo.remotePOSTServiceWithParameters(urlString: urlString, params: userDetials) { (data, error) -> Void in
                callback(data , error )
                
            }
        }
        // New User Accounts
        func newUserAccounts(userDetials: Dictionary<String, AnyObject>,userID:String, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
            print(userDetials)
            let urlString =  "\(baseUrl1)\(create.html)" + userID
            remoteRepo.remotePUTServiceWithParameters(urlString: urlString, params: userDetials) { (data, error) -> Void in
                //              if data != nil{
                //                if data![APIConstants.data.rawValue] != nil{
                //                  let profileData = NSKeyedArchiver.archivedData(withRootObject:data![APIConstants.data.rawValue])
                //                  Constants.kUserDefaults.set(profileData, forKey: appConstants.profileData.rawValue)
                //                }
                //              }
                
                
                
                callback(data , error?.description )
                
                
                
            }
        }
        
        
        // MARK: Forget Password
        func forgotPassword(userDetials: Dictionary<String, AnyObject>, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
            let urlString =  "\(baseUrl1)\(forgotPassword.html)"
            remoteRepo.remotePOSTServiceWithParameters(urlString: urlString, params: userDetials) { (data, error) -> Void in
                callback(data , error )
                
            }
        }
        
        // MARK: Create new user accounts
        func createNewuserAccount(userDetials: Dictionary<String, AnyObject>,userID:String, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
            print(userDetials)
            let urlString =  "\(baseUrl1)\(signUp.html)"
            remoteRepo.remotePOSTWithHeaderServiceWithParameters(urlString: urlString, params: userDetials) { (data, error) -> Void in
                
                callback(data , error?.description )
                
            }
        }
        func getAllUsers1(type:String = "",callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
            
            let url = "users/search/three" + type
            
            
            let urlString =  "\(baseUrl1)\(url.html)"
            
            remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
                callback(data, error)
            }
        }
        // MARK: Update Profile
        func userUpdateProfile(userDetials: Dictionary<String, AnyObject>,userID:String, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
            print("Values of user are\(userDetials.values)")
            print(userDetials)
            let urlString =  "\(baseUrl1)\(create.html)" + userID
            remoteRepo.remotePUTServiceWithParameters(urlString: urlString, params: userDetials) { (data, error) -> Void in
                callback(data , error?.description )
            }
        }


        func barUpdateee(userDetials: Dictionary<String, AnyObject>,userID:String, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
            let barId = Constants.kUserDefaults.string(forKey: "barId")!
            let urlString = remoteConfig.mBaseUrl + "bars/" + barId
            print("Values of user are\(userDetials.values)")
            print(userDetials)
        //    let urlString =  "\(baseUrl1)\(create.html)" + userID
            remoteRepo.remotePUTServiceWithParameters(urlString: urlString, params: userDetials) { (data, error) -> Void in
                callback(data , error?.description )
            }
        }
        
        // MARK: New Event
        func createNewEvent(userDetials: Dictionary<String, AnyObject>,userID:String, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
            print("Values of user are\(userDetials.values)")
         
            let url = "events"
            let urlString =  "\(baseUrl1)\(url.html)"
            remoteRepo.remotePOSTWithHeaderServiceWithParameters(urlString: urlString, params: userDetials) { (data, error) -> Void in
                callback(data , error?.description )
            }
        }
    
        func UpdateEvent(userDetials: Dictionary<String, AnyObject>,userID:String, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
            print("Values of user are\(userDetials.values)")
            let evtId = Constants.kUserDefaults.string(forKey: "currentEventId")
            print(evtId)
            let url = "events/" + evtId!
            let urlString =  "\(baseUrl1)\(url.html)"
            remoteRepo.remotePUTServiceWithParameters(urlString: urlString, params: userDetials) { (data, error) -> Void in
                callback(data , error?.description )
            }
        }
        
        // MARK: Update Profile With current user Id
        func userUpdateProfileCurUserID(userDetials: Dictionary<String, AnyObject>,userID:String, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
           var usrId = Constants.kUserDefaults.string(forKey: "currentUserId")
            print(userDetials)
            let urlString =  "\(baseUrl1)\(updateUserProfile.html)" + usrId!
            remoteRepo.remotePUTServiceWithParameters(urlString: urlString, params: userDetials) { (data, error) -> Void in
                callback(data , error?.description )
            }
        }
        func paymntUpdate(userDetials: Dictionary<String, AnyObject>,userID:String, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
           
            let urlString =  "\(baseUrl1)userPaymants/addBankDetails"
            remoteRepo.remotePOSTWithHeaderServiceWithParameters(urlString: urlString, params: userDetials) { (data, error) -> Void in
                callback(data , error?.description )
            }
        }
        
        
        // MARK: get bar id
        func getbaridd(userDetials: Dictionary<String, AnyObject>, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
            
            print("Values of user are\(userDetials.values)")
              let barid = Constants.kUserDefaults.string(forKey: appConstants.barId)
              print(barid)
            let urlString =  "\(baseUrl1)\(bars.html)" + barid!
            remoteRepo.remotePUTServiceWithParameters(urlString: urlString, params: userDetials) { (data, error) -> Void in
                callback(data , error?.description )
            }
        }
        
        // MARK: Update Profile userUpdateProfileWhenForgotPassword
        func userUpdateProfileWhenForgotPassword(userDetials: Dictionary<String, AnyObject>,userID:String, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
            let urlString =  "\(baseUrl1)\(create.html)" + userID
            remoteRepo.remotePUTServiceWithParameters(urlString: urlString, params: userDetials) { (data, error) -> Void in
                callback(data , error?.description )
                
            }
        }
        
        func getAllUsers(type:String = "",callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
            
            let url = "users/search/three" + type
            
            
            let urlString =  "\(baseUrl1)\(url.html)"
            
            remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
                callback(data, error)
            }
        }
        
        
        //MARK:- Get State
        func getState(query:String = "" ,pageNo:Int = 1 ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
            
            let url = "states?countryId=" + "\(query)"
            
            
            let urlString =  "\(baseUrl1)\(url.html)"
            
            remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
                callback(data, error)
            }
        }
        
        
        //MARK:- Get Towns of South Africa
        func getTowns(query:String = "" ,pageNo:Int = 1 ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
            
            let url = "zipCodes"
            
            
            let urlString =  "\(baseUrl1)\(url.html)"
            
            remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
                callback(data, error)
            }
        }
        
        
        //MARK:- Get LGA by State ID
        func getLGAByStateId(query:String = "" ,pageNo:Int = 1 ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
            
            let url = "lgas?stateId=" + "\(query)"
            
            
            let urlString =  "\(baseUrl1)\(url.html)"
            
            remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
                callback(data, error)
            }
        }
        
        
        
        //MARK:- Get LGA by Town ID
        func getLGAByTownId(query:String = "" ,pageNo:Int = 1 ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
            
            let url = "regions?zipCodeId=" + "\(query)"
            
            
            let urlString =  "\(baseUrl1)\(url.html)"
            
            remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
                callback(data, error)
            }
        }
        
        
        
        
        //MARK:- Get User Details
                func getHomeDetails(query:String = "" ,pageNo:Int = 1 ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
        
                    let url = "users/count/home/"
        
        
                    let urlString =  "\(baseUrl1)\(url.html)"
                    print(urlString)
                    remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
                        callback(data, error)
                    }
                }
        func getbaselineDetails(query:String = "" ,pageNo:Int = 1 ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
            
            let url = "categories"
            
            
            let urlString =  "\(baseUrl1)\(url.html)"
            print(urlString)
            remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
                callback(data, error)
            }
        }
        //  selectionCategories/searchdignstutre/drink
        func getsignatureDrink(query:String = "" ,pageNo:Int = 1 ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
            
            let url = "selectionCategories/searchdignstutre/drink"
            
            
            let urlString =  "\(baseUrl1)\(url.html)"
            print(urlString)
            remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
                callback(data, error)
            }
        }
        
        //MARK:- Get All User Data
        func getAllUserAPi(query:String = "" ,pageNo:Int = 1 ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
            let url = "users/" + "\(query)"
            
            
            let urlString =  "\(baseUrl1)\(url.html)"
            
            remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
                callback(data, error)
            }
        }
       
        //MARK:- Get All User Data
     
        func getbarAPi(query:String = "" ,pageNo:Int = 1 ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
            print(appConstants.barId)
            let url = "bars/" + "\(query)"
            
            
            let urlString =  "\(baseUrl1)\(url.html)"
            
            remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
                callback(data, error)
            }
        }
        func getcoverAPi(query:String = "" ,pageNo:Int = 1 ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
            print(appConstants.barId)
            let url = "bars/" + "\(query)"
            
            
            let urlString =  "\(baseUrl1)\(url.html)"
            
            remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
                callback(data, error)
            }
        }
        
        //MARK:- Get events
        func getEvents(query:String = "" ,pageNo:Int = 1 ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
            let url = "events"
            
            
            let urlString =  "\(baseUrl1)\(url.html)"
            
            remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
                callback(data, error)
            }
        }
        //MARK:- Get events
        func getEventsDetails(query:String = "" ,pageNo:Int = 1 ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
            let url = "events/"
            let evtId = Constants.kUserDefaults.string(forKey: "currentEventId")
            print(evtId)
            
            let urlString =  "\(baseUrl1)\(url.html)" + evtId!
            print(urlString)
            
            remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
                callback(data, error)
            }
        }
        
   

        
        
        //MARK:- Get All User Data
        func fetchUserId(query:String = "" ,pageNo:Int = 1 ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
            let currentUserId = Constants.kUserDefaults.string(forKey: "currentUserId")
            print(currentUserId)
            let url = "users/" + "\(currentUserId!)"
            
            
            let urlString =  "\(baseUrl1)\(url.html)"
            
            remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
                callback(data, error)
            }
        }
        
        func fetchPaymentInf(query:String = "" ,pageNo:Int = 1 ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
            let currentUserId = Constants.kUserDefaults.string(forKey: "currentUserId")
            print(currentUserId)
            let url = "userPaymants/Bank/Show"
            
            
            let urlString =  "\(baseUrl1)\(url.html)"
            print(urlString)
            remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
                callback(data, error)
            }
        }
        
        //MARK:- Get All Bartender & Doorperson
        func getbartenderDoorperson(query:String = "" ,pageNo:Int = 1 ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
            
            let url = "users/searchDoorAndBart/current/"
            
            
            let urlString =  "\(baseUrl1)\(url.html)"
            
            remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
                callback(data, error)
            }
        }
        
        //MARK:- AverageStatApi
        func AverageStatApi(query:String = "" ,pageNo:Int = 1 ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
            
            let url = "covers/varifyAverage/daily"
            
            
            let urlString =  "\(baseUrl1)\(url.html)"
            
            remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
                callback(data, error)
            }
        }
      
        //MARK:- FrequencyStatApi
        func ChartApii(query:String = "" ,pageNo:Int = 1 ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
            let eventId =  Constants.kUserDefaults.string(forKey: "currentEventId")
            print("Current EVent Id is - \(eventId)")
            let url = "events/only/one/" + eventId!
            
            
            let urlString =  "\(baseUrl1)\(url.html)"
            
            remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
                callback(data, error)
                

            }
        }
      
      //MARK:- Get User Details
      func getAllUsers(query:String = "" ,pageNo:Int = 1 ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
        
        let url = "users"
        
        
        let urlString =  "\(baseUrl1)\(url.html)"
        
        remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
          callback(data, error)
        }
      }
      
        
        
        
        
        
        
        //        // MARK: createCategory
        //        func createCategory(categoryDetials: Dictionary<String, AnyObject>, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
        //            let urlString =  "\(baseUrl1)\(createCategory.html)"
        //            remoteRepo.remotePOSTServiceWithParameters(urlString: urlString, params: categoryDetials) { (data, error) -> Void in
        //                callback(data , error )
        //
        //            }
        //        }
        
        
        
        
        
//        // MARK: createFolder
//        func createFolder(folderDetials: Dictionary<String, AnyObject>, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
//            let urlString =  "\(baseUrl1)\(createfolder.html)"
//            remoteRepo.remotePOSTServiceWithParameters(urlString: urlString, params: folderDetials) { (data, error) -> Void in
//                callback(data , error )
//
//            }
//        }
//
//        //MARK:- Get All Folder
//        func getAllFolder(query:String = "" ,pageNo:Int = 1 ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
//
//            let url = "folders"
//
//
//            let urlString =  "\(baseUrl1)\(url.html)"
//
//            remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
//                callback(data, error)
//            }
//        }
//
//
//        // MARK: createTask
//        func createTask(taskDetials: Dictionary<String, AnyObject>, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
//            let urlString =  "\(baseUrl1)\(createTask.html)"
//            remoteRepo.remotePOSTServiceWithParameters(urlString: urlString, params: taskDetials) { (data, error) -> Void in
//                callback(data , error )
//
//            }
//        }
//
//
//
//        //MARK:- Get Todays Task
//        func getTodaysTask(query:String = "" ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
//            let url = todaystask
//
//            let urlString =  "\(baseUrl1)\(url.html)"
//
//            remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
//                callback(data, error)
//            }
//        }
//
//
//        //MARK:- Get Task By Folder Id
//        func getTasksByFolderId(query:String = "" ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
//            let url = taskByFolderId + "\(query)"
//
//            let urlString =  "\(baseUrl1)\(url.html)"
//
//            remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
//                callback(data, error)
//            }
//        }
//
//
//        //MARK:- Get All Tasks
//        func getAllTasks(query:String = "" ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
//            let url = allTasks
//
//            let urlString =  "\(baseUrl1)\(url.html)"
//
//            remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
//                callback(data, error)
//            }
//        }
//
//
//
//        // MARK: updateTaskStatus
//        func updateTaskStatus(taskId:String,routeID:String?, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
//            let urlString =  "\(baseUrl1)\(updateTaskStatus.html)\(taskId)"
//            var dict = ["status":"completed"]
//            if routeID != nil {
//                dict["routeID"] = routeID!
//            }
//
//            remoteRepo.remotePOSTServiceWithParameters(urlString: urlString, params: dict as Dictionary<String, AnyObject>) { (data, error) -> Void in
//                callback(data , error )
//
//            }
//        }
//
//
//        // MARK: SpentAmount
//        func createAmount(amountDetials: Dictionary<String, AnyObject>, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
//            let urlString =  "\(baseUrl1)\(amount.html)"
//            remoteRepo.remotePOSTServiceWithParameters(urlString: urlString, params: amountDetials) { (data, error) -> Void in
//                callback(data , error )
//
//            }
//        }
        
        
        //
        //    //MARK: Resend Code
        //    func resendVarificationCode(userDetials: Dictionary<String, String>, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void) {
        //        let url =  "\(baseUrl1)\(resendCode.html)"
        //        remoteRepo.remotePOSTServiceWithParameters(urlString: url, params: userDetials as Dictionary<String, AnyObject>) { (data, error) -> Void in
        //            callback(data , error )
        //        }
        //    }
        //
        //
        //
        //    // MARK: SignUp With Device Id
        //    func userSignUp(userDetials: Dictionary<String, AnyObject>, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
        //        let url =  "\(baseUrl1)\(signUp.html)"
        //
        //        remoteRepo.remotePOSTServiceWithParameters(urlString: url, params: userDetials) { (data, error) -> Void in
        //            callback(data , error )
        //
        //        }
        //    }
        //
        //
        //
        //
        //
        //    //MARK: Logout User
        //    func logoutUserWithCompletion(callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
        //            let logout : String = self.logout
        //        remoteRepo.remotePOSTServiceWithParameters(urlString: logout, params: [:]) { (data, error) -> Void in
        //            callback(data , error )
        //        }
        //
        //    }
        //
        //    //MARK: Report a bug on Contact us
        //
        //    func reportBug(detail:Dictionary<String,AnyObject>, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
        //        let url = "\(baseUrl2)\(reportBug)"
        //        remoteRepo.remotePOSTServiceWithParameters(urlString: url, params: detail) { (data, error) -> Void in
        //            callback(data , error )
        //        }
        //
        //    }
        
        
        
    }
    
    


