//
//  PostRemoteReplicator.swift
//  SearchApp
//
//  Created by dEEEP on 14/03/18.
//  Copyright © 2018 vannteqfarm. All rights reserved.
//

import Foundation


class PostRemoteReplicator{
  
  //MARK:- API constants
  private let createPost = "posts"
    private let getPosts = "posts"
    private let deletePost = "posts/"
    private let createLike = "posts/favourite/"
    private let createDisLike = "posts/unfavourite"
     private let createComments = "comments"
     private let deleteComment = "comments/"
    private let requestIdentity = "postUsers/identity/request/"
    private let createUnSaved = "posts/unsaved/"
  private let comments = "comments/post"
   private let likes = "favourites/post"
  private let ratings = "ratings/post"
    private let getAllComments = "comments?postId="
    private let getReactedUsers = "posts/likes"
   //private let content = "commentContents"
    private let baseUrl1 = remoteConfig.mBaseUrl
  private var remoteRepo:RemoteRepository!
  
  init(){
    self.remoteRepo = RemoteRepository()
  }
  
  
  
    // MARK: create Post
    func createPost(postDetials: Dictionary<String, AnyObject>, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
        let urlString =  "\(baseUrl1)\(createPost.html)"
        remoteRepo.remotePOSTServiceWithParameters(urlString: urlString, params: postDetials) { (data, error) -> Void in
            callback(data , error )
            
        }
    }
  
  
  // MARK: update Post
  func updatePost(postDetials: Dictionary<String, AnyObject>,postID:String, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
    let urlString =  "\(baseUrl1)\(deletePost.html)" + postID
    remoteRepo.remotePUTServiceWithParameters(urlString: urlString, params: postDetials) { (data, error) -> Void in
      callback(data , error?.description )
      
    }
  }
//    
//    
    //MARK: Delete Comment
    func deletePost(PostID:String, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
        let urlString =  "\(baseUrl1)\(deletePost.html)" + PostID
        remoteRepo.remoteDELETEServiceWithParameters(urlString: urlString, params: [:] as Dictionary<String, AnyObject>) { (data, error) -> Void in
            callback(data , error?.description )
          
        }
    }
//
//    
//    
    //MARK:- Get All Categories
    func getAllCategories(query:String = "" ,pageNo:Int = 1 ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
        
        var url = "categories"
        
        if query != ""{
            url = url + query
        }
        
        
        let urlString =  "\(baseUrl1)\(url.html)"
        
        remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
            callback(data, error)
        }
    }

    
    //MARK:- Get All Posts
    func getAllPosts(query:String = "" ,pageNo:Int = 1 ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
        
        var url = "posts"
        
        if query != ""{
            url = url + query
        }
        
        
        let urlString =  "\(baseUrl1)\(url.html)"
        
        remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
            callback(data, error)
        }
    }
    
    
//    
//    
//    //MARK:- Get Post By ID
    func getPostDetailsByID(query:String = "" ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
        let url = getPosts + "/\(query)"
        
        let urlString =  "\(baseUrl1)\(url.html)"
        
        remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
            callback(data, error)
        }
    }
    
//
//    //MARK:- Get Post Comments By ID
//    func getPostCommentsByID(postId:String = "" ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
//        let url = getAllComments
//        
//        let urlString =  "\(baseUrl1)\(url.html)" + postId
//        
//        remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
//            callback(data, error)
//        }
//    }
//    
//    
//
  
  
  func getAllNotifications(callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
    let url = "notifications"
    
    let urlString =  "\(baseUrl1)\(url.html)"
    
    remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
      callback(data, error)
    }
  }
  
     //MARK: Like post
      func likePost(postID:String,postDetials: Dictionary<String, AnyObject>, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
        let urlString =  "\(baseUrl1)\(createLike.html)" + postID
        remoteRepo.remotePOSTServiceWithParameters(urlString: urlString, params: postDetials) { (data, error) -> Void in
          callback(data , error?.description )
    
        }
      }
  
  
  
    //MARK: Dislke post
    func dislikePost(postID:String, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
        let urlString =  "\(baseUrl1)\(createDisLike.html)/" + postID
        remoteRepo.remotePOSTServiceWithParameters(urlString: urlString, params: [:] as Dictionary<String, AnyObject>) { (data, error) -> Void in
            callback(data , error?.description )
          
        }
    }
  
  
  
//
//    //MARK: Create Comment
//    func createComment(postID:String,commentDetials: Dictionary<String, AnyObject>, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
//        let urlString =  "\(baseUrl1)\(createComments.html)"
//        remoteRepo.remotePOSTServiceWithParameters(urlString: urlString, params: commentDetials) { (data, error) -> Void in
//            callback(data , error?.description )
//            
//        }
//    }
//  
//    //MARK: Delete Comment
//    func deleteComment(commentID:String, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
//        let urlString =  "\(baseUrl1)\(deleteComment.html)" + commentID
//        remoteRepo.remoteDELETEServiceWithParameters(urlString: urlString, params: [:] as Dictionary<String, AnyObject>) { (data, error) -> Void in
//            callback(data , error?.description )
//            
//        }
//    }
//    
//    
    //MARK: Create Saved
    func requestIdentity(postID:String, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
        let urlString =  "\(baseUrl1)\(requestIdentity.html)" + postID
        remoteRepo.remotePOSTServiceWithParameters(urlString: urlString, params: [:] as Dictionary<String, AnyObject>) { (data, error) -> Void in
            callback(data , error?.description )
          
        }
    }
//
//    
//    //MARK: Create UnSaved
//    func createUnSaved(postID:String, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
//        let urlString =  "\(baseUrl1)\(createUnSaved.html)" + postID
//        remoteRepo.remotePOSTServiceWithParameters(urlString: urlString, params: [:] as Dictionary<String, AnyObject>) { (data, error) -> Void in
//            callback(data , error?.description )
//            
//        }
//    }
//    
//    
//    
//    
//    
//    //MARK:- Get Reacted Users By ID
//    func getReactedUsersByID(query:String = "" ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
//        let url = getReactedUsers + "/\(query)"
//        let urlString =  "\(baseUrl1)\(url.html)"
//        remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
//            callback(data, error)
//        }
//    }
  
//  //MARK:- Get Post By ID
//  func getPostByID(query:String = "" ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
//    let url = getPost + "/\(query)"
//
//    let urlString =  "\(baseUrl1)\(url.html)"
//
//    remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
//      callback(data, error)
//    }
//  }
//  
//  
//  func getAllCommentsForPost(query:String = "" ,pageNo:Int = 1 ,callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
//    let url = comments + "?entityId=\(query)" + "&pageNo=" + "\(pageNo)"
//    
//    let urlString =  "\(baseUrl1)\(url.html)"
//    
//    remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
//      callback(data, error)
//    }
//  }
//  
//  
//  func getCommentsCensored(callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
//    let url = content
//    
//    let urlString =  "\(baseUrl1)\(url.html)"
//    
//    remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
//      callback(data, error)
//    }
//  }
//  
//  
//  // MARK: Post Comment
//  func createCommentInPost(commentDetials: [String:String], callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
//    let urlString =  "\(baseUrl1)\(comments.html)"
//   
//    remoteRepo.remotePOSTServiceWithParameters(urlString: urlString, params: commentDetials as Dictionary<String, AnyObject>) { (data, error) -> Void in
//      callback(data , error )
//      
//    }
//  }
//  
//  
//  // MARK: like Dislike post
//  func likeDislikepost(commentDetials: [String:String], callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
//    let urlString =  "\(baseUrl1)\(likes.html)"
//    
//    remoteRepo.remotePOSTServiceWithParameters(urlString: urlString, params: commentDetials as Dictionary<String, AnyObject>) { (data, error) -> Void in
//      callback(data , error )
//      
//    }
//  }
//  
//  // MARK: dislike post
//  func dislikepost(postID:String, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
//    let urlString =  "\(baseUrl1)\(likes.html)/" + postID
//    remoteRepo.remoteDELETEServiceWithParameters(urlString: urlString, params: [:] as Dictionary<String, AnyObject>) { (data, error) -> Void in
//      callback(data , error?.description )
//      
//    }
//  }
//  
//  
//  // MARK: rating post
//  func ratingpost(commentDetials: [String:String], callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
//    let urlString =  "\(baseUrl1)\(ratings.html)"
//    
//    remoteRepo.remotePOSTServiceWithParameters(urlString: urlString, params: commentDetials as Dictionary<String, AnyObject>) { (data, error) -> Void in
//      callback(data , error )
//      
//    }
//  }
  
  
  
  
  

  
  
  
}

