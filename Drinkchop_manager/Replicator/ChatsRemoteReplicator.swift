//
//  ChatsRemoteReplicator.swift
//  DrinkchopBartender
//
//  Created by Gurpreet Gulati on 15/05/19.
//  Copyright © 2019 Temp. All rights reserved.
//

import Foundation
class ChatsRemoteReplicator{
    
    //MARK:- API constants
  
    private let baseUrl1 = remoteConfig.mBaseUrl
    private var remoteRepo:RemoteRepository!
    private var Chat  = "chats"
     private let setUnreadCountToZero = "chats/setZeroUnreadCount/"
     private let incUnreadCounts = "chats/incUnreadCount/"
    
    init(){
        self.remoteRepo = RemoteRepository()
    }
    
    
    
    //MARK:- Get chat list
    func getchatlist(callback:@escaping (_ responseData: Dictionary<String, AnyObject>?, _ error: NSError?) -> Void ) {
        let url = "chats"
        let urlString =  "\(baseUrl1)\(url.html)"
        
        remoteRepo.remoteGETService(urlString: urlString) { (data, error) -> Void in
            callback(data, error)
        }
    }
    
    
    // MARK: Create Chat and Get Chat Id
    func createChatAndGetID(chatDetials: [String:Any], callback:@escaping (_ responsedata: Dictionary<String, Any>?, _ error: String? ) -> Void)   {
        let urlString =  "\(baseUrl1)\(Chat.html)"
        remoteRepo.remotePOSTServiceWithParameters(urlString: urlString, params: chatDetials as Dictionary<String, Any> as Dictionary<String, AnyObject>) { (data, error) -> Void in
            callback(data , error )
            
        }
    }
    
    // MARK: Set Unread Count to Zero
    func setUnreadCountToZero(detailsOfChat: Dictionary<String, AnyObject>,chatID:String, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
        let urlString =  "\(baseUrl1)\(setUnreadCountToZero.html)" + chatID
        remoteRepo.remotePUTServiceWithParameters(urlString: urlString, params: detailsOfChat) { (data, error) -> Void in
            callback(data , error?.description )
        }
    }
    
    
    // MARK: Set Unread Count to Zero
    func incrementUnreadCount(detailsOfChat: Dictionary<String, AnyObject>,chatID:String, callback:@escaping (_ responsedata: Dictionary<String, AnyObject>?, _ error: String? ) -> Void)   {
        let urlString =  "\(baseUrl1)\(incUnreadCounts.html)" + chatID
        remoteRepo.remotePUTServiceWithParameters(urlString: urlString, params: detailsOfChat) { (data, error) -> Void in
            callback(data , error?.description )
        }
    }
    
    
    
    
    
    
    
    
    
    
}
