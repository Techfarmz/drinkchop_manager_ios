//
//  Card.swift
//  drinkchop-ios
//
//  Created by Tech Farmerz on 16/02/19.
//  Copyright © 2019 Gurpreet Singh. All rights reserved.
//

import Foundation

public class ImportedDrinks {
    
    public var alcoholPercentage : String?
    public var id: String?
    public var description: String?
    public var imgUrl: String?
    public var name: String?
    public var price: String?
    public var quantity: String?

    
    public var userInfo: NSDictionary?
    
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [ImportedDrinks]
    {
        var models:[ImportedDrinks] = []
        for item in array
        {
            models.append(ImportedDrinks(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
        id = dictionary["id"] as? String
        alcoholPercentage = dictionary["alcoholPercentage"] as? String
        description = dictionary["description"] as? String
        imgUrl = dictionary["imgUrl"] as? String
        name = dictionary["name"] as? String
         price = dictionary["price"] as? String
        quantity = dictionary["quantity"] as? String
       // userInfo = dictionary["user"] as? NSDictionary
    }
    
    public func dictionaryRepresentation() -> NSDictionary {
        let dictionary = NSMutableDictionary()
        dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.alcoholPercentage, forKey: "alcoholPercentage")
        dictionary.setValue(self.description, forKey: "description")
        dictionary.setValue(self.imgUrl, forKey: "imgUrl")
        dictionary.setValue(self.name, forKey: "name")
         dictionary.setValue(self.price, forKey: "price")
        dictionary.setValue(self.quantity, forKey: "quantity")
        
        return dictionary
    }
}
