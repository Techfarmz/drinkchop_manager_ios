//
//  Card.swift
//  drinkchop-ios
//
//  Created by Tech Farmerz on 16/02/19.
//  Copyright © 2019 Gurpreet Singh. All rights reserved.
//

import Foundation

public class BarUpdate {
    
    public var id: String?
    public var availbilityStart: String?
    public var availbilityTill: String?
    public var barlike: String?
    public var city: String?
    public var country: String?
    public var coverFee: String?
    public var Starttime: String?
    public var coverstatus: String?
    public var destcription: String?
    public var drinkstatus: String?
    public var email: String?
    public var endTime: String?
    public var entry: String?
    public var favouriteCount: String?
    public var fbLink: String?
    public var imgUrl: String?
    public var instagramlink: String?
    public var isCover: String?
    public var likeCount: String?
    public var name: String?
    public var radius: String?
    public var rating: String?
    public var review: String?
    public var shareCount: String?
    public var state: String?
    public var status: String?
    public var tax: String?
    public var twitterLink: String?
    public var zipCode: String?
    public var location : Location?
    public var days: [Days]?

    
    public var userInfo: NSDictionary?
    
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [BarUpdate]
    {
        var models:[BarUpdate] = []
        for item in array
        {
            models.append(BarUpdate(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
        id = dictionary["id"] as? String
         availbilityStart = dictionary["availbilityStart"] as? String
        availbilityTill = dictionary["availbilityTill"] as? String
        barlike = dictionary["barlike"] as? String
        city = dictionary["city"] as? String
        country = dictionary["country"] as? String
        coverFee = dictionary["coverFee"] as? String
        Starttime = dictionary["Starttime"] as? String
        coverstatus = dictionary["coverstatus"] as? String
        destcription = dictionary["destcription"] as? String
        drinkstatus = dictionary["drinkstatus"] as? String
        email = dictionary["email"] as? String
        endTime = dictionary["endTime"] as? String
        entry = dictionary["entry"] as? String
        favouriteCount = dictionary["favouriteCount"] as? String
        fbLink = dictionary["fbLink"] as? String
        imgUrl = dictionary["imgUrl"] as? String
        instagramlink = dictionary["instagramlink"] as? String
        isCover = dictionary["isCover"] as? String
        likeCount = dictionary["likeCount"] as? String
        name = dictionary["name"] as? String
        radius = dictionary["radius"] as? String
        rating = dictionary["rating"] as? String
        review = dictionary["review"] as? String
        shareCount = dictionary["shareCount"] as? String
        status = dictionary["status"] as? String
        tax = dictionary["tax"] as? String
        twitterLink = dictionary["twitterLink"] as? String
        zipCode = dictionary["zipCode"] as? String
   
   if (dictionary["location"] != nil) { location = Location(dictionary: dictionary["location"] as! NSDictionary) }
        if (dictionary["days"] != nil) { days = Days.modelsFromDictionaryArray(array: dictionary["days"] as! NSArray)
            // userInfo = dictionary["user"] as? NSDictionary
        } }
    
     func dictionaryRepresentation() -> NSDictionary {
    
        
        let dictionary = NSMutableDictionary()
        dictionary.setValue(self.id, forKey: "id")
           dictionary.setValue(self.availbilityStart, forKey: "availbilityStart")
        dictionary.setValue(self.availbilityTill, forKey: "availbilityTill")
        dictionary.setValue(self.barlike, forKey: "barlike")
        dictionary.setValue(self.city, forKey: "city")
        dictionary.setValue(self.country, forKey: "country")
        dictionary.setValue(self.coverFee, forKey: "coverFee")
        dictionary.setValue(self.Starttime, forKey: "Starttime")
        dictionary.setValue(self.coverstatus, forKey: "coverstatus")
        dictionary.setValue(self.destcription, forKey: "destcription")
        dictionary.setValue(self.drinkstatus, forKey: "drinkstatus")
        dictionary.setValue(self.email, forKey: "email")
        dictionary.setValue(self.endTime, forKey: "endTime")
        dictionary.setValue(self.entry, forKey: "entry")
        dictionary.setValue(self.favouriteCount, forKey: "favouriteCount")
        dictionary.setValue(self.fbLink, forKey: "fbLink")
        dictionary.setValue(self.imgUrl, forKey: "imgUrl")
        dictionary.setValue(self.instagramlink, forKey: "instagramlink")
        dictionary.setValue(self.isCover, forKey: "isCover")
        dictionary.setValue(self.likeCount, forKey: "likeCount")
        dictionary.setValue(self.name, forKey: "name")
        dictionary.setValue(self.radius, forKey: "radius")
        dictionary.setValue(self.rating, forKey: "rating")
        dictionary.setValue(self.review, forKey: "review")
        dictionary.setValue(self.shareCount, forKey: "shareCount")
        dictionary.setValue(self.status, forKey: "status")
        dictionary.setValue(self.tax, forKey: "tax")
        dictionary.setValue(self.twitterLink, forKey: "twitterLink")
        dictionary.setValue(self.zipCode, forKey: "zipCode")

        dictionary.setValue(self.location?.dictionaryRepresentation(), forKey: "location")
        
        
        
        var arr = [NSDictionary]()
        if self.days != nil{
            for item in self.days!{
                arr.append(item.dictionaryRepresentation())
            }
            dictionary.setValue(arr, forKey: "days")
        }
        
        
        return dictionary
    }
}


