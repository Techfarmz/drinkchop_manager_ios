//
//  Participantss.swift
//  DrinkchopBartender
//
//  Created by Gurpreet Gulati on 15/05/19.
//  Copyright © 2019 Temp. All rights reserved.
//

import Foundation

public class Participantss {
    public var isAdmin : Bool?
    public var isBlocked : Bool?
    public var unreadCount : Int?
    public var user : [User]?
    public var userId : String?
     public var timeStamp : String?

    
    

    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let Participantss_list = Participantss.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of Participantss Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [Participantss]
    {
        var models:[Participantss] = []
        for item in array
        {
            models.append(Participantss(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let Participantss = Participantss(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Participantss Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        isAdmin = dictionary["isAdmin"] as? Bool
        userId = dictionary["userId"] as? String
        timeStamp = dictionary["timeStamp"] as? String
        isBlocked = dictionary["isBlocked"] as? Bool
        unreadCount = dictionary["unreadCount"] as? Int
         if (dictionary["user"] != nil) { user = User.modelsFromDictionaryArray(array: dictionary["user"] as! NSArray) }
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.isAdmin, forKey: "isAdmin")
        dictionary.setValue(self.userId, forKey: "userId")
        dictionary.setValue(self.isBlocked, forKey: "isBlocked")
        dictionary.setValue(self.unreadCount, forKey: "unreadCount")
        dictionary.setValue(self.timeStamp, forKey: "timeStamp")
  
        var arr = [NSDictionary]()
        if self.user != nil{
            for item in self.user!{
                arr.append(item.dictionaryRepresentation())
            }
            dictionary.setValue(arr, forKey: "user")
            
        }
        
        return dictionary
    }
    
}
