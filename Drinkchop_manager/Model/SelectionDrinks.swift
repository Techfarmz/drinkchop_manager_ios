//
//  Card.swift
//  drinkchop-ios
//
//  Created by Tech Farmerz on 16/02/19.
//  Copyright © 2019 Gurpreet Singh. All rights reserved.
//

import Foundation

public class SelectionDrinks {
    
    public var alcoholPercentage : String?
    public var id: String?
    public var description: String?
    public var imgUrl: String?
    public var name: String?
    public var regularPrice: String?
    public var quantity: String?
    public var doublePrice: String?
    
    public var userInfo: NSDictionary?
    
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [SelectionDrinks]
    {
        var models:[SelectionDrinks] = []
        for item in array
        {
            models.append(SelectionDrinks(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
           regularPrice = dictionary["regularPrice"] as? String
           doublePrice = dictionary["doublePrice"] as? String
        id = dictionary["id"] as? String
        alcoholPercentage = dictionary["alcoholPercentage"] as? String
        description = dictionary["description"] as? String
        imgUrl = dictionary["imgUrl"] as? String
        name = dictionary["name"] as? String
        quantity = dictionary["quantity"] as? String
       // userInfo = dictionary["user"] as? NSDictionary
    }
    
    public func dictionaryRepresentation() -> NSDictionary {
        let dictionary = NSMutableDictionary()
           dictionary.setValue(self.regularPrice, forKey: "regularPrice")
           dictionary.setValue(self.doublePrice, forKey: "doublePrice")
        dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.alcoholPercentage, forKey: "alcoholPercentage")
        dictionary.setValue(self.description, forKey: "description")
        dictionary.setValue(self.imgUrl, forKey: "imgUrl")
        dictionary.setValue(self.name, forKey: "name")
        dictionary.setValue(self.quantity, forKey: "quantity")
        
        return dictionary
    }
}
