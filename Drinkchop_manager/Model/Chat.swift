//
//  Chat.swift
//  DRINKCHOP
//
//  Created by Tech Farmerz on 07/12/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

import Foundation

public class Chat {
    
    public var chatType : String?
    public var id : String?
    public var lastMessage : String?
    public var participants : [Participantss]?
    public var groupName : String?
    public var user : User?
    public var updatedAt : String?
    public var chatId : String?
    public var timeStamp : Double?
    public var message : String?
    public var opponentId : String?
    public var myId : String?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let Chat_list = Chat.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of Chat Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [Chat]
    {
        var models:[Chat] = []
        for item in array
        {
            models.append(Chat(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let Chat = Chat(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Chat Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        myId = dictionary["myId"] as? String
        opponentId = dictionary["opponentId"] as? String
         message = dictionary["message"] as? String
         timeStamp = dictionary["timeStamp"] as? Double
        chatId = dictionary["chatId"] as? String
        chatType = dictionary["chatType"] as? String
        updatedAt = dictionary["updatedAt"] as? String
        id = dictionary["id"] as? String
        lastMessage = dictionary["lastMessage"] as? String
          groupName = dictionary["groupName"] as? String
        if (dictionary["participants"] != nil) { participants = Participantss.modelsFromDictionaryArray(array: dictionary["participants"] as! NSArray) }
            if (dictionary["user"] != nil) { user = User(dictionary: dictionary["user"] as! NSDictionary) }
    }
    
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        
        dictionary.setValue(self.opponentId, forKey: "opponentId")
        dictionary.setValue(self.message, forKey: "message")
        dictionary.setValue(self.chatId, forKey: "chatId")
        dictionary.setValue(self.chatType, forKey: "chatType")
        dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.lastMessage, forKey: "lastMessage")
        dictionary.setValue(self.groupName, forKey: "groupName")
        dictionary.setValue(self.updatedAt, forKey: "updatedAt")
        dictionary.setValue(self.timeStamp, forKey: "timeStamp")
        dictionary.setValue(self.user?.dictionaryRepresentation(), forKey: "user")
         dictionary.setValue(self.myId, forKey: "myId")
        
        var arr = [NSDictionary]()
        if self.participants != nil{
            for item in self.participants!{
                arr.append(item.dictionaryRepresentation())
            }
            dictionary.setValue(arr, forKey: "participants")
        }
        
        
        
        
        
        return dictionary
    }
    
}
