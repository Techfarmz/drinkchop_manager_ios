//
//  eventadress.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 07/06/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import Foundation


enum eventadressAttributes: String {
    
    case address = "eventaddress"
    case state = "eventstate"
    case zipCode = "eventzipCode"
    case city = "eventcity"
    case country = "country"
    
}


public class Eventadress {
    public var address : String?
    public var state : String?
    public var zipCode : Int?
    public var city : String?
     public var country : String?
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let location_list = Location.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of Location Instances.
     */
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let location = Location(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Location Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        address = dictionary["address"] as? String
      
        state = dictionary["state"] as? String
        zipCode = dictionary["zipCode"] as? Int
        city = dictionary["city"] as? String
        country = dictionary["country"] as? String
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.address, forKey: "address")
        dictionary.setValue(self.state, forKey: "state")
        dictionary.setValue(self.zipCode, forKey: "zipCode")
        dictionary.setValue(self.country, forKey: "country")
        dictionary.setValue(self.city, forKey: "city")
        
        
        return dictionary
    }
    
}
