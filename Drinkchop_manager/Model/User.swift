

import Foundation
 


enum UserAttributes :String {
  
  case name = "name"
  case email = "email"
  case checkUser = "checkUser"
  case password = "password"
  case newPassword = "newPassword"
  case deviceId = "deviceId"
  case deviceType = "deviceType"
  case imgUrl = "imgUrl"
  case username = "username"
  case token = "token"
  case id = "id"
  case status = "status"
  case firstName = "firstName"
   case lastName = "lastName"
   case phone = "phone"
   case location = "location"
    case dob = "dob"
    case gender = "gender"
    case registrationId = "registrationId"
    case googleId = "googleId"
    case activationCode = "activationCode"
    case userId = "userId"
    case isFamily = "isFamily"
    case familyId = "familyId"
    case isSelected = "isSelected"
    case locationCoordinates = "locationCoordinates"
    case birthday = "birthday"
    case country = "country"
    
    case facebookId = "facebookId"
    case zipCodeId = "zipCodeId"
    case regionId = "regionId"
    case lgaId = "lgaId"
    case stateId = "stateId"
    case zipCode = "zipCode"
    case region = "region"
    case lga = "lga"
    case state = "state"
  


    
    
    
    
    
  
  static let getAll = [
    name,
    email,
    password,
    checkUser,
    newPassword,
    deviceId,
    deviceType,
     imgUrl,
    username,
    token,
    id,
    status,
    firstName,
    lastName,
    phone,
    location,
    dob,
    gender,
    registrationId,
    googleId,
    activationCode,
    userId,
    isFamily,
    familyId,
    isSelected,
    locationCoordinates,
    birthday,
    country,
    facebookId,
    zipCodeId,
    regionId,
    lgaId,
    stateId,
    zipCode,
    region,
    lga,
    state
    
    
  ]
}


public class User {
	public var name : String?
	public var email : String?
	public var password : String?
  public var newPassword : String?
  public var checkUser : Bool?
	public var deviceId : String?
	public var deviceType : String?
	public var imgUrl : String?
	public var username : String?
  public var token : String?
  public var id : String?
  public var status : String?
    public var firstName : String?
    public var lastName : String?
    public var phone : String?
   public var location : Location?
    public var eventadress : Eventadress?
    public var dob : String?
    public var gender : String?
    public var registrationId : String?
    public var googleId : String?
    public var activationCode : String?
    public var userId : Int?
    public var isFamily : Bool?
    public var familyId : Int?
    public var isSelected : Bool?
    public var locationCoordinates : [String]?
    public var birthday : String?
    public var country : String?
    public var facebookId : String?
    public var zipCodeId : String?
    public var lgaId : String?
    public var stateId : String?
    public var regionId : String?
    public var zipCode : String?
    public var lga : String?
    public var state : String?
    public var region : String?
    public var role : String?
    public var coverFee : String?
    public var description : String?
    public var availbilityStart : String?
    public var availbilityTill : String?
    public var date : String?
    public var barId : String?
    public var fbLink : String?
    public var instaLink : String?
    public var address : String?
    public var city : String?
    public var twitterLink : String?
    
    public var StripeAccountHolderName : String?
    public var StripeAccountLast4Digit : String?
    public var StripeCurrency : String?
    public var bankName : String?
    public var bankToken : String?
    public var routingNumber : String?
    
    public var groupName : String?

    
/**
    Returns an array of models based on given dictionary.
    
    Sample usage:
    let User_list = User.modelsFromDictionaryArray(someDictionaryArrayFromJSON)

    - parameter array:  NSArray from JSON dictionary.

    - returns: Array of User Instances.
*/
    public class func modelsFromDictionaryArray(array:NSArray) -> [User]
    {
        var models:[User] = []
        for item in array
        {
            models.append(User(dictionary: item as! NSDictionary)!)
        }
        return models
    }

/**
    Constructs the object based on the given dictionary.
    
    Sample usage:
    let User = User(someDictionaryFromJSON)

    - parameter dictionary:  NSDictionary from JSON.

    - returns: User Instance.
*/
	required public init?(dictionary: NSDictionary) {
        
         StripeAccountHolderName = dictionary["StripeAccountHolderName"] as? String
         StripeAccountLast4Digit = dictionary["StripeAccountLast4Digit"] as? String
         StripeCurrency = dictionary["StripeCurrency"] as? String
         bankName = dictionary["bankName"] as? String
         bankToken = dictionary["bankToken"] as? String
         routingNumber = dictionary["routingNumber"] as? String
         role = dictionary["role"] as? String
        description = dictionary["description"] as? String
         fbLink = dictionary["fbLink"] as? String
         instaLink = dictionary["instaLink"] as? String
         address = dictionary["address"] as? String
         city = dictionary["city"] as? String
         date = dictionary["date"] as? String
            availbilityTill = dictionary["availbilityTill"] as? String
        availbilityStart = dictionary["availbilityStart"] as? String
		name = dictionary["name"] as? String
		email = dictionary["email"] as? String
		password = dictionary["password"] as? String
		deviceId = dictionary["deviceId"] as? String
		deviceType = dictionary["deviceType"] as? String
		 imgUrl = dictionary["imgUrl"] as? String
    newPassword = dictionary["newPassword"] as? String
		username = dictionary["username"] as? String
    status = dictionary["status"] as? String
    token = dictionary["token"] as? String
    id = dictionary["id"] as? String
        firstName = dictionary["firstName"] as? String
        lastName = dictionary["lastName"] as? String
        phone = dictionary["phone"] as? String
      if (dictionary["location"] != nil) { location = Location(dictionary: dictionary["location"] as! NSDictionary) }
        if (dictionary["eventadress"] != nil) { eventadress = Eventadress(dictionary: dictionary["eventadress"] as! NSDictionary) }
        dob = dictionary["dob"] as? String
        gender = dictionary["gender"] as? String
        registrationId = dictionary["registrationId"] as? String
        googleId = dictionary["googleId"] as? String
        activationCode = dictionary["activationCode"] as? String
        userId = dictionary["userId"] as? Int
        isFamily = dictionary["isFamily"] as? Bool
        familyId = dictionary["familyId"] as? Int
        isSelected = dictionary["isSelected"] as? Bool
        checkUser = dictionary["checkUser"] as? Bool
        locationCoordinates = dictionary["locationCoordinates"] as? [String]
        birthday = dictionary["birthday"] as? String
        country = dictionary["country"] as? String
         facebookId = dictionary["facebookId"] as? String
        zipCodeId = dictionary["zipCodeId"] as? String
        lgaId = dictionary["lgaId"] as? String
        regionId = dictionary["regionId"] as? String
        stateId = dictionary["stateId"] as? String
        zipCode = dictionary["zipCode"] as? String
        lga = dictionary["lga"] as? String
        region = dictionary["region"] as? String
        state = dictionary["state"] as? String
        barId = dictionary["barId"] as? String
        twitterLink = dictionary["twitterLink"] as? String
	}

		
/**
    Returns the dictionary representation for the current instance.
    
    - returns: NSDictionary.
*/
	public func dictionaryRepresentation() -> NSDictionary {

		let dictionary = NSMutableDictionary()
        
         dictionary.setValue(self.StripeAccountHolderName, forKey: "StripeAccountHolderName")
         dictionary.setValue(self.StripeAccountLast4Digit, forKey: "StripeAccountLast4Digit")
         dictionary.setValue(self.StripeCurrency, forKey: "StripeCurrency")
         dictionary.setValue(self.bankName, forKey: "bankName")
         dictionary.setValue(self.bankToken, forKey: "bankToken")
         dictionary.setValue(self.routingNumber, forKey: "routingNumber")
        
          dictionary.setValue(self.role, forKey: "role")
        dictionary.setValue(self.description, forKey: "description")
            dictionary.setValue(self.fbLink, forKey: "fbLink")
            dictionary.setValue(self.instaLink, forKey: "instaLink")
            dictionary.setValue(self.address, forKey: "address")
            dictionary.setValue(self.city, forKey: "city")
            dictionary.setValue(self.date, forKey: "date")
            dictionary.setValue(self.availbilityTill, forKey: "availbilityTill")
            dictionary.setValue(self.availbilityStart, forKey: "availbilityStart")
		dictionary.setValue(self.name, forKey: "name")
		dictionary.setValue(self.email, forKey: "email")
		dictionary.setValue(self.password, forKey: "password")
    dictionary.setValue(self.newPassword, forKey: "newPassword")
		dictionary.setValue(self.deviceId, forKey: "deviceId")
		dictionary.setValue(self.deviceType, forKey: "deviceType")
		dictionary.setValue(self.imgUrl, forKey: "imgUrl")
		dictionary.setValue(self.username, forKey: "username")
    dictionary.setValue(self.status, forKey: "status")
    dictionary.setValue(self.id, forKey: "id")
    dictionary.setValue(self.token, forKey: "token")
        dictionary.setValue(self.firstName, forKey: "firstName")
        dictionary.setValue(self.lastName, forKey: "lastName")
        dictionary.setValue(self.phone, forKey: "phone")
       dictionary.setValue(self.location?.dictionaryRepresentation(), forKey: "location")
         dictionary.setValue(self.eventadress?.dictionaryRepresentation(), forKey: "eventadress")
        dictionary.setValue(self.dob, forKey: "dob")
        dictionary.setValue(self.gender, forKey: "gender")
        dictionary.setValue(self.registrationId, forKey: "registrationId")
        dictionary.setValue(self.googleId, forKey: "googleId")
        dictionary.setValue(self.activationCode, forKey: "activationCode")
        dictionary.setValue(self.userId, forKey: "userId")
        dictionary.setValue(self.isFamily, forKey: "isFamily")
        dictionary.setValue(self.familyId, forKey: "familyId")
        dictionary.setValue(self.isSelected, forKey: "isSelected")
        dictionary.setValue(self.locationCoordinates, forKey: "locationCoordinates")
        dictionary.setValue(self.birthday, forKey: "birthday")
        dictionary.setValue(self.country, forKey: "country")
        dictionary.setValue(self.facebookId, forKey: "facebookId")
        dictionary.setValue(self.zipCodeId, forKey: "zipCodeId")
        dictionary.setValue(self.checkUser, forKey: "checkUser")
        dictionary.setValue(self.lgaId, forKey: "lgaId")
        dictionary.setValue(self.stateId, forKey: "stateId")
        dictionary.setValue(self.regionId, forKey: "regionId")
        dictionary.setValue(self.zipCode, forKey: "zipCode")
        dictionary.setValue(self.lga, forKey: "lga")
        dictionary.setValue(self.state, forKey: "state")
        dictionary.setValue(self.region, forKey: "region")
        dictionary.setValue(self.barId, forKey: "barId")
        dictionary.setValue(self.twitterLink, forKey: "twitterLink")
        

		return dictionary
	}

}


public class countries {
    public var id : String?
    public var sortname : String?
    public var name : String?
    public var phonecode : String?


    
    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let User_list = User.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of User Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [countries]
    {
        var models:[countries] = []
        for item in array
        {
            models.append(countries(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let User = User(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: User Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        id = dictionary["id"] as? String
        sortname = dictionary["sortname"] as? String
        name = dictionary["name"] as? String
        phonecode = dictionary["phonecode"] as? String
      
    }
    
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.sortname, forKey: "sortname")
        dictionary.setValue(self.name, forKey: "name")
        dictionary.setValue(self.phonecode, forKey: "phonecode")
      
        
        
        return dictionary
    }
    
}
