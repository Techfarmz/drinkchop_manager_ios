//
//  Card.swift
//  drinkchop-ios
//
//  Created by Tech Farmerz on 16/02/19.
//  Copyright © 2019 Gurpreet Singh. All rights reserved.
//

import Foundation

public class SelectionCategory {
    
    public var id: String?
    public var drinks: [SelectionDrinks]?
    public var categoriesname: String?

    
    public var userInfo: NSDictionary?
    
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [SelectionCategory]
    {
        var models:[SelectionCategory] = []
        for item in array
        {
            models.append(SelectionCategory(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
        id = dictionary["id"] as? String
        categoriesname = dictionary["categoriesname"] as? String
        if (dictionary["drinks"] != nil) { drinks = SelectionDrinks.modelsFromDictionaryArray(array: dictionary["drinks"] as! NSArray)
       // userInfo = dictionary["user"] as? NSDictionary
    }
    
     func dictionaryRepresentation() -> NSDictionary {
        var arr = [NSDictionary]()
        if self.drinks != nil{
            for item in self.drinks!{
                arr.append(item.dictionaryRepresentation())
            }
            dictionary.setValue(arr, forKey: "drinks")
        }
        let dictionary = NSMutableDictionary()
        dictionary.setValue(self.id, forKey: "id")
        dictionary.setValue(self.categoriesname, forKey: "categoriesname")
        
        return dictionary
    }
}
}
