//
//  Card.swift
//  drinkchop-ios
//
//  Created by Tech Farmerz on 16/02/19.
//  Copyright © 2019 Gurpreet Singh. All rights reserved.
//

import Foundation

public class Days {
    
    public var barstatus : Bool?
  //  public var _id: String?
    public var day: String?
    public var endtime: String?
    public var newcoverfee: String?
    public var starttime: String?
    
    
    public var userInfo: NSDictionary?
    
    
    public class func modelsFromDictionaryArray(array:NSArray) -> [Days]
    {
        var models:[Days] = []
        for item in array
        {
            models.append(Days(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    required public init?(dictionary: NSDictionary) {
    //    _id = dictionary["_id"] as? String
        barstatus = dictionary["barstatus"] as? Bool
        day = dictionary["day"] as? String
        endtime = dictionary["endtime"] as? String
        newcoverfee = dictionary["newcoverfee"] as? String
         starttime = dictionary["starttime"] as? String
       // userInfo = dictionary["user"] as? NSDictionary
    }
    
    public func dictionaryRepresentation() -> NSDictionary {
        let dictionary = NSMutableDictionary()
    //    dictionary.setValue(self._id, forKey: "_id")
        dictionary.setValue(self.barstatus, forKey: "barstatus")
        dictionary.setValue(self.day, forKey: "day")
        dictionary.setValue(self.endtime, forKey: "endtime")
        dictionary.setValue(self.newcoverfee, forKey: "newcoverfee")
         dictionary.setValue(self.starttime, forKey: "starttime")
        
        return dictionary
    }
}
