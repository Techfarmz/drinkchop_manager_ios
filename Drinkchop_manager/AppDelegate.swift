//
//  AppDelegate.swift
//  Drinkchop_manager
//
//  Created by Tech Farmerz on 18/02/19.
//  Copyright © 2019 Tech Farmerz. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?

    static var originalAppDelegate : AppDelegate!
    func sharedInstance() -> AppDelegate{
        return AppDelegate.originalAppDelegate
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        AppDelegate.originalAppDelegate = self

        AppDelegate.originalAppDelegate = self
        FirebaseApp.configure()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rootViewController: UIViewController?
        let isLogin = Constants.kUserDefaults.bool(forKey: "isLogin")
        if isLogin == true {
            rootViewController = storyboard.instantiateViewController(withIdentifier: "HomeVC")
        }else
        {
            rootViewController = storyboard.instantiateViewController(withIdentifier: "ViewController")
        }
        let navigation = UINavigationController(rootViewController: rootViewController!)
        self.window?.rootViewController = navigation
        self.window?.makeKeyAndVisible()
 
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func showNotification(text: String){
        let displayInterval =  max(CGFloat(text.length) * 0.04 + 0.5,0.5)
        //  self.notificationBanner.display( withMessage: text, forDuration: Double(displayInterval) )
    }

}

extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}
